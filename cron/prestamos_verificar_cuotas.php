<?php
## Este script permite verificar las cuotas de un prestamo.
## De ser necesario aplica interes sobre las cuotas.
## Creador: Luis Viera
## Correo: 	levieraf@gmail.com
## Creado: 	25-06-2014 9:33:50 PM

## Ej.: $PATH_SERVER = /var/www/pyme
$PATH_SERVER = $_SERVER["PWD"]; 	

## Configuración de conexión
define('GESTOR_DATABASE','mysql', true);
define('SERVIDOR', 'localhost', true);
define('USUARIO', 'root', true);
define('CLAVE', '', true);
define('BASEDEDATOS', 'fortaleza_administrativo', true);
define('ADODB_FETCH_ASSOC', "ADODB_FETCH_ASSOC", true);

## Parametros base
$PRESTAMO_ESTATUS_CUOTA_PENDIENTE = 1;
$PRESTAMO_ESTATUS_CUOTA_PAGADO = 2;
$PRESTAMO_ESTATUS_CUOTA_PARCIAL = 2;
$PRESTAMO_ESTATUS_CUOTA_MORA = 4;

$PRESTAMO_ESTATUS_PRESTAMO_PENDIENTE = 1;
$PRESTAMO_ESTATUS_PRESTAMO_ANULADO = 3;
$PRESTAMO_ESTATUS_PRESTAMO_APROBADO = 4;


## Librerias base
include("$PATH_SERVER/selectraerp/libs/php/adodb5/adodb.inc.php");
include("$PATH_SERVER/selectraerp/libs/php/clases/ConexionComun.php");


## Iniciando Proceso
print "Iniciando verificación...\n";


$conexion = new ConexionComun();
$prestamos = $conexion->ObtenerFilasBySqlSelect("select * from prestamos_clientes where id_prestamos_estatus = {$PRESTAMO_ESTATUS_PRESTAMO_APROBADO}");
print_r($prestamos);