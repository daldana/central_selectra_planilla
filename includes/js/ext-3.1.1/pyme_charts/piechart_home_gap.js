/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.chart.Chart.CHART_URL = '../../../includes/js/ext-3.1.1/resources/charts.swf';

Ext.onReady(function(){
    var storeProductos = new Ext.data.JsonStore({
        fields: ['codigo', 'descripcion','cantidad','precio','label'],
        data:dataProductos
        
    });
    var storeClientes = new Ext.data.JsonStore({
        fields: ['codigo', 'descripcion','cantidad'],
        data: dataClientes
    });
    
    var storeProveedores = new Ext.data.JsonStore({
        fields: ['codigo', 'descripcion','cantidad'],
        data: dataProveedores
    });
    /*
    console.log(dataProductos)
    console.log(dataClientes)
    console.log(dataProveedores)*/
    var chartProductos = new Ext.Panel({
        width: 450,
        height: 350,
        title: 'Productos M&aacute;s Vendidos',
        renderTo: 'container-producto',
        collapsible: true,
        collapseFirst :true,
        items: {
            store: storeProductos,
            xtype: 'piechart',
            dataField: 'cantidad',
            categoryField: 'descripcion',
            series:[{
                    style:{
                        //colors:["#0000BB","#BB0000","#00BB00"]
                        //colors: ["#ff2400", "#94660e", "#00b8bf", "#edff9f"]
                        colors: ["#00b8bf","#ff2400", "#edff9f","#5DE375","#7C60EB","#C3ED68","#EDAB68","#F294E7","#B5F7F7","#EDE268"]
                    }
                    
            }],
            label: {
                display: 'label',
                field: 'cantidad',
                renderer: function(n) {
                  return n;
                },
                orientation: 'horizontal',
                color: '#333',
                'text-anchor': 'middle'
              },
            //extra styles get applied to the chart defaults
            extraStyle:
            {
                legend:
                {
                    display: 'left',
                    padding: 3,
                    font:
                    {
                        family: 'Tahoma',
                        size: 8
                    }
                }
            }
        }
    });
    
    new Ext.Panel({
        width: 450,
        height: 350,
        title: 'Clientes Con M&aacute;s Compras',
        renderTo: 'container-cliente',
        collapsible: true,
        collapseFirst :true,
        items: {
            store: storeClientes,
            xtype: 'piechart',
            dataField: 'cantidad',
            categoryField: 'descripcion',
            series:[{
                    style:{
                        //colors:["#0000BB","#BB0000","#00BB00"]
                        //colors: ["#ff2400", "#94660e", "#00b8bf", "#edff9f"]
                        colors: ["#00b8bf","#ff2400", "#edff9f","#5DE375","#7C60EB","#C3ED68","#EDAB68","#F294E7","#B5F7F7","#EDE268"]
                    }
                    
            }],
            label: {
                display: 'descripcion',
                field: 'cantidad',
                renderer: function(n) {
                  return n;
                },
                orientation: 'horizontal',
                color: '#333',
                'text-anchor': 'middle'
              },
            //extra styles get applied to the chart defaults
            extraStyle:
            {
                legend:
                {
                    display: 'left',
                    padding: 3,
                    font:
                    {
                        family: 'Tahoma',
                        size: 8
                    }
                }
            }
        }
    });
    
    new Ext.Panel({
        width: 450,
        height: 350,
        title: 'Proveedores Con M&aacute;s Ventas',
        renderTo: 'container-proveedor',
        collapsible: true,
        collapseFirst :true,
        items: {
            store: storeProveedores,
            xtype: 'piechart',
            dataField: 'cantidad',
            categoryField: 'descripcion',
            series:[{
                    style:{
                        //colors:["#0000BB","#BB0000","#00BB00"]
                        //colors: ["#ff2400", "#94660e", "#00b8bf", "#edff9f"]
                        colors: ["#00b8bf","#ff2400", "#edff9f","#5DE375","#7C60EB","#C3ED68","#EDAB68","#F294E7","#B5F7F7","#EDE268"]
                    }
                    
            }],
            label: {
                display: 'descripcion',
                field: 'cantidad',
                renderer: function(n) {
                  return n;
                },
                orientation: 'horizontal',
                color: '#333',
                'text-anchor': 'middle'
              },
            //extra styles get applied to the chart defaults
            extraStyle:
            {
                legend:
                {
                    display: 'left',
                    padding: 3,
                    font:
                    {
                        family: 'Tahoma',
                        size: 8
                    }
                }
            }
        }
    });
});
