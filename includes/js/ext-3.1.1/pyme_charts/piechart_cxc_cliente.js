/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
Ext.chart.Chart.CHART_URL = '../../../includes/js/ext-3.1.1/resources/charts.swf';

Ext.onReady(function(){
    var store = new Ext.data.JsonStore({
        fields: ['status', 'total'],
        data: [{
            status: 'Creditos',
            total: CHART_TOTAL_FACTURAS_PAGADAS
        },{
            status: 'Debitos',
            total: CHART_TOTAL_FACTURAS_PENDIENTES
        }]
    });
    
    new Ext.Panel({
        width: 500,
        height: 400,
        title: 'Gráfico Ventas del Cliente',
        renderTo: 'container',
        collapsible: true,
        collapseFirst :true,
        items: {
            store: store,
            xtype: 'piechart',
            dataField: 'total',
            categoryField: 'status',
            series:[{
                    style:{
                        //colors:["#0000BB","#BB0000","#00BB00"]
                        //colors: ["#ff2400", "#94660e", "#00b8bf", "#edff9f"]
                        colors: ["#00b8bf","#ff2400"]
                    }
                    
            }],
            label: {
                display: 'status',
                field: 'total',
                renderer: function(n) {
                  return n;
                },
                orientation: 'horizontal',
                color: '#333',
                'text-anchor': 'middle'
              },
            //extra styles get applied to the chart defaults
            extraStyle:
            {
                legend:
                {
                    display: 'bottom',
                    padding: 5,
                    font:
                    {
                        family: 'Tahoma',
                        size: 13
                    }
                }
            }
        }
    });
});