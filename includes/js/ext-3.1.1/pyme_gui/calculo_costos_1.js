var fields_articulo = [
        { mapping:'id_item', name:'id_item'   , type: 'int'     },
	{ mapping:'cod_item', name:'cod_item'   , type: 'string'     },
	{ mapping:'bultos', name:'bultos'   , type: 'int'     },
        { mapping:'cantidad', name:'cantidad'   , type: 'int'     },
	{ mapping:'incid_costo', name:'incid_costo'   , type: 'float'     },
	{ mapping:'incid_costo_con_flete', name:'incid_costo_con_flete'   , type: 'float'     }
];

devolucionForm = function(){
	// Arrays de detalle        
        var linesNumArray = new Array();
	var articuloIdArray = null;
        var claseArray = null;
	var cantidadArray = null;
	var IncidArray = null;        
	var IncidFleteArray = null;        
        var descripcionArray = new Array();
        var direccionesMapaWin = null;
        
	
	var Ncont = 0;
	
        var storeItemDevolucion = null;
	var gridItemDevolucion = null;
	var storeItemDevolver = null;
	var gridItemDevolver = null;
	var storeDirecciones = null;
	var gridDirecciones = null;
        var recId = 1;
        var editando = false;
        var expander = new Ext.ux.grid.RowExpander({
                tpl : new Ext.Template(
                    '<p><b>{descripcion_item}</p><br>',
                    '<p><b>Marca:</b> {marca_item}</p><br>',
                    '<p><b>Modelo</b> {modelo_item}</p><br>'
                )
            }); 

return{
	
	idWin:function(){return 'devolucionForm-win';},
	
	moduleFields: fields_articulo,
        moduleFields2: fields_articulo,
	
	initRecordDef : function(){
		return new Ext.data.Record.create(devolucionForm.moduleFields);
	},
	  
	initRecordDef2 : function(){
		return new Ext.data.Record.create(devolucionForm.moduleFields2);
	},         
	initColumnModel : function(){
	               return new Ext.grid.ColumnModel([
	                   expander,
	                   {header: 'Código', width:30,   dataIndex:'cod_item',  sortable: false},
	                   {header: 'Cantidad', width:30,   dataIndex:'cantidad',  sortable: false,
	                	   	editor: new Ext.form.NumberField({
	                	   			allowBlank:false,	                				
                                                        selectOnFocus: true,
                                                        id:'editor-cantidad-devolver',
                                                        listeners:{
                                                            focus:function(e){editando = true;},
                                                            blur:function(e){editando = false;}                                                    
                                                        }
                                        }),
                                        renderer:function (v, p, record){
                                            return v;					    		   
                                        }
	                   },
	                   {header: 'Motivo',    dataIndex:'motivo',  sortable: false,
	                	   	editor: new Ext.form.TextField({
	                	   			allowBlank:false,	                				
                                                        selectOnFocus: true,
                                                        listeners:{
                                                            focus:function(tf){
                                                                editando = true;
                                                            },
                                                            blur:function(e){editando = false;} 
                                                        }
                                        }),
                                        renderer:function (v, p, record){
                                            return v;
                                        }
	                   },
                            {header: 'N.Entrega', width:60,  dataIndex:'nota_entrega',  sortable: false}
	               ]);
	},
	
	initColumnModel2 : function(){
            return new Ext.grid.ColumnModel([
                {header: 'Código', width:40,   dataIndex:'codigo_item',  sortable: false},
                {header: 'Descripción', width:140, id: 'descripcion_item',   dataIndex:'descripcion_item',  sortable: false},
                {header: 'Marca',   dataIndex:'marca_item',  sortable: false},
                //{header: 'Modelo',  dataIndex:'modelo_item',  sortable: false},
                {header: 'N.Entrega', width:60,  dataIndex:'nota_entrega',  sortable: false}
            ]);
	},
	
	initColumnModel3 : function(){
            return new Ext.grid.ColumnModel([
                {header: 'Direcci&oacute;n',   dataIndex:'direccion',  sortable: false}
            ]);
	},
	formload:function(){
	        Ext.getCmp('add-solicitud-sust-form').form.reset();                            	
                storeItemDevolver.removeAll();
                storeItemDevolucion.removeAll();
                Ext.getCmp('patronbuscar-solicitar-sustitucion').setValue("");
	},
        
        validaDireccion:function(){
            var esta = Ext.getCmp('estado');
            var ciu = Ext.getCmp('ciudad');
            var sec = Ext.getCmp('sector');
            var av = Ext.getCmp('avenida');
            var call = Ext.getCmp('calle');
            var tipoloc = Ext.getCmp('tipo_loc_id');
            var num = Ext.getCmp('numero_locacion');
            var ref = Ext.getCmp('referencia');
            var tlf = Ext.getCmp('telefonos');
            
            if(esta.getValue() == esta.originalValue || ciu.getValue() == ciu.originalValue || sec.getValue() == sec.originalValue
         || av.getValue() == av.originalValue || call.getValue() == call.originalValue || tipoloc.getValue() == tipoloc.originalValue
  || num.getValue() == num.originalValue || ref.getValue() == ref.originalValue || tlf.getValue() == tlf.originalValue){                
                return false;
            }
            return true;
        },
        Init:function(){
        
        function buscar(idBuscar,store){
            var patron = Ext.getCmp(idBuscar);
            
            store.load({
                                    params:{
                                            start:0, 
                                            limit:25,
                                            task: "busqueda",
                                            patronbuscar : patron.getValue(),
                                            accion: 'Articulo//Get_Disponibles',
                                            clase:'R,P',
                                            padre:'S'
                                    }
            });
	}
        
        storeDirecciones =  new Ext.data.Store({
            storeId:'store-direcciones-devolucion',
            proxy:new Ext.data.HttpProxy({url:'../../clases/controladores/ControladorApp.php'}),
            restful : true,
            autoLoad:false,
            baseParams: {
                      moduleId: 'sustitucion'
            },
            reader: new Ext.data.JsonReader({
                totalProperty: "totalCount",
                root: "matches" // ,id: "bl"
                }, devolucionForm.initRecordDef()),
            listeners: {
                load: function(Store ,records, options){ }
            }
        });
        
        gridDirecciones = new Ext.grid.GridPanel({
            id: 'grid-direcciones-devolucion',
            autoScroll:true,
            viewConfig:{forceFit:true},
            border:true,
            height: 300,
            stripeRows: true,
            loadMasck:true,
            ds: storeDirecciones,
            cm: devolucionForm.initColumnModel3(),
            remoteSort:true,
            shadow: false,
            shadowOffset: 0,
            sm:  new Ext.grid.RowSelectionModel({
                singleSelect:true,
                listeners: {
                    rowselect: function(SelectionModel,rownumber,row){
                    }
                }
            }),
            listeners:{
                    dblclick:function(e){
                        //@TODO
                    }
            },
            bbar:[{
                    xtype:'displayfield',
                    value:'Haga doble click para selecionar la direcci&oacute;n de la devoluci&oacute;n'
                }]
        });
    
    storeItemDevolucion = new Ext.data.Store({
    	storeId:'store-item-solicitud-sustitucion',
        restful : true,
        baseParams: {
                  moduleId: 'sustitucion'
        },
        reader: new Ext.data.JsonReader({
            totalProperty: "totalCount",
            root: "matches" // ,id: "bl"
            }, devolucionForm.initRecordDef2()),
        listeners: {
            load: function(Store ,records, options){ }
        }
    });
    
    gridItemDevolucion= new Ext.grid.EditorGridPanel({
        id: 'grid-item-solicitud-sustitucion',
        title: 'Items a devolver',
        autoScroll:true,
        viewConfig:{forceFit:true},
        border:true,
        clicksToEdit: 1,
        height: 310,
        stripeRows: true,
        ds: storeItemDevolucion,
        cm: devolucionForm.initColumnModel(),
        autoExpandColumn : 'descripcion_item',
        remoteSort:false,
        shadow: false,
        shadowOffset: 0,
        sm:  new Ext.grid.RowSelectionModel({
            singleSelect:true,
            listeners: {
                rowselect: function(SelectionModel,rownumber,row){
                    if(row.data.clase == 'P')
                        this.grid.colModel.config[3].editor.setReadOnly(false);
                    else
                        this.grid.colModel.config[3].editor.setReadOnly(true);
                }
            }
        }),
        listeners:{
        	dblclick:function(e){
                        if(!editando){
                            var grid = Ext.getCmp('grid-item-solicitud-sustitucion');
                            var selmodel = grid.getSelectionModel();	    		
                            var filaSeleccionada = selmodel.getSelected();
                            var indexRowDel = storeItemDevolucion.indexOf(filaSeleccionada);
                            Ext.MessageBox.buttonText.yes = "Sí";
                            Ext.MessageBox.buttonText.no = "No";
                            Ext.MessageBox.buttonText.cancel = "Cancelar";
                            Ext.MessageBox.show({
                                title:'Confirmación!',
                                msg: 'Seguro que desea eliminar de la devoluci&oacute; este item?',
                                buttons: Ext.MessageBox.YESNOCANCEL,
                                fn: function (val){
                                     if(val == 'yes'){   
                                        storeItemDevolucion.removeAt(indexRowDel);
                                     }
                                },
                                                   icon: Ext.MessageBox.QUESTION
                            });
                        }                           
        	}
        },
        bbar:[{
                xtype:'displayfield',
                value:'Haga doble click para sacar item de lista de devoluci&oacute;n'
            }],
        plugins:[expander]
    });
	
    
    var tbGridSust = new Ext.Toolbar({
		id:'tbb-solicitar-sustitucion',
		autoDestroy:false,
    	buttonAlign: 'right',
    	items:	[
             {
      		xtype: 'textfield',
              allowBlank : false,
              //minLength: 5,
              //maxLength: 25,
              name: 'patronbuscar-solicitar-sustitucion',
              id: 'patronbuscar-solicitar-sustitucion',
              anchor:'50%',
              selectOnFocus : true,            		
              listeners:{
		        	specialkey: function(patronBlbuscar,e){ 
		            	if (e.getKey() == e.ENTER) { 
                                    buscar('patronbuscar-solicitar-sustitucion',storeItemDevolver); 
		            	}
        			}
      			}
              }, {
              text:'Buscar',
              id:'buscar-btn-solicitar-sustitucion',
              tooltip:'Click para buscar',
              iconCls:'edit',
              icon: '../../imagenes/icons/page_find.png',
              handler: function(){buscar('patronbuscar-solicitar-sustitucion',storeItemDevolver);}}]
    
    });
	
    storeItemDevolver = new Ext.data.Store({
    	storeId:'store-item-documento-solicitar',
        proxy:new Ext.data.HttpProxy({url:'../../clases/controladores/ControladorApp.php'}),
        remoteSort : true,
        restful : true,
        baseParams: {
                  moduleId: 'devolucion'
        },
        reader: new Ext.data.JsonReader({
            totalProperty: "totalCount",
            root: "matches" // ,id: "bl"
            }, devolucionForm.initRecordDef()),
        listeners: {
            load: function(Store ,records, options){ 
//                    console.log(Store);
            }
        }
    });
    
    gridItemDevolver= new Ext.grid.GridPanel({
        title:'Selecci&oacute;n de items',
        id: 'grid-item-documento-solicitar',
        //anchor:'96%',
        autoScroll:true,
        viewConfig:{forceFit:true},
        border:true,
        height: 310,
    	enableDragDrop   : true,
        stripeRows: true,
        loadMask:true,
        ds: storeItemDevolver,
        cm: devolucionForm.initColumnModel2(),
        autoExpandColumn : 'descripcion_item',
        remoteSort:true,
        shadow: false,
        shadowOffset: 0,
        selModel: new Ext.grid.RowSelectionModel({
            singleSelect:false,
            listeners: {
                rowselect: function(SelectionModel,rownumber,row){
                }
            }
        }),
        tbar:tbGridSust,
        listeners:{
        	dblclick:function (e){
				var rowSelected = gridItemDevolver.getSelectionModel().getSelected();
				var codValido = true;
				
                                rowSelected.data.cantidad = 1;
                                rowSelected.dirty = false;
				/*Ext.iterate(gridItemDevolucion.store.data.items, function(key, value) {                    	
            		
					if(key.data.item_id == rowSelected.data.item_id && key.data.documento == rowSelected.data.documento && key.data.linea == rowSelected.data.linea){
                                                codValido = false;
                                                Ext.MessageBox.alert('Advertencia', 'Este item ya fue elegido');
                                                return;
                                        }
            			
                                });*/
                                
                                
                                var MyData = {
                                        cantidad: 0,
                                        clase: "",
                                        codigo_item: "",
                                        descripcion_item: "",
                                        item_id: 0,
                                        motivo: "",
                                        marca_item : "",
                                        modelo_item : "",
                                        nota_entrega:"",
                                        sap_doc_entry:"",
                                        sap_line_num:"",
                                        __proto__: null
                                    };
                                
                                MyData.cantidad = rowSelected.data.cantidad;
                                MyData.clase = rowSelected.data.clase;
                                MyData.codigo_item = rowSelected.data.codigo_item;
                                MyData.descripcion_item = rowSelected.data.descripcion_item;
                                MyData.item_id = rowSelected.data.item_id;
                                MyData.motivo = rowSelected.data.motivo;
                                MyData.marca_item = rowSelected.data.marca_item;
                                MyData.modelo_item = rowSelected.data.modelo_item;
                                MyData.nota_entrega = rowSelected.data.nota_entrega;
                                MyData.sap_doc_entry = rowSelected.data.sap_doc_entry;
                                MyData.sap_line_num = rowSelected.data.sap_line_num;
                                
                                //var newRow = new storeItemDevolucion.recordType(rowSelected.data, recId); // create new record
                                var newRow = new  Ext.data.Record(MyData, "ext-new-record-devolucion"+recId); // create new record
                                ///console.log(newRow)
				recId++;
                                if(codValido){
					storeItemDevolucion.add(newRow);///rowSelected
                                        
                                        //recId
                                        //storeItemDevolucion.commit();
                                       //console.log(storeItemDevolver,storeItemDevolucion);
				}
    		}        	
        },
        bbar:[{
                xtype:'displayfield',
                value:'Haga doble click para agregar item a lista de devoluci&oacute;n'
            }]
    });
    
    /***
     * 
     * campos de direccion
     */
    var comboEstado = new Ext.form.ComboBox({
			    xtype:          'combo',
			    anchor:'96%',
			    mode:           'remote',
			    triggerAction:  'all',
			    fieldLabel:     'Estado (<a style="color:red">*</a>)',
			    name:           'estado',
			    hiddenName:     'estado',
			    id:     'estado',
			    displayField:   'estado',
			    //submitValue: true,
			    valueField : 'estado',
			    emptyText:'Seleccione un Estado',
                            allowBlank:false,
			    editable:false,
			    store:          new Ext.data.JsonStore({
			    	proxy:new Ext.data.HttpProxy({url:'../../clases/controladores/ControladorApp.php'}),
			    	autoLoad: true,
			    	baseParams: {
			    		accion:'Accion/Geografia/Obtener_Estados',
                                        output:'json'
			    	},
			        fields : [
			        			{name:'estado', type:'string'}
			                  ]
			    }),
                            listeners:{
				select:function(cmb,rec,idx){
					//storeDocs.load({params:{task:'get',carga_id:cargaID,tipo:newVal}});
					ciudad = Ext.getCmp('ciudad');
					ciudad.clearValue();
					ciudad.store.load({params:{task:'get',estado:this.getValue()}});
					ciudad.enable();
				}
                            }
                      });
                      
                      
   var storeCiudad = new Ext.data.JsonStore({
			    	proxy:new Ext.data.HttpProxy({url:'../../clases/controladores/ControladorApp.php'}),
			    	autoLoad: false,
			    	baseParams: {
			    		accion:'Accion/Geografia/Obtener_Ciudades',
                                        output:'json'
			    	},
			        fields : [
			        			{name:'ciudad', type:'string'}
			                  ]
			    });
                            
                       /*
                        *     
                        *          QUITAR ALLOW BLANK
                        *          
                        **/     
   var comboCiudad =  new Ext.form.ComboBox({
		    anchor:'96%',
		    mode:           'local',
		    triggerAction:  'all',
		    id: 'ciudad',
		    forceSelection: true,
                    allowBlank:true,
		    typeAhead: true,
		    editable:       false,
		    fieldLabel:     'Ciudad (<a style="color:red">*</a>)',
		    name:           'ciudad',
		    hiddenName:     'ciudad',
		    displayField:   'ciudad',
		    valueField:     'ciudad',
		    store:          storeCiudad,
		    disabled: true,
                    emptyText:'Seleccione una Ciudad'
		});                    
    
    var calle = new Ext.form.TextField({
        anchor:'96%',
        id: 'calle',
        name:'calle',
        emptyText:'Ingrese Nº o Nombre de Calle',
        allowBlank:false,
        fieldLabel:     'Calle (<a style="color:red">*</a>)',
        listeners:{
            focus:function(e){
                /*if(this.getValue() == "Ingrese Nº o Nombre de Calle")
                    this.setValue("");*/
            },
            blur:function(e){
                /*if(this.getValue() == "")
                    this.setValue("Ingrese Nº o Nombre de Calle");*/
            }
        }
    });
    
    var avenida = new Ext.form.TextField({
        anchor:'96%',
        id: 'avenida',
        name:'avenida',
        emptyText:'Ingrese Nº o Nombre de Avenida',
        allowBlank:false,
        fieldLabel:     'Avenida (<a style="color:red">*</a>)',
        listeners:{
            focus:function(e){
//                if(this.getValue() == "Ingrese Nº o Nombre de Avenida")
//                    this.setValue("");
            },
            blur:function(e){
//                if(this.getValue() == "")
//                    this.setValue("Ingrese Nº o Nombre de Avenida");
            }
        }
    });
    
    var sector = new Ext.form.TextField({
        anchor:'96%',
        id: 'sector',
        name:'sector',
        emptyText:'Ingrese Nombre de Sector',
        fieldLabel:     'Sector (<a style="color:red">*</a>)',
        allowBlank:false,
        listeners:{
            focus:function(e){
//                if(this.getValue() == "Ingrese Nombre de Sector")
//                    this.setValue("");
            },
            blur:function(e){
//                if(this.getValue() == "")
//                    this.setValue("Ingrese Nombre de Sector");
            }
        }
    });
    
    var tipo_locacion = new Ext.form.ComboBox({
                    anchor:'96%',
		    mode:           'remote',
		    triggerAction:  'all',
		    forceSelection: true,
		    typeAhead: true,
		    editable:       false,
                    allowBlank:false,
		    fieldLabel:     'Tipo Locaci&oacute;n (<a style="color:red">*</a>)',
		    name:           'tipo_loc_id',
                    id:             'tipo_loc_id',
		    hiddenName:     'tipo_loc_id',
		    displayField:   'locacion',
		    valueField:     'tipo_loc_id',
                    //value:'Selecione Tipo de Locación',
                    emptyText:'Selecione Tipo de Locación',
		    store:          new Ext.data.JsonStore({
		    	proxy:new Ext.data.HttpProxy({url:'../../clases/controladores/ControladorApp.php'}),
		    	autoLoad: true,
		    	baseParams: {
			    		accion:'Accion/Geografia/Obtener_Locacion',
                                        output:'json'},
		        fields : [
		                  	{name:'locacion', type:'string'},
		        		{name:'tipo_loc_id', type:'int'}
		                  ]
		    }),
		    listeners:{
				select:function(cmb,rec,idx){
					//storeDocs.load({params:{task:'get',carga_id:cargaID,tipo:newVal}});
//					docs = Ext.getCmp('documentos-tipo');
//					docs.clearValue();
//					docs.store.load({params:{task:'get',carga_id:cargaID,tipo:this.getValue()}});
//					docs.enable();
				}			
			}
		});
    
    var numero_locacion = new Ext.form.TextField({
        anchor:'96%',
        id: 'numero_locacion',
        name:'numero_locacion',
        emptyText:'Ingrese Número',
        fieldLabel:     'N&uacute;mero (<a style="color:red">*</a>)',
        allowBlank:false,
        listeners:{
            focus:function(e){
//                if(this.getValue() == "Ingrese Número")
//                    this.setValue("");
            },
            blur:function(e){
//                if(this.getValue() == "")
//                    this.setValue("Ingrese Número");
            }
        }
    });    
    
    
    var referencia = new Ext.form.TextField({
        anchor:'96%',
        id: 'referencia',
        name:'referencia',
        emptyText:'Ingrese Referencia',
        fieldLabel:'Referencia (<a style="color:red">*</a>)',
        allowBlank:false,
        listeners:{
            focus:function(e){
//                if(this.getValue() == "Ingrese Referencia")
//                    this.setValue("");
            },
            blur:function(e){
//                if(this.getValue() == "")
//                    this.setValue("Ingrese Referencia");
            }
        }
    }); 
    
    var observaciones = new Ext.form.TextArea({
        anchor:'90%',
        id: 'observaciones',
        name:'observaciones',
        fieldLabel:     'Observaciones',
        emptyText:'Ingrese Observaciones'
    });    
    
    var telefonos = new Ext.form.TextField({
        anchor:'90%',
        id: 'telefonos',
        name:'telefonos',
        emptyText:'Ingrese Telefonos',
        fieldLabel:     'Telefonos (<a style="color:red">*</a>)',
        allowBlank:false,
        listeners:{
            focus:function(e){
//                if(this.getValue() == "Ingrese Telefonos")
//                    this.setValue("");
            },
            blur:function(e){
//                if(this.getValue() == "")
//                    this.setValue("Ingrese Telefonos");
            }
        }
    }); 
    
    var botonDireccion = {
                                    xtype:'button',
                                    style:'margin-top:18px',
                                    icon: '../../imagenes/icons/folder_user.png',
                                    tooltip:'Direcciones anteriores',
                                    listeners:{
                                        click:function(){  
                                            storeDirecciones =  new Ext.data.Store({
                                                storeId:'store-direcciones-devolucion',
                                                proxy:new Ext.data.HttpProxy({url:'../../clases/controladores/ControladorApp.php'}),
                                                restful : true,
                                                autoLoad:false,
                                                baseParams: {
                                                          moduleId: 'sustitucion'
                                                },
                                                reader: new Ext.data.JsonReader({
                                                    totalProperty: "totalCount",
                                                    root: "matches" // ,id: "bl"
                                                    }, devolucionForm.initRecordDef()),
                                                listeners: {
                                                    load: function(Store ,records, options){ }
                                                }
                                            });

                                            gridDirecciones = new Ext.grid.GridPanel({
                                                id: 'grid-direcciones-devolucion',
                                                autoScroll:true,
                                                viewConfig:{forceFit:true},
                                                border:true,
                                                height: 300,
                                                stripeRows: true,
                                                loadMasck:true,
                                                ds: storeDirecciones,
                                                cm: devolucionForm.initColumnModel3(),
                                                remoteSort:true,
                                                shadow: false,
                                                shadowOffset: 0,
                                                sm:  new Ext.grid.RowSelectionModel({
                                                    singleSelect:true,
                                                    listeners: {
                                                        rowselect: function(SelectionModel,rownumber,row){
                                                        }
                                                    }
                                                }),
                                                listeners:{
                                                        dblclick:function(e){
                                                            //@TODO
                                                            var rowSelected = gridDirecciones.getSelectionModel().getSelected();
                                                            //console.log(rowSelected.data.locacion)
                                                            Ext.getCmp('ciudad').setDisabled(false);
                                                            Ext.getCmp('estado').setValue(rowSelected.data.estado);
                                                            Ext.getCmp('ciudad').setValue(rowSelected.data.ciudad);
                                                            Ext.getCmp('sector').setValue(rowSelected.data.sector);
                                                            Ext.getCmp('avenida').setValue(rowSelected.data.avenida);
                                                            Ext.getCmp('calle').setValue(rowSelected.data.calle);
                                                            Ext.getCmp('tipo_loc_id').setValue(rowSelected.data.tipo_loc_id);
                                                            //Ext.getCmp('tipo_loc_id').setRawValue(rowSelected.data.locacion);
                                                            Ext.getCmp('numero_locacion').setValue(rowSelected.data.numero_locacion);
                                                            Ext.getCmp('referencia').setValue(rowSelected.data.referencia);
                                                            Ext.getCmp('nueva-direccion').setValue('0');
                                                            direccionesWin.close();
                                                        }
                                                },
                                                bbar:[{
                                                        xtype:'displayfield',
                                                        value:'Haga doble click para selecionar la direcci&oacute;n de la devoluci&oacute;n'
                                                    }]
                                            });
                                            
                                            var direccionesWin = new Ext.Window({
                                                draggable: true,
                                                modal: false,
                                                id: 'direcciones-win',
                                                layout: 'fit',
                                                maximizable:false,
                                                minHeight: 250,
                                                minWidth: 500,
                                                plain: false,
                                                resizable: true,
                                                items: [ gridDirecciones ],
                                                title: 'Direcciones Anteriores',
                                                height: 350,//490
                                                width: 400
                                            });
                                            
                                            direccionesWin.show();
                                            
                                            storeDirecciones.load({params:{                                                    
                                                accion:'Accion/Geografia/Obtener_Direcciones_Lider',
                                                output:'json'
                                            }});
                                        }
                                    }
                                };
    
    
    var tabForm = new Ext.FormPanel({
        url: '../../clases/controladores/ControladorApp.php',
        labelAlign: 'top',
        id: 'add-solicitud-sust-form',
        bodyStyle:'padding:5px',
        width: 800,
        items: [{
            layout:'column',
            border:false,
            items:[{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
			                {
                                                xtype:'fieldset',
                                            anchor:'96%',
                                            title: 'Direcci&oacute;n',
                                            //collapsible: true,
                                            autoHeight:true,
                                            items:[
                                                      {layout:'column',border:false,items:[{columnWidth:.8,layout:'form',border:false,items:[comboEstado]},{columnWidth:.1,layout:'form',border:false,items:[botonDireccion]},{columnWidth:.1,layout:'form',border:false,items:[botonDireccionMapa]}]},
                                                      comboCiudad,
                                                      sector,
                                                      avenida,
                                                      calle,
                                                      tipo_locacion,
                                                      numero_locacion,
                                                      referencia,
                                                      {xtype:'textfield',hidden:true,name:'nueva_direccion',id:'nueva-direccion',value:'1'},
                                                      telefonos,
                                                      observaciones
                                                  ]
                                        }
                                        //aqui grid
		                ]
            },{
                columnWidth:.03,
                layout: 'form',
                border:false,
                items: [
                        	{
                                    xtype:'button',
                                    style:'margin-top:240px',
                                    icon: '../../imagenes/icons/arrow_right.png',
                                    tooltip:'Agregar a devoluci&oacute;n',
                                    listeners:{
                                        click:function(){                                            
                                            var rowSelected = gridItemDevolver.getSelectionModel().getSelected();
                                            var codValido = true;
                                            rowSelected.data.cantidad = 1;
                                            rowSelected.dirty = false;
                                            /*Ext.iterate(gridItemDevolucion.store.data.items, function(key, value) {                    	

                                                    if(key.data.item_id == rowSelected.data.item_id && key.data.documento == rowSelected.data.documento && key.data.linea == rowSelected.data.linea){
                                                            codValido = false;
                                                            Ext.MessageBox.alert('Advertencia', 'Este item ya fue elegido');
                                                            return;
                                                    }

                                            });*/
                                            var MyData = {
                                                    cantidad: 0,
                                                    clase: "",
                                                    codigo_item: "",
                                                    descripcion_item: "",
                                                    item_id: 0,
                                                    motivo: "",
                                                    marca_item :"",
                                                    modelo_item: "",
                                                    nota_entrega :"",
                                                    sap_doc_entry: "",
                                                    sap_line_num: "",
                                                    __proto__: null
                                                };

                                            MyData.cantidad = rowSelected.data.cantidad;
                                            MyData.clase = rowSelected.data.clase;
                                            MyData.codigo_item = rowSelected.data.codigo_item;
                                            MyData.descripcion_item = rowSelected.data.descripcion_item;
                                            MyData.item_id = rowSelected.data.item_id;
                                            MyData.motivo = rowSelected.data.motivo;
                                            MyData.marca_item = rowSelected.data.marca_item;
                                            MyData.modelo_item = rowSelected.data.modelo_item;
                                            MyData.nota_entrega = rowSelected.data.nota_entrega;
                                            MyData.sap_doc_entry = rowSelected.data.sap_doc_entry;
                                            MyData.sap_line_num = rowSelected.data.sap_line_num;

                                            //var newRow = new storeItemDevolucion.recordType(rowSelected.data, recId); // create new record
                                            var newRow = new  Ext.data.Record(MyData, "ext-new-record-devolucion"+recId); // create new record
                                            ///console.log(newRow)
                                            recId++;
                                            if(codValido){
                                                    storeItemDevolucion.add(newRow);///rowSelected

                                                    //recId
                                                    //storeItemDevolucion.commit();
                                                   //console.log(storeItemDevolver,storeItemDevolucion);
                                            }
                                        }
                                    }
                                }
                       ]
            },{
                columnWidth:.57,
                layout: 'form',
                border:false,
                items: [
                                gridItemDevolver,
                                gridItemDevolucion
                       ]
            }]
        }],

        buttons: [{
            text: 'Guardar',
            listeners :{
                click : function( el, e ){
                    //console.log(NewGMapPanel.getMap().getCenter().toUrlValue());
                    if (tabForm.form.isValid() && devolucionForm.validaDireccion()) {// && devolucionForm.validaDireccion()
                                var i = 0;

                                articuloIdArray = new Array();
                                cantidadArray = new Array();
                                motivoArray = new Array();
                                claseArray = new Array();
                                notaArray = new Array();
                                sapDocEntryArray = new Array();
                                descripcionArray = new Array();
                                linesNumArray = new Array();
                                descripcionArray = new Array();
                                
                                var breakSubmit = false;
                                
                                Ext.iterate(gridItemDevolucion.store.data.items, function(key, value) {   
                                        //console.log(key.data.cantidad)
                                        if(key.data.cantidad == 0 || key.data.cantidad == '' || key.data.cantidad == undefined || key.data.cantidad == null){
                                            Ext.MessageBox.alert('Advertencia', 'Existen al menos 1 item con cantidad en 0. Por favor, verificar');
                                            breakSubmit = true;
                                            return;
                                        }
                                        if(key.data.motivo == ''){
                                            Ext.MessageBox.alert('Advertencia', 'Existen al menos 1 item sin motivo. Esta informaci&oacute; es obligatoria. Por favor, verificar');
                                            breakSubmit = true;
                                            return;
                                        }
                                        articuloIdArray.push(key.data.item_id);
                                        claseArray.push(key.data.clase);
                                        cantidadArray.push(key.data.cantidad); 
                                        motivoArray.push(key.data.motivo.replace("/,/gi","99_1_99_2_99_3_99"));
                                        descripcionArray.push(key.data.descripcion_item.replace(/,/gi,"99_1_99_2_99_3_99"));
                                        notaArray.push(key.data.nota_entrega);
                                        sapDocEntryArray.push(key.data.sap_doc_entry);
                                        linesNumArray.push(key.data.sap_line_num);
                                        Ncont++;
                                });

                                if(breakSubmit){
                                    return;                    			
                                }
                                Ncont = motivoArray.length;

                                if(Ncont == 0){
                                    Ext.MessageBox.alert('Advertencia', 'Debe ingresar items para la devoluci&oacute;n');
                                    return;
                                }
                                
                                Ext.MessageBox.buttonText.yes = "Sí";
                                Ext.MessageBox.buttonText.no = "No";
                                Ext.MessageBox.buttonText.cancel = "Cancelar";
                                Ext.MessageBox.show({
                                                    title:'Confirmación!',
                                                    msg: 'Está realizando una devoluci&oacute;n. <br />Desea continuar?',
                                                    buttons: Ext.MessageBox.YESNOCANCEL,
                                                   fn: function (val){

                                                         if(val == 'yes'){     	        		   				
                                                             tabForm.form.submit({
                                                                params:{
                                                                    accion: 'Accion/Devolucion/Crear_Devolucion',
                                                                    articulosEnSolicitar: [articuloIdArray],
                                                                    claseEnSolicitar:[claseArray],
                                                                    cantidadesEnSolicitar: [cantidadArray],
                                                                    motivoEnSolicitar: [motivoArray],
                                                                    notasEnSolicitar: [notaArray],
                                                                    sapDocEntriesEnSolicitar: [sapDocEntryArray],
                                                                    descripcionEnSolicitar: [descripcionArray],
                                                                    lineNumEnSolicitar: [linesNumArray],
                                                                    num_items: Ncont//,
                                                                    //gmap_lat_lon:NewGMapPanel.getMap().getCenter().toUrlValue()
                                                                },
                                                                waitMsg:'Insertando...',
                                                                    reset: true,
                                                                    failure: function(form_instance, action) {
                                                                                Ext.MessageBox.alert('Error', action.result.mensaje);
                                                                    },
                                                                    success: function(form_instance, action) {
                                                                        Ext.MessageBox.alert('Mensaje', action.result.mensaje+"\nSe creó la devoluci&oacute; # "+action.result.caso);

                                                                        tabForm.form.reset();                            	
                                                                        storeItemDevolver.removeAll();
                                                                        storeItemDevolucion.removeAll();
                                                                        Ext.getCmp('grid-item-asignado').store.reload();
                                                                        devolucionFormWin.hide();
                                                                    }
                                                                });
                                                            
                                                         }
                                                               },
                                                   icon: Ext.MessageBox.QUESTION
                                    });
                                
                    }
        			else{
                        Ext.MessageBox.alert('Error', 'Debe especificar todos los campos');
                    }
                }
            }
        },
        {
            text: 'Cancelar',
            listeners:{
	            click: function( el, e ){
	            	devolucionFormWin.hide();       	
	        	
	        	}
        	}
        }]
    });
    
    
    devolucionFormWin = new Ext.Window({
        closeAction:'hide',
        draggable: true,
        modal: true,
        id: 'devolucionForm-win',
        layout: 'fit',
        region:'center',
        maximizable:false,
        minHeight: 250,
        minWidth: 500,
        plain: false,
        resizable: true,
        items: [ tabForm ],
        constrainHeader:true,
        //constrain:true,
        /*style : {
          'position' : '50% 50%'  
        },*/
        x:100,
        y:100,
        title: 'C&aacute;lculo de Costos',
        height: 680,//490
        width: 1000
    });
    
    devolucionFormWin.show('viewport-home');
    
    homeAppPanel = new Ext.Viewport({
    		layout: 'border',
    		id:'viewport-home',
    		items: [devolucionFormWin],
            renderTo: 'container-calculo-costo',
            listeners:{
        		afterrender:function(cmp){
        			this.doLayout(true,false);
        		}
        	}
    });
}
};
}();
//
//Ext.onReady(devolucionForm.Init, devolucionForm, true);
//devolucionForm.Init();
Ext.onReady(homeApp.Init, homeApp, true);