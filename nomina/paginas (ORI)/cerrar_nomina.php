<?php 
require '../lib/database.php';

set_time_limit(0);

$db = new Database($_SESSION['bd']);

// Cerrar la Nómina
$sql = "SELECT periodo_ini, periodo_fin, anio, mes, periodo, frecuencia 
	    FROM   nom_nominas_pago 
		WHERE  codnom='{$_GET['codigo_nomina']}' AND codtip='{$_SESSION['codigo_nomina']}'"; // tipnom='{$_SESSION['codigo_nomina']}'
$res = $db->query($sql);
$obj = $res->fetch_object();

// Generar netos
$sql = "SELECT ficha, cedula, cuentacob FROM nompersonal WHERE tipnom='{$_SESSION['codigo_nomina']}'";
$res = $db->query($sql);

$sqlNeto = "INSERT INTO nom_nomina_netos (codnom, tipnom, ficha, cedula, cta_ban, neto) 
            VALUES ";

$sqlNetoValues = array();

while($fila = $res->fetch_assoc())
{
	$sql2 = "SELECT tipcon, monto, codcon, valor 
	         FROM   nom_movimientos_nomina 
	         WHERE  codnom='{$_GET['codigo_nomina']}' AND ficha='{$fila['ficha']}'  
	         AND    tipnom='{$_SESSION['codigo_nomina']}'";
	
	$res2 = $db->query($sql2);

	$asignaciones = $deducciones = 0;
	
	if($res2->num_rows > 0)
	{
		while($fila2 = $res2->fetch_assoc())
		{
			if($fila2['tipcon']=='A')
				$asignaciones+=$fila2['monto'];
			elseif($fila2['tipcon']=='D')
				$deducciones+=$fila2['monto'];

			if($fila2['codcon']==114)
			{
				$sql3 = "UPDATE nompersonal 
				         SET    estado='Vacaciones' 
				         WHERE  tipnom='{$_SESSION['codigo_nomina']}' AND ficha='{$fila['ficha']}'";
				$res3 = $db->query($sql3);
 
				// $sql3 = "UPDATE nom_progvacaciones SET estado='Pagada' WHERE ficha='{$fila['ficha']}'";
			    $sql3 = "UPDATE nom_progvacaciones 
			             SET    estado='Pagada' 
			             WHERE  ficha='{$fila['ficha']}' AND fechavac='{$obj->periodo_ini}'";
				$res3 = $db->query($sql3);			 
			}

			if($fila2['codcon']>=500 && $fila2['codcon']<=599)
			{
				$sql3 = "UPDATE nomprestamos_detalles 
					     SET    estadopre='Cancelada' 
						 WHERE  codnom='{$_SESSION['codigo_nomina']}' AND ficha='{$fila['ficha']}' 
						 AND    fechaven BETWEEN '{$obj->periodo_ini}' AND '{$obj->periodo_fin}'
						 AND    estadopre='Pendiente'";
				$res3 = $db->query($sql3);
				// Para optimizar la consulta crear el índice (Esta tabla no tiene primary key ni índices):
				// CREATE INDEX fechaven_idx ON nomprestamos_detalles (fechaven);
			}

			$sql3 = "SELECT cod_tac, operacion 
					 FROM   nomconceptos_acumulados 
				     WHERE  codcon='{$fila2['codcon']}'";
			$res3 = $db->query($sql3);
			 
			while($fila3 = $res3->fetch_assoc())
			{	
				if($fila3['cod_tac'] == 'LIQ')
				{
					$sql4 = "UPDATE nompersonal SET 
					         estado = 'Egresado', 
					         fecharetiro = sysdate() 
					         WHERE ficha = '{$fila['ficha']}'";
					$res4 = $db->query($sql4);	
				}

				$sql4 = "INSERT INTO nomacumulados_det 
				         (ficha, ceduda, anioa, mesa, fecha, cod_tac, montototal, codcon, codnom, tipnom, operacion, refer)
				         VALUES 
				         ('{$fila['ficha']}', '{$fila['cedula']}', '{$obj->anio}', '{$obj->mes}', 
				          '".date('Y-m-d')."', '{$fila3['cod_tac']}', '{$fila2['monto']}', '{$fila2['codcon']}', 
				          '{$_GET['codigo_nomina']}', '{$_SESSION['codigo_nomina']}', '{$fila3['operacion']}',
				          '{$fila2['valor']}')";
				$res4 = $db->query($sql4) ;
			}
		}	

		$neto=$asignaciones-$deducciones;

		if($neto!=0)
		{
			// $sql2 = "INSERT INTO nom_nomina_netos (codnom, tipnom, ficha, cedula, cta_ban, neto) 
			//          VALUES 
			//          ('{$_GET['codigo_nomina']}', '{$_SESSION['codigo_nomina']}', '{$fila['ficha']}',
			//           '{$fila['cedula']}', '{$fila['cuentacob']}', '{$neto}')";
			// $res2 = $db->query($sql2);

	        $sqlNetoValues[] = "('{$_GET['codigo_nomina']}', '{$_SESSION['codigo_nomina']}', '{$fila['ficha']}', ".
	                            "'{$fila['cedula']}', '{$fila['cuentacob']}', '{$neto}')";
		}

		//-----------------------------------------------------------------------------------------------------------
		// Actualizar campos adicionales (97, 98 y 99) al cerrar la Planilla de Vacaciones
		if($obj->frecuencia==8)
		{
			$adicionales = array('97'=>$obj->periodo_ini, '98'=>$obj->periodo_ini, '99'=>$neto);

			foreach ($adicionales as $id => $valor) 
			{
				// Consultar si el campo adicional ya existe para la ficha actual
				$sql2 = "SELECT valor 
				         FROM   nomcampos_adic_personal
				         WHERE  ficha='{$fila['ficha']}' AND id='{$id}' AND tiponom='{$_SESSION['codigo_nomina']}'";
				$res2 = $db->query($sql2);

				if($res2->num_rows == 0)
				{
					// Registrar el campo adicional
					$sql3 = "INSERT INTO `nomcampos_adic_personal` 
							 (`ficha`, `id`, `valor`, `tipo`, `tiponom`) 
							 VALUES 
							 ('{$fila['ficha']}', '{$id}', '{$valor}', NULL, '{$_SESSION['codigo_nomina']}')";
					$res3 = $db->query($sql3);
				}
				else
				{
					// Actualizar el campo adicional
					$sql3 = "UPDATE `nomcampos_adic_personal` SET
							 valor='{$valor}'
							 WHERE ficha='{$fila['ficha']}' AND id='{$id}' AND tiponom='{$_SESSION['codigo_nomina']}'";
					$res3 = $db->query($sql3);
				}
			}
		}	
		//-----------------------------------------------------------------------------------------------------------
	}
}

$countNeto = count($sqlNetoValues);

if($countNeto > 0)
{
	$sqlNeto .= implode(",", $sqlNetoValues);
	$res = $db->query($sqlNeto);
}

$sql = "UPDATE nomperiodos 
        SET    status='Cerrado' 
        WHERE  codfre='{$obj->frecuencia}' AND anio='{$obj->anio}' AND nper='{$obj->periodo}'";
$res = $db->query($sql);

$sql = "UPDATE nom_nominas_pago SET 
        status='C', 
        libre=0 
        WHERE codnom='{$_GET['codigo_nomina']}' AND codtip='{$_SESSION['codigo_nomina']}'"; // tipnom='{$_SESSION['codigo_nomina']}'
$res = $db->query($sql);
?>