<?php
if(!isset($_SESSION)) session_start();

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Panama');

require('vendor/tcpdf/tcpdf.php');
require('vendor/fpdi/fpdi.php');
require_once('../../lib/database.php');

class MYPDF extends TCPDF
{
    var $_template;
	
    public function Header() 
    {
        global $carta_id, $db;

        /* Lo primero que se debe hacer es consultar las opciones del modelo de constancia   */
        /* para saber si se va a usar el header y pie de página de la configuración general, */
        /* si se va a usar un template o si no se desea usar ninguno de los dos              */

        $sql = "SELECT configuracion, template
                FROM   nomtipos_constancia
                WHERE  codigo='".$carta_id."'";
        $res = $db->query($sql);
        $carta = $res->fetch_object();

        if($carta->configuracion == 'Template')
        {
            if (is_null($this->_template)) 
            {
                $template = "templates/" . $carta->template;

                if (file_exists($template)) 
                {
                    $this->setSourceFile($template);
                    $this->_template = $this->importPage(1);
                    $size = $this->useTemplate($this->_template);
                }
            }    
        }

       //  if($carta->configuracion == 'Header')
       //  {
		    	// $res=query('SELECT encabezado FROM nomconf_constancia', $conexion);
		    	// $fila=fetch_array($res);
		    	// $encabezado=$fila['encabezado'];
		         
		    	// if(!empty($encabezado))
		    	// {
		    	// 	$this->SetTopMargin(0);
	    		// 	$this->SetLeftMargin(0);
	    		// 	$this->SetRightMargin(0);
		    	// 	$encabezado=str_replace('"', "'", $encabezado);
		    	// 	eval("\$html = \"$encabezado\";");
		    	// 	$html=str_replace("'", '"', $html);
		    	// 	$this->writeHTML($html, true, false, true, false, '');
		    	// }
       //  }        
    }

    public function Footer()
    {
    	// $conexion = conexion();

     //    $res = query('SELECT configuracion 
     //                  FROM   nomtipos_constancia 
     //                  WHERE  codigo='. $GLOBALS['carta_id'], $conexion);
     //    $fila = fetch_array($res);
     //    $configuracion = utf8_encode($fila['configuracion']);

     //    if($configuracion == 'Header')
     //    {        
	    // 	$res=query('SELECT pie_pagina FROM nomconf_constancia', $conexion);
	    // 	$fila=fetch_array($res);
	    // 	$pie_pagina=$fila['pie_pagina'];

	    // 	if(!empty($pie_pagina))
	    // 	{
	    // 		$this->SetY(-43);
	    // 		$this->SetLeftMargin(0);
	    // 		$this->SetRightMargin(0);
	    // 		$pie_pagina=str_replace('"', "'", $pie_pagina);
	    // 		eval("\$html = \"$pie_pagina\";");
	    // 		$html=str_replace("'", '"', $html);
	    // 		$this->writeHTML($html, true, false, true, false, '');
	    // 	}
	    // }        
    }
}
?>