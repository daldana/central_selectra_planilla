<?php
session_start();
ob_start();

include("func_bd.php"); 
?>
<script>

function AbrirListPersonal()
{
AbrirVentana('seleccionar_constancia_tipo.php',660,800,0);
}
function AbrirListPersonal2()
{
AbrirVentana('list_personal.php',660,800,0);
}
function AbrirListGeneral()
{
//sueldo promedio
AbrirVentana('list_personal_general.php?tipo=1',660,800,0);
}
function AbrirReporte()
{
	//alert(document.form1.cboTipoNomina.value);
	AbrirVentana('reporte_personal_x.php?tipo='+1,660,800,0);
}
function AbrirAsigDat()
{
	//alert(document.form1.cboTipoNomina.value);
	AbrirVentana('reporte_personal_x_csb.php?tipo='+1,660,800,0);
}
function AbrirPS()
{
	//alert(document.form1.cboTipoNomina.value);
	AbrirVentana('reporte_PS.php',660,800,0);
}
function AbrirAP()
{
	//alert(document.form1.cboTipoNomina.value);
	AbrirVentana('reporte_anticipos.php',660,800,0);
}
function AbrirIVSS(){
   AbrirVentana('list_personalIVSS.php',660,800,0);
}
function AbrirCargaFamiliar(){
   location.href='configurar_carga_familiar.php'
   //AbrirVentana('list_personalIVSS.php',660,800,0);
}
function AbrirPersonal(){
   location.href='configurar_personal.php'
   //AbrirVentana('list_personalIVSS.php',660,800,0);
}
function AbrirExpediente(){
   location.href='configurar_expediente.php'
   //AbrirVentana('list_personalIVSS.php',660,800,0);
}
function AbrirCumple(){
   location.href='configurar_cumple_dia.php?opcion=1'
   //AbrirVentana('list_personalIVSS.php',660,800,0);
}
function AbrirPadreMadre(){
	location.href='configurar_cumple_dia.php?opcion=2'
}
function AbrirListadoHijosTallas(){
  location.href='../tcpdf/reportes/reporte_listado_hijos.php';
}
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/css/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/select2/select2-metronic.css"/>
<link rel="stylesheet" href="../../includes/assets/plugins/data-tables/DT_bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../includes/css/components.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="../../includes/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
  .tile-icon {
    color: white;
    line-height: 125px; 
    font-size: 80px;
}
</style>
<!-- <link href="../../includes/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/> -->
<link href="../../includes/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link href="../../includes/assets/css/custom-header4.css" rel="stylesheet" type="text/css"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width"  marginheight="0">

<div class="page-container">
  <!-- BEGIN CONTENT -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <!-- BEGIN PAGE CONTENT-->
      <div class="row">
        <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet box blue">
            <div class="portlet-title">
              <h4>Integrantes (Personal)</h4>
            </div>
            <div class="portlet-body">
              <div class="row">

                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirCargaFamiliar();">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="fa fa-child"></i>
                          <h5>Carga Familiar</h5>                            
                      </div>
                    </div>
                  </a>
                </div>

                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirPersonal()">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="fa fa-user"></i>
                          <h5>Personal</h5>                            
                      </div>
                    </div>
                  </a>
                </div>

                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirExpediente();">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="fa fa-folder-open"></i>
                          <h5>Expediente Personal</h5>                            
                      </div>
                    </div>
                  </a>
                </div>

                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirCumple()">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="fa  fa-th-list"></i>
                          <h5>Listado de Cumpleaños</h5>                            
                      </div>
                    </div>
                  </a>
                </div>

                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirPadreMadre()">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="fa fa-users"></i>
                          <h5>Listado Madres y Padres</h5>                            
                      </div>
                    </div>
                  </a>
                </div>

                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirListPersonal2()">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="fa fa-file-text"></i>
                          <h5>Constancias de Trabajo </h5>                            
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2 text-center">
                  <a href="javascript:AbrirListadoHijosTallas()">
                    <div class="tile bg-blue">
                      <div class="tile-body">
                      <i class="icon-emoticon-smile"></i>
                          <h5>Niños por persona</h5>                            
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            <!-- END PORTLET BODY-->
            
          </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        </div>
      </div>
    <!-- END PAGE CONTENT-->
    </div>
  </div>
    <!-- END CONTENT -->
</div>



<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../includes/assets/plugins/respond.min.js"></script>
<script src="../../includes/assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="../../includes/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../includes/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../includes/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../includes/assets/plugins/data-tables/DT_bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
