<?php 
session_start();
ob_start();
$termino= $_SESSION['termino'];
require_once("func_bd.php");
require_once("../lib/common.php");

$sSql = new bd($_SESSION['bd']);
$sql = "SELECT 'Activos' tipo, count(cedula) cantidad FROM nompersonal WHERE estado='Activo' group by 1
union all 
(
SELECT 'En Vacaciones' tipo, count(cedula) cantidad FROM nompersonal WHERE estado='Vacaciones' group by 1
) ";        
$result = $sSql->query($sql);


$sql = "SELECT ficha,cedula,apenom,fecing,inicio_periodo,fin_periodo from nompersonal where tipemp != 'Fijo' and isnull(fin_periodo)!=1 and DATEDIFF(fin_periodo,curdate()) <=30";        
$resultContratos = $sSql->query($sql);

$sqld = "SELECT * from nompersonal as np
              INNER JOIN 
              nomexpediente as ne on np.cedula = ne.cedula
              INNER JOIN 
              nomexpediente_documentos  as nd ON   ne.cod_expediente_det =nd.id  
              WHERE np.estado='Activo' and isnull(nd.fecha_vencimiento)!=1 and DATEDIFF(nd.fecha_vencimiento,curdate()) <=30";        
$resultdocumentos = $sSql->query($sqld);


?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8">
<!--title>.: <?php echo $_SESSION['nombre_sistema']." :: Planilla"; ?> :.</title-->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="description">
<meta content="" name="author">

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
<link href="../../includes/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="../../includes/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../includes/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/select2/select2.css">
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/select2/select2-metronic.css">

<link rel="stylesheet" href="../../includes/assets/plugins/data-tables/DT_bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/bootstrap-datepicker/css/datepicker.css">
<link rel="stylesheet" type="text/css"href="../../includes/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css"/>
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="../../includes/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="../../includes/assets/css/style-metronic.css" rel="stylesheet" type="text/css">
<link href="../../includes/assets/css/style.css" rel="stylesheet" type="text/css">
<link href="../../includes/assets/css/style-responsive.css" rel="stylesheet" type="text/css">
<link href="../../includes/assets/css/plugins.css" rel="stylesheet" type="text/css">
<!--link href="../../includes/assets/css/themes/green.css" rel="stylesheet" type="text/css"-->
<link href="../../includes/assets/css/custom.css" rel="stylesheet" type="text/css">
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico">

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
    <script src="../../includes/assets/plugins/respond.min.js"></script>
    <script src="../../includes/assets/plugins/excanvas.min.js"></script> 
    <![endif]-->
<script src="../../includes/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="../../includes/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="../../includes/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="../../includes/assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../includes/assets/plugins/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="../../includes/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../../includes/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../../includes/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="../../includes/assets/plugins/flot/jquery.flot.min.js"></script>
<script src="../../includes/assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../includes/assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../includes/assets/plugins/flot/jquery.flot.stack.min.js"></script>
<script src="../../includes/assets/plugins/flot/jquery.flot.crosshair.min.js"></script>
<script src="../../includes/assets/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script>
    var DIR_INCLUDES = "../../";
</script>
<script src="../../includes/assets/scripts/core/app.js"></script>
<script src="../../includes/assets/scripts/core/helpers.js"></script>
<script src="../../includes/assets/scripts/core/datatable.js"></script>
<script src="../../includes/js/gui/numeros.js"></script>
<script src="../../includes/assets/scripts/custom/calendar-inicio.js" type="text/javascript"></script>
<script src="../../includes/assets/scripts/custom/components-pickers.js"></script>
<script src="../../includes/assets/scripts/custom/charts-inicio.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function() {    
       App.init();
    });
</script>
<!-- END JAVASCRIPTS -->
<style type="text/css">
    .page-content { 
        margin-left: 0px; 
        margin-top: 0px;
        min-height: 600px; 
        padding: 25px 20px 20px 20px;
    }
</style>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body>
<!-- BEGIN CONTAINER -->
<div>
    <div class="page-content" style="margin-left: 0px;">
        




            <div class="modal fade" id="portlet-config-calendar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Evento</h4>
                        </div>
                        <div class="modal-body">
                            
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue">Guardar</button>
                            <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div id="evento-modal" class="modal fade" role="dialog" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Evento</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="#" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Evento</label>
                                                    <div class="col-md-8">
                                                        <input type="text" value="" class="form-control" placeholder="Event Title..." id="nombre-evento"/><br/>
                                                    </div>
                                                    <label class="control-label col-md-4">Fecha y Hora Inicio</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date form_datetime input-large" data-date="2014-01-21T15:25:00Z">
                                                            <input type="text" size="16" readonly class="form-control" id="fecha-inicio-calendar">
                                                            <span class="input-group-btn">
                                                                <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                                            </span>
                                                            <span class="input-group-btn">
                                                                <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                    </div>
                                                    <br></br>
                                                    <label class="control-label col-md-4">Fecha y Hora Fin</label>
                                                    <div class="col-md-8">
                                                        <div class="input-group date form_datetime input-large" data-date="2014-01-21T15:25:00Z">
                                                            <input type="text" size="16" readonly class="form-control" id="fecha-fin-calendar">
                                                            <span class="input-group-btn">
                                                                <button class="btn default date-reset" type="button"><i class="fa fa-times"></i></button>
                                                            </span>
                                                            <span class="input-group-btn">
                                                                <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                    </div>
                                                    <br></br>
                                                    
                                                        <label class="control-label col-md-4">Color</label>
                                                        <div class="col-md-4">
                                                            <select id="color-evento" class="bs-select form-control" data-show-subtext="true">
                                                                <option style="background-color:yellow" value="yellow">Amarillo</option>
                                                                <option style="background-color:#35aa47;" value="green">Verde</option>
                                                                <option style="background-color:#4b8df8;" value="blue">Azul</option>
                                                                <option style="background-color:#e02222;" value="red">Rojo</option>
                                                                <option style="background-color:#852b99;" value="purple">Morado</option>
                                                                <option style="background-color:#fafafa;" value="gray">Gris</option>
                                                            </select>
                                                        </div>
                                                    

                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn " data-dismiss="modal" aria-hidden="true">Cerrar</button>
                                            <button class="btn blue btn-primary" data-dismiss="modal" onclick="javascript:CalendarIndex.guardarEvento()">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <!-- BEGIN PORTLET-->                    
                    <a href="#evento-modal" data-toggle="modal" class="btn green">
                    Agregar Evento +
                    </a>
                    <div class="form-group">
                        <label>Calendarios</label>
                        <select id="tipo_calendario" class="form-control" onchange="CalendarIndex.initCalendar();">
                            <option value="0">Mi Calendario</option>
                            <option value="1">De Empresa</option>
                            <!--option value="2">De Personal</option>
                            <option value="3">De Tipos Planilla</option-->
                        </select>
                    </div>
                    <div class="portlet box grey calendar">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-calendar"></i>Calendario de Eventos
                            </div>
                        </div>
                        <div class="portlet-body light-grey">
                            <div id="calendar">
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
                <!-- Contratos por vencer -->
               <div class="row">
                 <div class="col-md-6">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Contratos Por Vencer
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>
                                         #
                                    </th>
                                    <th>
                                         Cedula
                                    </th>
                                    <th>
                                         Nombre
                                    </th>
                                    <th>
                                         Inicio
                                    </th>
                                    <th>
                                         Fin
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    while($fila = $resultContratos->fetch_assoc()){?>
                                        
                                    <tr>
                                        <td>
                                             <?php echo $fila['ficha'] ?>
                                        </td>
                                        <td>
                                             <?php echo $fila['cedula'] ?>
                                        </td>
                                        <td>
                                             <?php echo $fila['apenom'] ?>
                                        </td>
                                        <td>
                                             <?php echo $fila['fecing'] ?>
                                        </td>
                                        <td>
                                            <?php echo $fila['fin_periodo'] ?>
                                        </td>
                                    </tr>       
                                <?php
                                    }?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                 <div class="row">
                 <div class="col-md-5">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs"></i>Documentos Por Vencer
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>
                                         # 
                                    </th>
                                    <th>
                                         id
                                    </th>
                                    <th>
                                         Nombre
                                    </th>
                                  
                                    <th>
                                         Vence
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    while($fila = $resultdocumentos->fetch_assoc()){?>
                                        
                                    <tr>
                                        <td>
                                             <?php echo $fila['id'] ?>
                                        </td>
                                          <td>
                                             <?php echo $fila['apenom'] ?>
                                        </td>
                                        <td>
                                             <?php echo $fila['nombre_documento'] ?>
                                        </td>
                                        <td>
                                             <?php echo $fila['fecha_vencimiento'] ?>
                                        </td>
                                      
                                    </tr>       
                                <?php
                                    }?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
              
             </div>
                 <!--  Activo y Vacaciones-->
                    <div class="col-md-6">
                       <div class="portlet box blue">
                          <div class="portlet-title">
                              <div class="caption">
                                 <i class="fa fa-reorder"></i>Empleados / Vacaciones
                               </div>
                           </div>
                        <div class="portlet-body">
                            <h4>.</h4>
                            <div id="pie_chart_1" class="chart"> </div>
                        </div>
                    </div>
                 



           
<!--[if lt IE 9]>
<script src="../../../../includes/assets/plugins/respond.min.js"></script>
<script src="../../../../includes/assets/plugins/excanvas.min.js"></script> 
<![endif]-->

<script type="text/javascript">
    var dataEmpleadosVacaciones = Array();
    <?php
        while($fila = $result->fetch_assoc()){?>
            descripcionV = "<?php echo $fila['tipo'] ?>";
            cantidadV = "<?php echo $fila['cantidad'] ?>";
            
            dataEmpleadosVacaciones.push({
                label:descripcionV,
                data: cantidadV
            });      
    <?php
        }?>

    var DB_CONNECT = "";

    jQuery(document).ready(function() {       
            CalendarIndex.initCalendar();
            ComponentsPickers.init();
            Charts.initPieCharts();
            //Charts.initBarCharts();

    });
</script>





    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<!-- END BODY -->
</body>
<!-- END BODY -->
</html>