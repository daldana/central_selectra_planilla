<?php
$path = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
$ruta = dirname(dirname(__FILE__));

require_once $path . "/generalp.config.inc.php";
require      $ruta . "../vendor/autoload.php"; // Incluimos librerias de composer

if (!isset($_SESSION)) session_start();

date_default_timezone_set('America/Caracas');

$params = array(
    'dbname'   => $_SESSION['bd'],
    'user'     => DB_USUARIO,
    'password' => DB_CLAVE,
    'host'     => DB_HOST,
    'driver'   => 'pdo_mysql', 
	'charset'  => 'utf8',    
);

$conexion =  \Doctrine\DBAL\DriverManager::getConnection($params, new \Doctrine\DBAL\Configuration());