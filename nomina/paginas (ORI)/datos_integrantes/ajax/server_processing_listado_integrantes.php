<?php
    $ruta = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
    $ruta = str_replace('\\', '/', $ruta);
    require_once $ruta.'/generalp.config.inc.php';
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Easy set variables
     */
    
    /* Array of database columns which should be read and sent back to DataTables. Use a space where
     * you want to insert a non-database field (for example a counter or static image)
     */
    $aColumns = array( 'descrip', 'ficha', 'cedula', 'apenom', 'estado', 'nomposicion_id');
    
    /* Indexed column (used for fast and accurate table cardinality) */
    $sIndexColumn = "personal_id";
    
    /* DB table to use */
    $sTable = "nomvis_integrantes";
    
    /* Database connection information */
    $db_user       = DB_USUARIO;
    $db_pass   = DB_CLAVE;
    $db_name         = $_SESSION['bd'];
    $db_host     = DB_HOST;
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP server-side, there is
     * no need to edit below this line
     */
    error_reporting(E_ALL);
    /* 
     * MySQL connection
     */
    $conexion =  mysqli_connect( $db_host, $db_user, $db_pass, $db_name  ) or
        die( 'Could not open connection to server' );    

    mysqli_query($conexion, 'SET CHARACTER SET utf8');
    
    /* 
     * Paging
     */
    $sLimit = "";
    if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
    {
        $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
    }
    
    
    /*
     * Ordering
     */
    $sOrder = "";
    if ( isset( $_GET['iSortCol_0'] ) )
    {
        $sOrder = "ORDER BY  ";
        for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
        {
            if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
            {
                $sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
            }
        }
        
        $sOrder = substr_replace( $sOrder, "", -2 );
        if ( $sOrder == "ORDER BY" )
        {
            $sOrder = "";
        }
    }
    
    
    /* 
     * Filtering
     * NOTE this does not match the built-in DataTables filtering which does it
     * word by word on any field. It's possible to do here, but concerned about efficiency
     * on very large tables, and MySQL's regex functionality is very limited
     */
    $sWhere = "";
    if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
    {
        $sWhere = "WHERE (";
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string($conexion, $_GET['sSearch'])."%' OR ";
        }
        $sWhere = substr_replace( $sWhere, "", -3 );
        $sWhere .= ')';
    }
    
    /* Individual column filtering */
    for ( $i=0 ; $i<count($aColumns) ; $i++ )
    {
        if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
        {
            if ( $sWhere == "" )
            {
                $sWhere = "WHERE ";
            }
            else
            {
                $sWhere .= " AND ";
            }
            $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysqli_real_escape_string($conexion, $_GET['sSearch_'.$i])."%' ";
        }
    }
    
    
    /*
     * SQL queries
     * Get data to display
     */
    $sQuery = "
        SELECT SQL_CALC_FOUND_ROWS personal_id, `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
        FROM   $sTable
        $sWhere
        $sOrder
        $sLimit
        ";
    $rResult = mysqli_query( $conexion, $sQuery ) or die(mysqli_error($conexion));
    //echo $sQuery;
    /* Data set length after filtering */
    $sQuery = "
        SELECT FOUND_ROWS()
    ";
    $rResultFilterTotal = mysqli_query( $conexion, $sQuery ) or die(mysqli_error($conexion));
    $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
    $iFilteredTotal = $aResultFilterTotal[0];
    
    /* Total data set length */
    $sQuery = "
        SELECT COUNT(`".$sIndexColumn."`)
        FROM   $sTable
    ";
    $rResultTotal = mysqli_query( $conexion, $sQuery ) or die(mysql_error($conexion));
    $aResultTotal = mysqli_fetch_array($rResultTotal);
    $iTotal = $aResultTotal[0];
    
    
    /*
     * Output
     */
    $output = array(
        "sEcho" => intval($_GET['sEcho']),
        "iTotalRecords" => $iTotal,
        "iTotalDisplayRecords" => $iFilteredTotal,
        "aaData" => array()
    );
    
    while ( $aRow = mysqli_fetch_array( $rResult ) )
    {
        $row = array(); 
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
                $row[] = $aRow[ $aColumns[$i] ];
        }
        $row[] = "<a href=\"../ag_integrantes.php?ficha=".$aRow['ficha']."&edit&back_listado_integrantes\" title=\"Editar\">" . 
                 "<img src=\"../../../includes/imagenes/icons/pencil.png\" alt=\"Editar\" width=\"16\" height=\"16\"></a>";
        $row[] = "<a href=\"../../fpdf/datos_personal.php?cedula=".$aRow['cedula']."&ficha=".$aRow['ficha']."\" title=\"Imprimir\">" . 
                 "<img src=\"../../../includes/imagenes/icons/printer.png\" alt=\"Imprimir\" width=\"16\" height=\"16\"></a>";
        $row[] = "<a href=\"../../expediente/expediente_list_ajax.php?cedula=".$aRow['cedula']."\" title=\"Ver Expediente\">" .
                 "<img src=\"../../../includes/imagenes/icons/folder_page.png\" alt=\"Ver Expediente\" width=\"16\" height=\"16\"></a>";
        $row[] = "<a href=\"../../../reporte_pub/colaborador.php?id=".$aRow['personal_id']."\" title=\"Movimientos Contraloría\">" .
                 "<img src=\"../images/view.gif\" alt=\"Movimientos Contraloría\" width=\"16\" height=\"16\"></a>";

        $output['aaData'][] = $row;
    }
    
    echo json_encode( $output );
?>