<?php 
session_start();
ob_start();

require_once("../lib/common.php");
include ("func_bd.php"); 

$conexion=new bd($_SESSION['bd']);

$termino = isset($_SESSION['termino']) ? $_SESSION['termino'] : '';
$registro_id = isset($_POST['registro_id']) ? $_POST['registro_id'] : '';	
$op = isset($_POST['op']) ? $_POST['op'] : '';

if ($op==3) //Se presiono el boton de eliminar
{
	$sql = "DELETE FROM nom_nominas_pago 
	        WHERE codnom='{$registro_id}' AND tipnom='{$_SESSION['codigo_nomina']}' 
	        AND   codtip='{$_SESSION['codigo_nomina']}'";			
	$conexion->query($sql);

	$sql = "DELETE FROM nom_movimientos_nomina 
	        WHERE codnom='{$registro_id}' AND tipnom='{$_SESSION['codigo_nomina']}'";
	$conexion->query($sql);

	activar_pagina('nomina_de_vacaciones.php');			 
} 

$sql = "SELECT * 
		FROM   nom_nominas_pago 
        WHERE  tipnom='{$_SESSION['codigo_nomina']}' AND codtip='{$_SESSION['codigo_nomina']}' 
        AND    frecuencia='8'
        ORDER BY codnom, descrip"; 
$res = $conexion->query($sql, "utf8");		

$sql_max = "SELECT max(codnom) as maximo 
            FROM nom_nominas_pago 
            WHERE tipnom='{$_SESSION['codigo_nomina']}' AND frecuencia=8";
$res_max  = $conexion->query($sql_max); 
$fila_max = $res_max->fetch_assoc();
?>
<?php include("../header4.php"); ?>
<link href="../../includes/assets/css/custom-datatables.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
  .portlet > .portlet-title > .actions > .btn.btn-sm {
    margin-top: -9px !important;
  }
  .text-middle{
  	vertical-align: middle !important
  }

  .ajustar-texto
  {
  	white-space: normal !important;
  }

  td.icono
  {
  	padding-left: 0px !important; 
  	padding-right: 0px !important; 
  	width: 10px !important;
  }
</style>
<script type="text/javascript" src="../lib/common.js"></script>
<script>
function GenerarNomina()
{
	AbrirVentana('barraprogreso_vacaciones.php', 150, 500, 0);
}

function CerrarVentana()
{
	javascript:window.close();
}

function showProcesando(){
	//console.log("Mostrar Procesando");
    App.blockUI({
        target: '#blockui_portlet_body',
        boxed: true,
        message: 'Procesando'
    });
}

function enviar(op, id, nomina, codtip)
{		
	if (op==1){		// Opcion de Agregar
		document.frmPrincipal.op.value=op;
		document.frmPrincipal.action="ag_nomina_vacaciones.php";
		document.frmPrincipal.submit();	
	}
	if (op==2){	 	// Opcion de Modificar
		document.frmPrincipal.registro_id.value=id;		
		document.frmPrincipal.op.value=op;
		document.frmPrincipal.action="ag_nomina_vacaciones.php";
		document.frmPrincipal.submit();		
	}
	if (op==3){		// Opcion de Eliminar
		if (confirm("\u00BFEst\u00E1 seguro que desea eliminar el registro?"))
		{					
			document.frmPrincipal.registro_id.value=id;
			document.frmPrincipal.op.value=op;
  			document.frmPrincipal.submit();
		}		
	}
	
	if (op==4){		// Generar Nómina
		document.frmPrincipal.registro_id.value=id;
		document.frmPrincipal.codigo_nomina.value=nomina;
		AbrirVentana('barraprogreso_vacaciones.php?registro_id='+id+'&codigo_nomina='+nomina, 180, 480, 0);
	}
	
	if (op==5){	// Movimiento de Nómina
		document.location.href="movimientos_nomina_pago.php?codigo_nomina="+id+"&codt="+codtip+"&vac="+1;	
	}
	if (op==6){	//CERRAR NÓMINA
		if (confirm("\u00BFEst\u00E1 seguro que desea cerrar esta <?php echo $termino; ?>?"))
		{
			showProcesando();

			var cerrar_nomina=abrirAjax()
			cerrar_nomina.open("GET", "cerrar_nomina.php?codigo_nomina="+id, true)
			cerrar_nomina.onreadystatechange=function() 
			{
				if (cerrar_nomina.readyState==4)
				{
					document.location.href="nomina_de_vacaciones.php"
				}
			}
			cerrar_nomina.send(null);
		}
	}
	if (op==7){	//CERRAR NÓMINA
		showProcesando();
		
		var nomina=abrirAjax()
		nomina.open("GET", "abrir_nomina.php?codigo_nomina="+id, true)
		nomina.onreadystatechange=function() 
		{
			if (nomina.readyState==4)
			{
				document.location.href="nomina_de_vacaciones.php"
			}
		}
		nomina.send(null);		
	}
}
</script>
<div class="page-container">
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<a href="nomina_de_pago.php"><img src="images/Clipboard.png" width="23" height="21"/></a> Lista de <?php echo $termino; ?>s de Vacaciones: <?php echo $_SESSION['nomina']; ?>
							</div>
							<div class="actions">
								<a class="btn btn-sm blue"  onclick="javascript: window.location='ag_nomina_vacaciones.php'">
									<i class="fa fa-plus"></i> Agregar
								</a>
								<a class="btn btn-sm blue"  onclick="javascript: window.location='submenu_vacaciones.php'">
									<i class="fa fa-arrow-left"></i> Regresar
								</a>
							</div>
						</div>
						<div class="portlet-body" id="blockui_portlet_body">
							<form action="" method="post" name="frmPrincipal" id="frmPrincipal">
							<table class="table table-striped table-bordered table-hover" id="table_datatable">
							<thead>
							<tr>
								<th class="text-center text-middle"><?php echo $termino; ?></th>
								<th class="text-center text-middle ajustar-texto">Tipo <?php echo $termino; ?></th>
								<th class="text-center text-middle" >Descripci&oacute;n</th>
								<th class="text-center text-middle">Estado</th>
								<th class="text-center text-middle">Inicio</th>
								<th class="text-center text-middle">Final</th>
								<th class="text-center text-middle">Pago</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
							</thead>
							<tbody>
							<?php								
								while( $fila = $res->fetch_assoc() )
								{ 
								?>
									<tr class="odd gradeX">
										<td class="text-center ajustar-texto"><?php echo $fila['codnom']; ?></td>
										<td class="text-center ajustar-texto"><?php echo $fila['tipnom']; ?></td>
										<td class="ajustar-texto"><?php echo $fila['descrip']; ?></td>
										<td class="text-center ajustar-texto"><?php echo $fila['status']; ?></td>
										<td class="text-center ajustar-texto"><?php echo date("d/m/Y",strtotime($fila['periodo_ini']));?></td>
										<td class="text-center ajustar-texto"><?php echo date("d/m/Y",strtotime($fila['periodo_fin']));?></td>
										<td class="text-center ajustar-texto"><?php echo date("d/m/Y",strtotime($fila['fechapago']));?></td>
										<td class="text-center icono"><a href="javascript:enviar(5,<?php echo $fila['codnom']; ?>,<?php echo $_SESSION['codigo_nomina']; ?>,<?php echo $fila['codtip']; ?>);" title="Movimientos"><img src="images/view.gif" width="16" height="16"></a></td>

										<?php
										if($fila["status"]=="A")
										{
										?>
											<td class="text-center icono"><a href="javascript:enviar(4, <?php echo $fila['codnom']; ?>,<?php echo $_SESSION['codigo_nomina']?>,0);" title="Generar <?php echo $termino; ?>"><img src="img_sis/ico_propiedades.gif" width="15" height="15" ></a></td>
											<td class="text-center icono"><a href="javascript:enviar(2, <?php echo $fila['codnom']; ?>,0,0);" title="Consutar <?php echo $termino; ?>"><img src="img_sis/ico_list.gif"   width="15" height="15"></a></td>
											<td class="text-center icono"><a href="javascript:enviar(3, <?php echo $fila['codnom']; ?>,0,0);" title="Eliminar <?php echo $termino; ?>"><img src="../imagenes/delete.gif" width="16" height="16"></a></td>
											<td class="text-center icono"><a href="javascript:enviar(6, <?php echo $fila['codnom']; ?>,0,0);" title="Cerrar <?php echo $termino; ?>"><img src="../imagenes/cancel.gif" width="16" height="16"></a></td>
										<?php
										}
										else
										{
											if($fila_max['maximo']==$fila['codnom'])
											{
												?><td class="text-center icono"><a href="javascript:enviar(7,<?php echo $fila['codnom']; ?>,0,0)" title="Abrir <?php echo $termino; ?>"><img src="../imagenes/ok.gif" width="16" height="16"></a></td><?php												
											}
											else{ ?><td>&nbsp;</td><?php } ?>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										<?php
										}?>
									</tr>
								  <?php							
								}
							?>
							</tbody>
							</table>
							    <input name="codigo_nomina" type="hidden" value="">
								<input name="registro_id" type="hidden" value="">
								<input name="op" type="hidden" value="">
								<input name="marcar_todos" type="hidden" value="1">	
							</form>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<?php include("../footer4.php"); ?>
<script type="text/javascript">
$(document).ready(function() { 

   	$('#table_datatable').DataTable({
    	"iDisplayLength": 25,
    	"sPaginationType": "bootstrap_extended", 
    	"aaSorting":[],
        "oLanguage": {
        	"sSearch": "<img src='../../includes/imagenes/icons/magnifier.png' width='16' height='16' > Buscar:",
            "sLengthMenu": "Mostrar _MENU_",
            "sInfoEmpty": "",
            "sInfo":"Total _TOTAL_ registros",
            "sInfoFiltered": "",
  		    "sEmptyTable":  "No hay datos disponibles",
            "sZeroRecords": "No se encontraron registros",
            "oPaginate": {
                "sPrevious": "P&aacute;gina Anterior",
                "sNext": "P&aacute;gina Siguiente",
                "sPage": "P&aacute;gina",
                "sPageOf": "de",
            }
        },
        "aLengthMenu": [ 
            [5, 10, 25, 50,  -1],
            [5, 10, 25, 50, "Todos"]
        ],
        "aoColumnDefs": [
            { 'bSortable': false, 'bSearchable': false, 'aTargets': [7, 8, 9, 10, 11] }
        ],
		"fnDrawCallback": function() {
		    $('#table_datatable_filter input').attr("placeholder", "Escriba frase para buscar");
		}
	});

	$('#table_datatable_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); 
	$('#table_datatable_wrapper .dataTables_length select').addClass("form-control input-xsmall"); 
});
</script>
</body>
</html>