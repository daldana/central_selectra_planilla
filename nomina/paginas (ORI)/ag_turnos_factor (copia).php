<?php 
require_once '../../generalp.config.inc.php';
session_start();
ob_start();

	include ("../header.php");
	include("../lib/common.php");
	include("func_bd.php");	
	ini_set("memory_limit","16M");
	$conexion=conexion();
	

?>

<script>

function Enviar(){					
	
	if (document.frmAgregarProfesion.registro_id.value==0){ 
		document.frmAgregarProfesion.op_tp.value=1
	}
	else{ 	
		document.frmAgregarProfesion.op_tp.value=2}		
	
	if (document.frmAgregarProfesion.txtdescripcion.value==0)
	{
		document.frmAgregarProfesion.op_tp.value=-1
		alert("Debe ingresar una descripción valida. Verifique...");			
	}				
	
}


</script>
<script language="javascript" type="text/javascript" src="datetimepicker.js">
//Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
//Script featured on JavaScript Kit (http://www.javascriptkit.com)
//For this script, visit http://www.javascriptkit.com 

</script>


<?php 
	
	
	
	$registro_id=$_POST[registro_id];
	$op_tp=$_POST[op_tp];
	$validacion=0;
	
	if ($registro_id==0) // Si el registro_id es 0 se va a agregar un registro nuevo
	{			
		
		if ($op_tp==1)
		{
		//nomturnos_factor        	factor_id,    turno_id,    descripcion,  valor,  fecha...
		$codigo_nuevo=AgregarCodigo("nomturnos_factor","factor_id");
		
		$query="insert into nomturnos_factor values 
		($codigo_nuevo,
		'$_POST[txtdescripcion]',0)";

				 

		$result=sql_ejecutar($query);	
		activar_pagina("turnos_factor_list.php"); //activar_pagina("turnos_factor_list.php");				
		}
	}
	else // Si el registro_id es mayor a 0 se va a editar el registro actual
	{	
		$query="select * from nomturnos_factor where factor_id=$registro_id";		
		$result=sql_ejecutar($query);	
		$row = mysqli_fetch_array ($result);	
		$codigo=$row[factor_id];	
		$turno_id=$row[turno_id];	
		$nombre=$row[descripcion];	
		$con_id=$row[con_id];	
		$valor=$row[valor];	
		$fecha=$row[fecha];			
	}	
		
		
	if ($op_tp==2)
		{					
			//nomturnos_factor        	factor_id,    turno_id,    descripcion,  valor,  fecha...				
				$query="UPDATE nomturnos_factor set factor_id=$registro_id,
				turno_id='$_POST[txtturno_id]'
				descripcion='$_POST[txtdescripcion]'
				con_id='$_POST[txtcon_id]'
				valor='$_POST[txtvalor]'
				fecha='$_POST[txtfecha]'
				where factor_id=$registro_id";				
				
				$result=sql_ejecutar($query);				
				activar_pagina("turnos_factor_list.php"); // activar_pagina("turnos_factor_list.php");										
		{			
	}
}	

?>
<form action="" method="post" name="frmAgregarProfesion" id="frmAgregarProfesion" enctype="multipart/form-data">
  <p>
  <input name="op_tp" type="Hidden" id="op_tp" value="-1">
  <input name="registro_id" type="Hidden" id="registro_id" value="<?php echo $_POST[registro_id]; ?>">
  </p>
  <table width="780" height="125" border="0" class="row-br">
    <tr>
      <td height="31" class="row-br"><font color="#000066"><strong>&nbsp;<font color="#000066">
        <?php
		if ($registro_id==0)
		{
		echo "Agregar Factor";
		}
		else
		{
		echo "Modificar Factor";
		}
		?>
      </font></strong></font></td>
    </tr>
    <tr>
      <td width="489" height="86" class="ewTableAltRow"><table width="790" border="0" bordercolor="#0066FF">
        <tr bgcolor="#FFFFFF">
          <td width="217" height="23" bgcolor="#FFFFFF" class="ewTableAltRow"><font size="2" face="Arial, Helvetica, sans-serif">Id del Factor:</font></td>
          <td width="390" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">
            <input name="txtcodigo" type="text" id="txtcodigo" disabled="disabled" style="width:100px" onKeyPress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" value="<?php if ($registro_id!=0){ echo $codigo; }  ?>" maxlength="10">
          </font></td>
          <td width="169" colspan="-1" bgcolor="#FFFFFF" class="ewTableAltRow" >&nbsp;</td>
        </tr>
	

					
        <tr valign="middle" bgcolor="#FFFFFF">
          <td height="24" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">Id  delTurno:</font></td>
          <td valign="middle" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">
            <input name="txtturno_id" type="text" id="txtturno_id"  style="width:300px" value="<?php if ($registro_id!=0){ echo $turno_id; }  ?>" maxlength="10">
          </font></td>
          <td width="169" colspan="-1" bgcolor="#FFFFFF" class="ewTableAltRow" >&nbsp;</td>
        </tr>
        
        <tr valign="middle" bgcolor="#FFFFFF">
          <td height="24" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">Descripci&oacute;n del Factor :</font></td>
          <td valign="middle" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">
            <input name="txtdescripcion" type="text" id="txtdescripcion" style="width:300px" value="<?php if ($registro_id!=0){ echo $nombre; }  ?>" maxlength="60">
          </font></td>
          <td width="169" colspan="-1" bgcolor="#FFFFFF" class="ewTableAltRow" >&nbsp;</td>
        </tr>
     
        <tr valign="middle" bgcolor="#FFFFFF">
          <td height="24" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">Id  de Conceptos:</font></td>
          <td valign="middle" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">
            <input name="txtcon_id" type="text" id="txtcon_id"  style="width:300px" value="<?php if ($registro_id!=0){ echo $con_id; }  ?>" maxlength="10">
          </font></td>
          <td width="169" colspan="-1" bgcolor="#FFFFFF" class="ewTableAltRow" >&nbsp;</td>
        </tr>
					   
        <tr valign="middle" bgcolor="#FFFFFF">
          <td height="24" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">Valor del Factor:</font></td>
          <td valign="middle" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">
            <input name="txtvalor" type="text" id="txtvalor" style="width:300px" value="<?php if ($registro_id!=0){ echo $valor; }  ?>" maxlength="60">
          </font></td>
          <td width="169" colspan="-1" bgcolor="#FFFFFF" class="ewTableAltRow" >&nbsp;</td>
        </tr>
     
		
 <tr valign="middle" bgcolor="#FFFFFF">
          <td height="24" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">Fecha :</font></td>
          <td valign="middle" bgcolor="#FFFFFF" class="ewTableAltRow" ><font size="2" face="Arial, Helvetica, sans-serif">
            <input name="txtfecha" type="text" id="txtfecha" style="width:300px" value="<?php if ($registro_id!=0){ echo $fecha; }  ?>" maxlength="60">
          </font></td>
          <td width="169" colspan="-1" bgcolor="#FFFFFF" class="ewTableAltRow" >&nbsp;</td>
        </tr>
     
		
		
        <tr bgcolor="#FFFFFF">
          <td height="24" bgcolor="#FFFFFF" class="ewTableAltRow">&nbsp;</td>
          <td colspan="2" bgcolor="#FFFFFF" class="ewTableAltRow"><div align="right"><font size="2" face="Arial, Helvetica, sans-serif"></font>
                  <table width="85" border="0">
                    <tr>
                      <td width="39"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif">
                          <?php btn('cancel','history.back();',2) ?>
                      </font></div></td>
                      <td width="36"><div align="center"><font size="2" face="Arial, Helvetica, sans-serif">
                          <?php btn('ok','Enviar(); document.frmAgregarProfesion.submit();',2) ?>
					
                      </font></div></td>
                    </tr>
                  </table>
            <font size="2" face="Arial, Helvetica, sans-serif"></font></div></td>
        </tr>
      </table>      </td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>  
</form>
<p>&nbsp;</p>
</body>
</html>