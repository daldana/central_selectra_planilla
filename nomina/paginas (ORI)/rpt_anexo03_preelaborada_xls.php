<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Panama');

if (PHP_SAPI == 'cli')
	die('Este ejemplo solo se debe ejecutar desde un navegador Web');

include('../lib/common_excel.php');
include('lib/php_excel.php');
include("funciones_nomina.php");
$conexion=conexion();

$codtip=$_GET['codtip'];
$mesano1=$_GET['mesano'];
$mesano2=$_GET['mesano1'];
$ma1=explode("/",$mesano1);
$ma2=explode("/",$mesano2);

require_once 'phpexcel/Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Selectra")
							 ->setLastModifiedBy("Selectra")
							 ->setTitle("Fondo de Cesantia")
							 ->setSubject("Office 2007 XLSX Test Document");

$sql = "SELECT e.nom_emp as empresa, e.rif, e.tel_emp as telefono, e.dir_emp as direccion, e.ciu_emp as ciudad, 
               e.edo_emp, e.imagen_izq as logo
		FROM   nomempresa e";

$res=query($sql, $conexion);

$fila=fetch_array($res);
$logo=$fila['logo'];

//$sql2 = "SELECT np.periodo_ini as desde, np.periodo_fin as hasta  FROM nom_nominas_pago np 
//         WHERE  np.codnom=".$codnom." AND np.tipnom=".$codtip;
//$res2=query($sql2, $conexion);
//$fila2=fetch_array($res2);

/*$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setCoordinates('B2');
$objDrawing->setPath('../imagenes/'.$logo);
//$objDrawing->setResizeProportional(true);
$objDrawing->setHeight(80);
//$objDrawing->setAutoSize(220);
$objDrawing->setOffsetX(0);
$objDrawing->setOffsetY(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
*/
//$objDrawing->setHeight(36);
//$objPHPExcel->getActiveSheet()->getHeaderFooter()->addImage($objDrawing, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);

$objPHPExcel->setActiveSheetIndex(0); 

$objPHPExcel->getActiveSheet()->setTitle('Anexo 03');
$objPHPExcel->getActiveSheet()
            ->setCellValue('E2',  $fila['empresa'])
            ->setCellValue('E3', 'RIF '.$fila['rif'].' Telefonos '.$fila['telefono'])
            ->setCellValue('E4', 'Direccion: '.$fila['direccion'])
            ->setCellValue('E6', 'PLANILLA ANEXO 03')
            ->setCellValue('E7', 'FECHA INICIO : '.$mesano1.' -  FECHA FIN:'.$mesano2)
             ->setCellValue('E8', 'Fecha: '.date('d/m/Y'));    

$objPHPExcel->getActiveSheet()->getStyle('E6')->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getStyle('E6')->getFont()->setSize(14);
//$objPHPExcel->getActiveSheet()->mergeCells('C2:D5');
$objPHPExcel->getActiveSheet()->mergeCells('E2:P2');
$objPHPExcel->getActiveSheet()->mergeCells('E3:P3');
$objPHPExcel->getActiveSheet()->mergeCells('E4:P4');
$objPHPExcel->getActiveSheet()->mergeCells('E6:K6');
$objPHPExcel->getActiveSheet()->mergeCells('E7:K7');
$objPHPExcel->getActiveSheet()->mergeCells('E8:K8');
$objPHPExcel->getActiveSheet()->mergeCells('F10:G10');
$objPHPExcel->getActiveSheet()->mergeCells('F11:G11');
//$objPHPExcel->getActiveSheet()->getStyle('C2:D5')->applyFromArray(allBordersThin());
//$objPHPExcel->getActiveSheet()->getStyle('C2:D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//$objPHPExcel->getActiveSheet()->getStyle('C2:D5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E6:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E7:K7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E8:K8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E6:K6')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A10:AC10')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A10:AC10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('A11:AC11')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A11:AC11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(30);
$cadena='GRUPO SEGUN TABLA RETENCION Nº DEPENDIENTES';
$tam_cadena=strlen($cadena);
$objPHPExcel->getActiveSheet()
        
            ->setCellValue('A10', '')
            ->setCellValue('B10', '')
            ->setCellValue('C10', '7')
            ->setCellValue('D10', '')
            ->setCellValue('E10', '8')
            ->setCellValue('F10', '9')
            ->setCellValue('H10', '10')
            ->setCellValue('I10', '11')
            ->setCellValue('J10', '12')                   
            ->setCellValue('K10', '13')
            ->setCellValue('L10', '14')
            ->setCellValue('M10', '15')
            ->setCellValue('N10', '16')
            ->setCellValue('O10', '17')
            ->setCellValue('P10', '18')
            ->setCellValue('Q10', '19')
            ->setCellValue('R10', '20')
            ->setCellValue('S10', '21')
            ->setCellValue('T10', '22')
            ->setCellValue('U10', '23')
            ->setCellValue('V10', '24')
            ->setCellValue('W10', '25')
            ->setCellValue('X10', '26')
            ->setCellValue('Y10', '27')
            ->setCellValue('Z10', '28')
            ->setCellValue('AA10', '29')
            ->setCellValue('AB10', '30')
            ->setCellValue('AC10', '31');
$objPHPExcel->getActiveSheet()       
            ->setCellValue('A11', 'EMPLEADO DECLARA')
            ->setCellValue('B11', 'TIPO')
            ->setCellValue('C11', 'CEDULA')
            ->setCellValue('D11', 'DV')
            ->setCellValue('E11', 'NOMBRE')
            ->setCellValue('F11', 'GRUPO SEGUN TABLA RETENCION Nº DEPENDIENTES')
            ->setCellValue('H11', 'Nº DE MESES TRABAJADOS')
            ->setCellValue('I11', 'REMNUNERACIONES RECIBIDAS DURANTE EL AÑO EN SALARIOS')
            ->setCellValue('J11', 'REMNUNERACIONES RECIBIDAS DURANTE EL AÑO EN SALARIOS EN ESPECIES')                   
            ->setCellValue('K11', 'REMNUNERACIONES RECIBIDAS DURANTE EL AÑO EN GASTOS DE REPRESENTACION')
            ->setCellValue('L11', 'REMNUNERACIONES RECIBIDAS DURANTE EL AÑO EN SALARIOS SIN RETENCIONES')
            ->setCellValue('M11', 'DEDUCCIONES BASICAS Y POR DEPENDIENTES')
            ->setCellValue('N11', 'SEGURO EDUCATIVO')
            ->setCellValue('O11', 'INTERESES HIPOTECARIOS')
            ->setCellValue('P11', 'INTERESES EDUCATIVOS')
            ->setCellValue('Q11', 'PRIMAS SEGUROS')
            ->setCellValue('R11', 'APORTE FONDO DE JUBILACION')
            ->setCellValue('S11', 'TOTAL')
            ->setCellValue('T11', 'RENTA NETA GRAVABLE')
            ->setCellValue('U11', 'IMPUESTO CAUSADO')
            ->setCellValue('V11', 'AJUSTE AL IMPUESTO CAUSADO')
            ->setCellValue('W11', 'EXENCION LEY 6 2005')
            ->setCellValue('X11', 'RETENCIONES DURANTE EL AÑO EN SALARIOS')
            ->setCellValue('Y11', 'RETENCIONES DURANTE EL AÑO EN GASTOS DE REPRESENTACION')
            ->setCellValue('Z11', 'AJUSTE A FAVOR DE EMPLEADOS')
            ->setCellValue('AA11', 'COLUMNA 24 A 28')
            ->setCellValue('AB11', 'A FAVOR DEL FISCO')
            ->setCellValue('AC11', 'A FAVOR DEL EMPLEADO');
cellColor('A11:AC11', 'FFFF00');
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth($tam_cadena);
$objPHPExcel->getActiveSheet()->getStyle('A10:AC10')->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->getStyle('A11:AC11')->applyFromArray(allBordersThin());
$sql = "SELECT DISTINCT np.ficha, np.cedula as cedula, np.apenom as nombre, np.seguro_social as seguro, np.fecing as fecha_ingreso
					FROM   nom_movimientos_nomina nm, nompersonal np
					WHERE  nm.cedula=np.cedula  AND nm.tipnom='".$codtip."'
					ORDER BY np.apenom";
$i=12;
$ini=$i;
$res=query($sql, $conexion);

$total_salarios_integral=$total_deducciones=$total_seguro=$total_total=$total_renta_neta=0;
$total_salarios=$total_xiii_mes=$total_vacaciones=0;
while($fila=fetch_array($res))
{
    $ficha=$fila['ficha'];
    if($fila['fecha_ingreso']<fecha_sql($mesano1)){
        $inicio=fecha_sql($mesano1);
        }else{
                $inicio=$fila['fecha_ingreso'];
        }    
    $meses=floor(antiguedad($inicio,fecha_sql($mesano2),'D')/30);
    
    //SALARIOS INTEGRAL (SALARIOS + XIII MES + VACACIONES)
    $sql_salarios_integral = "SELECT nm.monto as monto
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np 
            WHERE  np.codnom=nm.codnom AND np.status='C' AND np.anio=nm.anio AND np.frecuencia in (2,3,7,8) AND np.tipnom=".$codtip.""
            . " AND nm.ficha=".$ficha." AND ( nm.codcon=100 OR nm.codcon=102 OR nm.codcon=114 ) AND nm.anio=".$ma1[2]."";
    
    $res_salarios_integral=query($sql_salarios_integral, $conexion);
    $salarios_integral=0;
    while($fila_salarios_integral=fetch_array($res_salarios_integral))
    {
        $salarios_integral=$salarios_integral+$fila_salarios_integral['monto'];
    }
    
    //GASTOS REPRESENTACION
    $sql_gastosr = "SELECT nm.monto as monto
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np 
            WHERE  np.codnom=nm.codnom AND np.status='C' AND np.anio=nm.anio AND np.frecuencia in (2,3,7,8) AND np.tipnom=".$codtip.""
            . " AND nm.ficha=".$ficha." AND ( nm.codcon=145) AND nm.anio=".$ma1[2]."";
    
    $res_gastosr=query($sql_gastosr, $conexion);
    $gastosr=0;
    while($fila_gastosr=fetch_array($res_gastosr))
    {
        $gastosr=$gastosr+$fila_gastosr['monto'];
    }
    
    //DEDUCCIONES
    $sql_deducciones = "SELECT nm.monto as monto
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np 
            WHERE  np.codnom=nm.codnom AND np.status='C' AND np.anio=nm.anio AND np.frecuencia in (2,3,7,8) AND np.tipnom=".$codtip.""
            . " AND nm.ficha=".$ficha." AND ( nm.codcon=200) AND nm.anio=".$ma1[2]."";
    
    $res_deducciones=query($sql_deducciones, $conexion);
    $deducciones=0;
    while($fila_deducciones=fetch_array($res_deducciones))
    {
        $deducciones=$deducciones+$fila_deducciones['monto'];
    }
    
    //SEGURO EDUCATIVO
    $sql_seguro = "SELECT nm.monto as monto
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np 
            WHERE  np.codnom=nm.codnom AND np.status='C' AND np.anio=nm.anio AND np.frecuencia in (2,3,7,8) AND np.tipnom=".$codtip.""
            . " AND nm.ficha=".$ficha." AND ( nm.codcon=201) AND nm.anio=".$ma1[2]."";
    
    $res_seguro=query($sql_seguro, $conexion);
    $seguro=0;
    while($fila_seguro=fetch_array($res_seguro))
    {
        $seguro=$seguro+$fila_seguro['monto'];
    }
    //SEGURO EDUCATIVO GR
    $sql_segurogr = "SELECT nm.monto as monto
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np 
            WHERE  np.codnom=nm.codnom AND np.status='C' AND np.anio=nm.anio AND np.frecuencia in (2,3,7,8) AND np.tipnom=".$codtip.""
            . " AND nm.ficha=".$ficha." AND ( nm.codcon=207) AND nm.anio=".$ma1[2]."";
    
    $res_segurogr=query($sql_segurogr, $conexion);
    $segurogr=0;
    while($fila_segurgro=fetch_array($res_segurogr))
    {
        $segurogr=$segurogr+$fila_seguro['monto'];
    }    
    //INTERESES HIPOTECARIOS
    $interes_hipotecarios=0;
    
    //INTERESES EDUCATIVOS
    $interes_educativos=0;
    
    //PRIMAS SEGUROS
    $primas_seguros=0;
    
    //FONDO JUBILACION
    $fondo_jubilacion=0;

    //TOTAL
    $total=$salarios+$gastosr;
    
    //RENTA NETA GRAVABLE
    $renta_neta=$total-$deducciones-$seguro-$interes_hipotecarios-$interes_educativos-$primas_seguros-$fondo_jubilacion;
  
    
    //SALARIOS
    $sql_salarios = "SELECT nm.monto as monto
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np 
            WHERE  np.codnom=nm.codnom AND np.status='C' AND np.anio=nm.anio AND np.frecuencia in (2,3,7,8) AND np.tipnom=".$codtip.""
            . " AND nm.ficha=".$ficha." AND ( nm.codcon=100) AND nm.anio=".$ma1[2]."";
    
    $res_salarios=query($sql_salarios, $conexion);
    $salarios=0;
    while($fila_salarios=fetch_array($res_salarios))
    {
        $salarios=$salarios+$fila_salarios['monto'];
    }
    
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'No');
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'RUC');
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $fila['cedula']);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, utf8_encode($fila['nombre']));
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'A');
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $meses);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $salarios_integral);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $gastosr);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $deducciones);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $seguro);
    $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $total);
    $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $renta_neta);
    $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('W'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('X'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '0');
    $objPHPExcel->getActiveSheet()->setCellValue('AA'.$i, '0');
    if(($salarios_integral-$deducciones)>0)
    {
        $objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, '0');
        $objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, $salarios_integral-$deducciones);
    }
    else
    {
        $objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, $salarios_integral-$deducciones);
        $objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, '0');
    }
    
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AC'.$i)->applyFromArray(allBordersThin());
        //$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    $i++;
}
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, "TOTALES");
$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, "=SUM(I".$ini.":I".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, "=SUM(J".$ini.":J".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, "=SUM(K".$ini.":K".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, "=SUM(L".$ini.":L".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, "=SUM(M".$ini.":M".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, "=SUM(N".$ini.":N".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, "=SUM(O".$ini.":O".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, "=SUM(P".$ini.":P".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, "=SUM(Q".$ini.":Q".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, "=SUM(R".$ini.":R".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, "=SUM(S".$ini.":S".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, "=SUM(T".$ini.":T".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, "=SUM(U".$ini.":U".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, "=SUM(V".$ini.":V".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, "=SUM(W".$ini.":W".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, "=SUM(X".$ini.":X".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, "=SUM(Y".$ini.":Y".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, "=SUM(Z".$ini.":Z".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AA'.$i, "=SUM(AA".$ini.":AA".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, "=SUM(AB".$ini.":AB".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, "=SUM(AC".$ini.":AC".($i-1).")");
$objPHPExcel->getActiveSheet()->getStyle('I'.$i.':AC'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':AC'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$i=$i+7;

$objPHPExcel->getActiveSheet()->mergeCells('E'.$i.':G'.($i+4));
$objPHPExcel->getActiveSheet()->getStyle('E'.$i.':G'.($i+4))->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->mergeCells('H'.$i.':I'.($i+4));
$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':I'.($i+4))->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->mergeCells('K'.$i.':M'.($i+4));
$objPHPExcel->getActiveSheet()->getStyle('K'.$i.':M'.($i+4))->applyFromArray(allBordersThin());
$i=$i+5;
$objPHPExcel->getActiveSheet()->mergeCells('E'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i.':G'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'RECIBIDO POR');
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':I'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, 'AUTORIZADO POR');
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('K'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('K'.$i.':M'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, 'PREPARADO POR');
$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->setSelectedCells('B100');


//=================================================================================
//HOJA RESUMEN PREELABORADA
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1); 
$sql = "SELECT e.nom_emp as empresa, e.rif, e.tel_emp as telefono, e.dir_emp as direccion, e.ciu_emp as ciudad, 
               e.edo_emp, e.imagen_izq as logo
		FROM   nomempresa e";

$res=query($sql, $conexion);

$fila=fetch_array($res);
$objPHPExcel->getActiveSheet()->setTitle('Resumen Preelaborada');

$objPHPExcel->getActiveSheet()
            ->setCellValue('E2',  $fila['empresa'])
            ->setCellValue('E3', 'RIF '.$fila['rif'].' Telefonos '.$fila['telefono'])
            ->setCellValue('E4', 'Direccion: '.$fila['direccion'])
            ->setCellValue('E6', 'RESUMEN PREELABORADA')
            ->setCellValue('E7', 'FECHA INICIO : '.$mesano1.' -  FECHA FIN:'.$mesano2)
             ->setCellValue('E8', 'Fecha: '.date('d/m/Y'));    

$objPHPExcel->getActiveSheet()->getStyle('E6')->getFont()->setName('Arial');
$objPHPExcel->getActiveSheet()->getStyle('E6')->getFont()->setSize(14);
//$objPHPExcel->getActiveSheet()->mergeCells('C2:D5');
$objPHPExcel->getActiveSheet()->mergeCells('E2:P2');
$objPHPExcel->getActiveSheet()->mergeCells('E3:P3');
$objPHPExcel->getActiveSheet()->mergeCells('E4:P4');
$objPHPExcel->getActiveSheet()->mergeCells('E6:K6');
$objPHPExcel->getActiveSheet()->mergeCells('E7:K7');
$objPHPExcel->getActiveSheet()->mergeCells('E8:K8');
//$objPHPExcel->getActiveSheet()->getStyle('C2:D5')->applyFromArray(allBordersThin());
//$objPHPExcel->getActiveSheet()->getStyle('C2:D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
//$objPHPExcel->getActiveSheet()->getStyle('C2:D5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E6:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E7:K7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E8:K8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E6:K6')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E4')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A10:AM10')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A10:AM10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(7);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(20);

$objPHPExcel->getActiveSheet()       
            ->setCellValue('A10', 'CEDULA')
            ->setCellValue('B10', 'DV')
            ->setCellValue('C10', 'NOMBRE')
            ->setCellValue('D10', 'CLAVE')
            ->setCellValue('E10', 'DEDUCCION')
            ->setCellValue('F10', 'ENERO')
            ->setCellValue('G10', 'G.R.')
            ->setCellValue('H10', 'FEBRERO')
            ->setCellValue('I10', 'G.R.')
            ->setCellValue('J10', 'MARZO')                   
            ->setCellValue('K10', 'G.R.')
            ->setCellValue('L10', 'ABRIL')
            ->setCellValue('M10', 'G.R.')
            ->setCellValue('N10', 'XIII MES')
            ->setCellValue('O10', 'MAYO')
            ->setCellValue('P10', 'G.R.')
            ->setCellValue('Q10', 'JUNIO')
            ->setCellValue('R10', 'G.R.')
            ->setCellValue('S10', 'JULIO')
            ->setCellValue('T10', 'G.R.')
            ->setCellValue('U10', 'AGOSTO')
            ->setCellValue('V10', 'G.R.')
            ->setCellValue('W10', 'XIII MES')
            ->setCellValue('X10', 'SEPTIEMBRE')
            ->setCellValue('Y10', 'G.R.')
            ->setCellValue('Z10', 'OCTUBRE')
            ->setCellValue('AA10', 'G.R.')
            ->setCellValue('AB10', 'NOVIEMBRE')
            ->setCellValue('AC10', 'G.R.')
            ->setCellValue('AD10', 'DICIEMBRE')
            ->setCellValue('AE10', 'G.R.')
            ->setCellValue('AF10', 'XIII MES')
            ->setCellValue('AG10', 'AGUINALDO')
            ->setCellValue('AH10', 'BONOS')
            ->setCellValue('AI10', 'TOTAL SALARIO')
            ->setCellValue('AJ10', 'TOTAL G.R.')
            ->setCellValue('AK10', 'TOTAL XIII')
            ->setCellValue('AL10', 'TOTAL SEGURO EDUCATIVO')
            ->setCellValue('AM10', 'DESC. RENTA');
            
cellColor('A10:AM10', 'FFFF00');
$objPHPExcel->getActiveSheet()->getStyle('A10:AM10')->applyFromArray(allBordersThin());
$sql = "SELECT DISTINCT np.ficha, np.cedula as cedula, np.apenom as nombre, np.seguro_social as seguro, np.fecing as fecha_ingreso
					FROM   nom_movimientos_nomina nm, nompersonal np
					WHERE  nm.cedula=np.cedula  AND nm.tipnom='".$codtip."'
					ORDER BY np.apenom";
$i=11;
$ini=$i;
$res=query($sql, $conexion);
while($fila=fetch_array($res))
{
    $ficha=$fila['ficha'];
    $cedula=$fila['cedula'];
    $nombre=$fila['nombre'];
    $enero=$febrero=$marzo=$abril=$mayo=$junio=$julio=$agosto=$septiembre=$octubre=$novimebre=$diciembre=0;
    $gr_enero=$gr_febrero=$gr_marzo=$gr_abril=$gr_mayo=$gr_junio=$gr_julio=$gr_agosto=$gr_septiembre=$gr_octubre=$gr_noviembre=$gr_diciembre=0;
    $xiii_mes_abril=$xiii_mes_agosto=$xiii_mes_diciembre=0;
    $aguinaldos=$total_salario=$total_gr=$total_xiii_mes=$seguro_educativo=$desc_renta=0;
    $sql_montos = "SELECT nm.monto as monto, nm.codcon as concepto, nm.mes as mes
            FROM   nom_movimientos_nomina nm, nom_nominas_pago np
            WHERE  np.codnom = nm.codnom AND np.status='C' AND np.frecuencia in (2,3,7)
            AND np.codtip=".$codtip." AND nm.ficha=".$ficha." AND nm.anio=".$ma1[2]."";
    //echo $sql_montos;
    $res_montos=query($sql_montos, $conexion);
    
    while($fila_montos=fetch_array($res_montos))
    {
        if($fila_montos['mes']==1)
        {
            if($fila_montos['concepto']==100)           
                $enero=$enero+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_enero=$gr_enero+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==2)
        {
            if($fila_montos['concepto']==100)           
                $febrero=$febrero+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_febrero=$gr_febrero+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==3)
        {
            if($fila_montos['concepto']==100)           
                $marzo=$marzo+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_marzo=$gr_marzo+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==4)
        {
            if($fila_montos['concepto']==100)           
                $abril=$abril+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_abril=$gr_abril+$fila_montos['monto'];
            
             if($fila_montos['concepto']==102)            
                $xiii_mes_abril=$xiii_mes_abril+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==5)
        {
            if($fila_montos['concepto']==100)           
                $mayo=$mayo+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_mayo=$gr_mayo+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==6)
        {
            if($fila_montos['concepto']==100)           
                $junio=$junio+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_junio=$gr_junio+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==7)
        {
            if($fila_montos['concepto']==100)           
                $julio=$julio+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_julio=$gr_julio+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==8)
        {
            if($fila_montos['concepto']==100)           
                $agosto=$agosto+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_agosto=$gr_agosto+$fila_montos['monto'];
            
              if($fila_montos['concepto']==102)            
                $xiii_mes_agosto=$xiii_mes_agosto+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==9)
        {
            if($fila_montos['concepto']==100)           
                $septiembre=$septiembre+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_septiembre=$gr_septiembre+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==10)
        {
            if($fila_montos['concepto']==100)           
                $octubre=$octubre+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_octubre=$gr_octubre+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==11)
        {
            if($fila_montos['concepto']==100)           
                $noviembre=$noviembre+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_noviembre=$gr_noviembre+$fila_montos['monto'];
           
        }
        
        if($fila_montos['mes']==12)
        {
            if($fila_montos['concepto']==100)           
                $diciembre=$diciembre+$fila_montos['monto'];
         
            if($fila_montos['concepto']==145)            
                $gr_diciembre=$gr_diciembre+$fila_montos['monto'];
            
              if($fila_montos['concepto']==102)            
                $xiii_mes_diciembre=$xiii_mes_diciembre+$fila_montos['monto'];
           
        }
        
        $total_salario=$enero+$febrero+$marzo+$abril+$mayo+$junio+$julio+$agosto+$septiembre+$octubre+$noviembre+$diciembre;
        $total_gr=$gr_enero+$gr_febrero+$gr_marzo+$gr_abril+$gr_mayo+$gr_junio+$gr_julio+$gr_agosto+$gr_septiembre+$gr_octubre+$gr_noviembre+$gr_diciembre;
        $total_xiii_mes=$xiii_mes_abril+$xiii_mes_agosto+$xiii_mes_diciembre;
        $seguro_educativo=($total_salario+($total_gr*0.55))*0.0125;
        
    }
    
    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $cedula);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '');
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $nombre);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '');
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, '');
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $enero);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $gr_enero);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $febrero);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $gr_febrero);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $marzo);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $gr_marzo);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $abril);
    $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $gr_abril);
    $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $xiii_mes_abril);
    $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $mayo);
    $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $gr_mayo);
    $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $junio);
    $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $gr_junio);
    $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $julio);
    $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $gr_julio);
    $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $agosto);
    $objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $gr_agosto);
    $objPHPExcel->getActiveSheet()->setCellValue('W'.$i, $xiii_mes_agosto);
    $objPHPExcel->getActiveSheet()->setCellValue('X'.$i, $septiembre);
    $objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, $gr_septiembre);
    $objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, $octubre);
    $objPHPExcel->getActiveSheet()->setCellValue('AA'.$i, $gr_octubre);
    $objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, $noviembre);
    $objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, $gr_noviembre);
    $objPHPExcel->getActiveSheet()->setCellValue('AD'.$i, $diciembre);
    $objPHPExcel->getActiveSheet()->setCellValue('AE'.$i, $gr_diciembre);
    $objPHPExcel->getActiveSheet()->setCellValue('AF'.$i, $xiii_mes_diciembre);
    $objPHPExcel->getActiveSheet()->setCellValue('AG'.$i, '');
    $objPHPExcel->getActiveSheet()->setCellValue('AH'.$i, '');
    $objPHPExcel->getActiveSheet()->setCellValue('AI'.$i, $total_salario);
    $objPHPExcel->getActiveSheet()->setCellValue('AJ'.$i, $total_gr);
    $objPHPExcel->getActiveSheet()->setCellValue('AK'.$i, $total_xiii_mes);
    $objPHPExcel->getActiveSheet()->setCellValue('AL'.$i,$seguro_educativo);
    $objPHPExcel->getActiveSheet()->setCellValue('AM'.$i, '0');
    
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AM'.$i)->applyFromArray(allBordersThin());
        //$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    $i++;
}
$objPHPExcel->getActiveSheet()->getStyle('F'.$i.':AM'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, "TOTALES");
$objPHPExcel->getActiveSheet()->getStyle('F'.$i.':AC'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, "=SUM(F".$ini.":F".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, "=SUM(G".$ini.":G".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, "=SUM(H".$ini.":H".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, "=SUM(I".$ini.":I".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, "=SUM(J".$ini.":J".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, "=SUM(K".$ini.":K".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, "=SUM(L".$ini.":L".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, "=SUM(M".$ini.":M".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, "=SUM(N".$ini.":N".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, "=SUM(O".$ini.":O".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, "=SUM(P".$ini.":P".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, "=SUM(Q".$ini.":Q".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, "=SUM(R".$ini.":R".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, "=SUM(S".$ini.":S".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, "=SUM(T".$ini.":T".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, "=SUM(U".$ini.":U".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, "=SUM(V".$ini.":V".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, "=SUM(W".$ini.":W".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, "=SUM(X".$ini.":X".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, "=SUM(Y".$ini.":Y".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, "=SUM(Z".$ini.":Z".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AA'.$i, "=SUM(AA".$ini.":AA".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, "=SUM(AB".$ini.":AB".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, "=SUM(AC".$ini.":AC".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AD'.$i, "=SUM(AD".$ini.":AD".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AE'.$i, "=SUM(AE".$ini.":AE".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AF'.$i, "=SUM(AF".$ini.":AF".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AG'.$i, "=SUM(AG".$ini.":AG".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AH'.$i, "=SUM(AH".$ini.":AH".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AI'.$i, "=SUM(AI".$ini.":AI".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AJ'.$i, "=SUM(AJ".$ini.":AJ".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AK'.$i, "=SUM(AK".$ini.":AK".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AL'.$i, "=SUM(AL".$ini.":AL".($i-1).")");
$objPHPExcel->getActiveSheet()->setCellValue('AM'.$i, "=SUM(AM".$ini.":AM".($i-1).")");

//$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $total_prima);
//$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $total_indemnizacion);
//$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $total_total);
$i=$i+7;

$objPHPExcel->getActiveSheet()->mergeCells('E'.$i.':G'.($i+4));
$objPHPExcel->getActiveSheet()->getStyle('E'.$i.':G'.($i+4))->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->mergeCells('H'.$i.':J'.($i+4));
$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':J'.($i+4))->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->mergeCells('K'.$i.':M'.($i+4));
$objPHPExcel->getActiveSheet()->getStyle('K'.$i.':M'.($i+4))->applyFromArray(allBordersThin());
$i=$i+5;
$objPHPExcel->getActiveSheet()->mergeCells('E'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i.':G'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'RECIBIDO POR');
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('H'.$i.':J'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i.':J'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, 'AUTORIZADO POR');
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('K'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('K'.$i.':M'.$i)->applyFromArray(allBordersThin());
$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, 'PREPARADO POR');
$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->getActiveSheet()->setSelectedCells('B100');


$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Reporte_Anexo03_Preelaborada.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$objWriter->save('php://output');

exit;

?>




