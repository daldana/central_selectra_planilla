<?php 
require_once '../../generalp.config.inc.php';
session_start();
ob_start();

//error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
//nomturnos_factor        	factor_id, turno_id, descripcion,  valor,  fecha...
require_once '../lib/common.php';
include ("func_bd.php");
$conexion=conexion();

$registro_id=$_POST['registro_id'];	
$op=$_POST['op'];

if ($op==3) //Se presiono el boton de Eliminar
{	
	$query="DELETE FROM nomturnos_factor WHERE factor_id=$registro_id";	
		
	$result=sql_ejecutar($query);
	activar_pagina("turnos_factor_list.php"); // activar_pagina("turnos_factor_list.php");		 
}     
$strsql = "SELECT * FROM nomturnos_factor ORDER BY descripcion ASC";
$result = sql_ejecutar($strsql); // query($strsql, $conexion);		
?>

<?php include("../header4.php"); // <html><head></head><body> ?>
<link href="../../includes/assets/css/custom-datatables.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function enviar(op,id){

	if (op==1){		// Opcion de Agregar
		document.frmProfesiones.op.value=op;
		document.frmProfesiones.action="ag_turnos_factor.php";
		document.frmProfesiones.submit();	
	}
	if (op==2){	 	// Opcion de Modificar	
		document.frmProfesiones.registro_id.value=id;		
		document.frmProfesiones.op.value=op;
		document.frmProfesiones.action="ag_turnos_factor.php";
		document.frmProfesiones.submit();		
	}
	if (op==3){		// Opcion de Eliminar
		if (confirm("\u00BFEst\u00E1 seguro que desea eliminar el registro ?"))
		{					
			document.frmProfesiones.registro_id.value=id;
			document.frmProfesiones.op.value=op;
  			document.frmProfesiones.submit();
		}		
	}
}
</script>




<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								Factor  de losTurnos
							</div>
							<div class="actions">
								<a class="btn btn-sm blue"  onclick="javascript: window.location='ag_turnos_factor.php'">
									<i class="fa fa-plus"></i>
									Agregar
								</a>
								<a class="btn btn-sm blue"  onclick="javascript: window.location='menu_configuracion.php'">
									<i class="fa fa-arrow-left"></i> Regresar
								</a>
							</div>
						</div>
						<div class="portlet-body">
						<form action="" method="post" name="frmProfesiones" id="frmProfesiones"> -->
							<table class="table table-striped table-bordered table-hover" id="table_datatable">
							<thead>
				
						
							<tr>
								<th>Id del Factor <!-- Id Factor --></th>
								<th>Id  delTurno</th>
								<th>Descripci&oacute;n del Factor</th>
								<th>Id  de Conceptos</th>
								<th>Valor del Factor</th>
								<th>Fecha</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<!-- <th>&nbsp;</th> -->
							</tr>
							</thead>
							<tbody>
				
							<?php
								while( $fila = mysqli_fetch_array($result) ) 
								{ 								
								?>
									<tr class="odd gradeX">
										<td><?php echo $fila['factor_id']; ?></td>
										<td><?php echo $fila['turno_id']; ?></td>
										<td><?php echo $fila['descripcion']; ?></td>
										<td><?php echo $fila['con_id']; ?></td>
										<td><?php echo $fila['valor']; ?></td>
										<td><?php echo $fila['fecha']; ?></td>  
										<td style="text-align: center">
									      <a href="javascript:enviar(<?php echo(2); ?>,<?php echo($fila['factor_id']); ?>);" title="Editar">
									      <img src="../../includes/imagenes/icons/pencil.png" width="16" height="16"></a>
											<!-- <a href="factor_edit.php?codigo=<?php //echo $fila['factor_id']; ?>" title="Editar">
											<img src="../../includes/imagenes/icons/pencil.png" width="16" height="16">
											</a> -->
										</td>
										<td style="text-align: center">
										      <a href="javascript:enviar(<?php echo(3); ?>,<?php echo($fila['factor_id']); ?>);" title="Eliminar">
										      <img src="../../includes/imagenes/icons/delete.png" alt="Eliminar" width="16" height="16"></a>
					                    </td>
									</tr>
								  <?php									
								}
							?>
							
							</tbody>
							</table>
							    <input name="registro_id" type="hidden" value="">
							    <input name="op" type="hidden" value="">	
							</form>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<?php include("../footer4.php"); ?>
<script type="text/javascript">
	 $(document).ready(function() { 

            $('#table_datatable').DataTable({
            	"iDisplayLength": 25,
                //"sPaginationType": "bootstrap",
            	"sPaginationType": "bootstrap_extended", 
                "oLanguage": {
                	"sSearch": "<img src='../../includes/imagenes/icons/magnifier.png' width='16' height='16' > Buscar:",
                    "sLengthMenu": "Mostrar _MENU_",
                    //"sInfo": "Showing page _PAGE_ of _PAGES_", // Mostrando 1 to 5 de 18 entradas
                    //"sInfoEmpty": "No hay registros para mostrar",
                    "sInfoEmpty": "",
                    //"sInfo": "",
                    "sInfo":"Total _TOTAL_ registros",
                    "sInfoFiltered": "",
          		    "sEmptyTable":  "No hay datos disponibles",
                    "sZeroRecords": "No se encontraron registros",
                    "oPaginate": {
                        "sPrevious": "P&aacute;gina Anterior",//"Prev",
                        "sNext": "P&aacute;gina Siguiente",//"Next",
                        "sPage": "P&aacute;gina",//"Page",
                        "sPageOf": "de",//"of"
                    }
                },
                "aLengthMenu": [ // set available records per page
                    [5, 10, 25, 50,  -1],
                    [5, 10, 25, 50, "Todos"]
                ],                
                "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [2] },
                    { "bSearchable": false, "aTargets": [ 2 ] },
                    { "sWidth": "8%", "aTargets": [2] },
                ],
				 "fnDrawCallback": function() {
				        $('#table_datatable_filter input').attr("placeholder", "Escriba frase para buscar");
				 }
            });

            $('#table_datatable').on('change', 'tbody tr .checkboxes', function(){
                 $(this).parents('tr').toggleClass("active");
            });

            $('#table_datatable_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); 
            $('#table_datatable_wrapper .dataTables_length select').addClass("form-control input-xsmall"); 
	 });
</script>
</body>
</html>