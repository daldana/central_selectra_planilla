<?php
session_start();
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('America/Caracas');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once("../lib/common.php");
//require_once("../paginas/func_bd.php");
require_once("../paginas/phpexcel/Classes/PHPExcel.php");
require_once("../paginas/phpexcel/Classes/PHPExcel/IOFactory.php");

$conexion = new bd($_SESSION['bd']);

// Inicializar variables
$success  = true;
$mensaje  = '';

// Validar posibles errores: 

if($_FILES['archivo']['name'] != '')
{
	$name	  = $_FILES['archivo']['name'];
	$tname 	  = $_FILES['archivo']['tmp_name'];
	$type 	  = $_FILES['archivo']['type'];

	if($type == 'application/vnd.ms-excel')
		$ext = 'xls'; // Extension excel 97
	else if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
		$ext = 'xlsx'; // Extension excel 2007 y 2010
	else{

		if (function_exists('finfo_file')) 
		{
			$type  = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $tname);

			if($type == 'application/vnd.ms-excel')
				$ext = 'xls'; // Extension excel 97
			else if($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
				$ext = 'xlsx'; // Extension excel 2007 y 2010		
		}
		// else
		// 	alert("¡Error! No se encuentra disponible la funcion finfo_file. Active la extensión fileinfo en su archivo php.ini");

		if(!isset($ext))
		{
			$mensaje = "¡Error! Extensión de archivo inválida.";
			$success = false;
		}
	}

	if(isset($ext))
	{
		$xls  = 'Excel5';
		$xlsx = 'Excel2007';

		// Creando el lector
		$objReader = PHPExcel_IOFactory::createReader($$ext);

		// Cargamos el archivo
		$objPHPExcel = $objReader->load($tname);

		$objPHPExcel->setActiveSheetIndex(0);

		$i=2;
		while($objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue() != '')
		{
			$ficha              = $objPHPExcel->getActiveSheet()->getCell("A".$i)->getCalculatedValue(); // Numero de Ficha - Requerido
			$apellidos          = $objPHPExcel->getActiveSheet()->getCell("B".$i)->getCalculatedValue(); // Apellidos
			$nombres            = $objPHPExcel->getActiveSheet()->getCell("C".$i)->getCalculatedValue(); // Nombres
			$cedula             = $objPHPExcel->getActiveSheet()->getCell("D".$i)->getCalculatedValue(); // Cedula
			$fecnac             = $objPHPExcel->getActiveSheet()->getCell("E".$i)->getFormattedValue();  // Fecha Nac (dia/mes/año => 21/03/2015)
			$lugarnac           = $objPHPExcel->getActiveSheet()->getCell("F".$i)->getCalculatedValue(); // Lugar
			$fecing             = $objPHPExcel->getActiveSheet()->getCell("G".$i)->getFormattedValue();  // Fecha de Ingreso
			$suesal             = $objPHPExcel->getActiveSheet()->getCell("H".$i)->getCalculatedValue(); // Salario
			$hora_base          = $objPHPExcel->getActiveSheet()->getCell("I".$i)->getCalculatedValue(); // Horas Base
			$nomposicion_id     = $objPHPExcel->getActiveSheet()->getCell("J".$i)->getCalculatedValue(); // Numero de Posicion
			$cta_presupuestaria = $objPHPExcel->getActiveSheet()->getCell("K".$i)->getCalculatedValue(); // Cta. Presupuestaria
			$estado             = $objPHPExcel->getActiveSheet()->getCell("L".$i)->getCalculatedValue(); // Status
			$sexo               = $objPHPExcel->getActiveSheet()->getCell("M".$i)->getCalculatedValue(); // Sexo
			$seguro_social      = $objPHPExcel->getActiveSheet()->getCell("N".$i)->getCalculatedValue(); // Seguro Social
			$num_decreto        = $objPHPExcel->getActiveSheet()->getCell("O".$i)->getCalculatedValue(); // #Decreto
			$tipo_empleado      = $objPHPExcel->getActiveSheet()->getCell("P".$i)->getCalculatedValue(); // Titular o Interino
			$clave_ir           = $objPHPExcel->getActiveSheet()->getCell("Q".$i)->getCalculatedValue(); // Clave I/R
			$i++;

			//===============================================================================================================================
				//echo "Fecha nacimiento => ". $fecnac . " Fecha Ingreso: ".$fecing;
				$fecnac = str_replace('/', '-', $fecnac);
				$fecnac = DateTime::createFromFormat('m-d-y', $fecnac); // DateTime::createFromFormat('d-m-Y', $fecnac);
				$errorDatetime = DateTime::getLastErrors();
				$fecnac = ($errorDatetime['warning_count'] > 0) ? null :  $fecnac->format('Y-m-d') ;

				//echo "Fecha nacimiento => ". $fecnac; exit;

				$fecing = str_replace('/', '-', $fecing);
				$fecing = DateTime::createFromFormat('m-d-y', $fecing); //DateTime::createFromFormat('d-m-Y', $fecing);
				$errorDatetime = DateTime::getLastErrors();
				$fecing = ($errorDatetime['warning_count'] > 0) ? null :  $fecing->format('Y-m-d') ;

				$apenom = $apellidos.', '.$nombres;
			//===============================================================================================================================		
			## Registro del empleado
			
			$sql="INSERT INTO nompersonal
			             (ficha, apellidos, nombres, apenom, cedula, fecnac, lugarnac, fecing, suesal, hora_base, nomposicion_id, 
			              estado, sexo, seguro_social, num_decreto, cta_presupuestaria, tipo_empleado, clave_ir, tipnom) 
				  VALUES ('{$ficha}', '{$apellidos}', '{$nombres}', '{$apenom}', '{$cedula}', '{$fecnac}', '{$lugarnac}', '{$fecing}', 
				  	      '{$suesal}', '{$hora_base}', '{$nomposicion_id}', '{$estado}', '{$sexo}', '{$seguro_social}', '{$num_decreto}', 
				  	      NULLIF('{$cta_presupuestaria}',''), NULLIF('{$tipo_empleado}',''), NULLIF('{$clave_ir}',''), 
				  	      '".$_SESSION['codigo_nomina']."')";		
			$res = $conexion->query($sql);

			if(!$res)
			{
				$success = false;

				$mensaje = "¡Error al registrar los empleados! (Código: ".$conexion->getCodigoError().") ".$conexion->getMensajeError();
				
				if($conexion->getCodigoError() == '1062')
				{
					$mensaje = "¡Error! Existen empleados duplicados (Ficha No: ".$ficha.")";
				}

				break;
			}	
		}

		if($i==2)
			$mensaje = "Error al importar los datos. La celda A$i no contiene datos.";
		else if($success)
			$mensaje = "Datos importados exitosamente";
	}
}
else
{
	$mensaje = "Error al cargar el archivo";
	$success = false;
}

echo "%&&%".($success ? '1' : '0')."&&&&".$mensaje;
?>
