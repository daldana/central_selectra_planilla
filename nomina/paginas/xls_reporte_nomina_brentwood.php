<?php
require_once('../lib/database.php');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Panama');

set_time_limit(0);

if (PHP_SAPI == 'cli')
	die('Este ejemplo solo se debe ejecutar desde un navegador Web');

/** Include PHPExcel */
include('lib/php_excel.php');
require_once("phpexcel/Classes/PHPExcel.php");
require_once("phpexcel/Classes/PHPExcel/IOFactory.php");

if(isset($_POST['codnom']))
{
	$codnom = $_POST['codnom'];

	$conexion = new Database($_SESSION['bd']);

	$sql = "SELECT n.descrip, DATE_FORMAT(periodo_ini, ' de %M %Y') as fecha_inicio, DATE_FORMAT(periodo_ini, '%d') as dia_ini,
			       f.codfre as frecuencia
			FROM   nom_nominas_pago n 
			INNER JOIN nomfrecuencias f ON n.frecuencia=f.codfre
			WHERE n.codnom='{$codnom}'";
	$res = $conexion->query($sql);

	if($obj = $res->fetch_object())
	{
		$fecha_inicio = traducir_mes(strtolower($obj->fecha_inicio));
		$fecha_inicio = strtoupper($fecha_inicio);

		if($obj->frecuencia == 2) // 1ra Quincena
			$titulo_hoja0 = 'I' . $fecha_inicio;
		else if ($obj->frecuencia == 3) // 2da Quincena
			$titulo_hoja0 = 'II' . $fecha_inicio;
		else
			$titulo_hoja0 = $obj->dia_ini . $fecha_inicio; 
	}	

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()->setCreator("Selectra Planilla")
								 ->setLastModifiedBy("Selectra Planilla")
								 ->setTitle("Reporte de Planilla");

	// Color de fondo por defecto de las hojas del libro
	// colorFondoLibro('FFFFFF'); // Blanco

	//==============================================================================================
	//==============================================================================================
	// Creación de Hojas de Cálculo
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(1);	
	$objPHPExcel->getActiveSheet()->setTitle('COMPROBANTES ADV');	

	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(2);	
	$objPHPExcel->getActiveSheet()->setTitle('EXTRAS');	

	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(3);	
	$objPHPExcel->getActiveSheet()->setTitle('TOTAL A PAGAR AGS -BRENTWOOD');	

	//==============================================================================================
	// Hoja de cálculo => ADMINISTRATIVO	
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(4);	
	$objPHPExcel->getActiveSheet()->setTitle('ADMINISTRATIVO');		

	$objPHPExcel->getActiveSheet()->setCellValue('A2', 'ADMINISTRATIVOS BRENTWOOD ' . $titulo_hoja0)
								  ->setCellValue('D2', 'DETALLE PARA SOBRE DE EXTRAS')
								  ->setCellValue('A3', 'NOMBRE')
								  ->setCellValue('B3', 'REGULAR TOTAL')
								  ->setCellValue('C3', 'S/P TOTAL')
								  ->setCellValue('D3', 'SP')
								  ->setCellValue('E3', 'LIBRE')
								  ->setCellValue('F3', 'NACIONAL')
								  ->setCellValue('G3', 'EXTRA')
								  ->setCellValue('H3', 'BONO');

	$objPHPExcel->getActiveSheet()->getStyle('A2:H3')->getFont()->setName('Century Gothic')->setSize(11);

	// Celdas en cursiva
	$objPHPExcel->getActiveSheet()->getStyle('D2:H2')->getFont()->setItalic(true);

	// Celdas con texto alineado horizontalmente en el centro
	$objPHPExcel->getActiveSheet()->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	$objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	

	// Celdas combinadas
	$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');

	// Celdas en negrita
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true);

	// Celdas con borde completo
	$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->applyFromArray(borderExterno(PHPExcel_Style_Border::BORDER_MEDIUM));
	$objPHPExcel->getActiveSheet()->getStyle('D2:H2')->applyFromArray(borderExterno(PHPExcel_Style_Border::BORDER_MEDIUM));
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));

	// Celdas con borde derecho
	$objPHPExcel->getActiveSheet()->getStyle('C3')->applyFromArray(borderRight(PHPExcel_Style_Border::BORDER_MEDIUM));

	// Ajustar Texto
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setWrapText(true); 

	// Ancho de las columnas
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40); // 39,29 (280 px) => Original: 23,43 (169 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14); // 13,29 (98 px)  => Original: 13,43 (99 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(16); // 15,29 (112 px) => Original: 15,57 (114 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12); // 11,29 (84 px)  => Original: 11,43 (85 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12); // 11,29 (84 px)  => Original: 11,71 (87 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13); // 12,29 (91 px)  => Original: 12,43 (92 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12); // 11,29 (84 px)  => Original: 11,43 (85 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12); // 11,29 (84 px)  => Original: 11,43 (85 px)

	//==============================================================================================
	// Hoja de cálculo => VALES	 
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(5);	
	$objPHPExcel->getActiveSheet()->setTitle('VALES');	

	$objPHPExcel->getActiveSheet()->setCellValue('A3', 'VALES DESCONTADOS '.$titulo_hoja0);
	
	cellColor('A3:B3', '0F243E');  // Celdas con color de fondo	
	colorTexto('A3:B3', 'FFFFFF'); // Celdas con color de texto diferente

	$objPHPExcel->getActiveSheet()->mergeCells('A3:B3'); // Combinar celdas

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(28);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);

	// Hoja de cálculo => Hoja 1 
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(6);	
	$objPHPExcel->getActiveSheet()->setTitle('Hoja 1');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'PLANILLA '.$titulo_hoja0.'-BRENTWOOD')
								  ->setCellValue('A2', 'CÓDIGO')
								  ->setCellValue('B2', 'NOMBRE')
								  ->setCellValue('C2', 'REGULAR')
								  ->setCellValue('D2', 'BRUTO EXTRA')
								  ->setCellValue('F2', 'TOTAL EXTRA');

	cellColor('A1', '0F243E');  // Celdas con color de fondo	
	colorTexto('A1', 'FFFFFF'); // Celdas con color de texto diferente

	$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->getFont()->setBold(true);

	$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');

	$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objPHPExcel->getActiveSheet()->getStyle('A1:F2')->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));

	// Nivel de zoom y vista previa de salto pagina
	$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(76);	
	$objPHPExcel->getActiveSheet()->getSheetView()->setView(PHPExcel_Worksheet_SheetView::SHEETVIEW_PAGE_BREAK_PREVIEW);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(9);  // => Original: 8,57  (65 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(29); // => Original: 28,43 (204 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14); // => Original: 13,43 (99 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15); // => Original: 14,14 (104 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13); // => Original: 12,00 (89 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16); // => Original: 15,14 (111 px)

	//==============================================================================================
	// Hoja de cálculo => I DE MES ANIO
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle($titulo_hoja0);

	// Tipo de letra por defecto de la Hoja 0
	$objPHPExcel->getActiveSheet()->getDefaultStyle()->getFont()->setName('Calibri')->setSize(11); 

	$objPHPExcel->getActiveSheet()->setCellValue('I1', 'PLANILLA BRENTWOOD')
								  ->setCellValue('Q1', 16)
								  ->setCellValue('R1', 17)
								  ->setCellValue('S1', 18)
								  ->setCellValue('T1', 19)
								  ->setCellValue('A2', 'CORRESPONDE A LA ' . $titulo_hoja0 )
								  ->setCellValue('R3', 'DEDUCCIONES')
								  ->setCellValue('N4', 'SALARIO')
								  ->setCellValue('U4', 'TOTAL')
								  ->setCellValue('V4', 'SALARIO')
								  ->setCellValue('A5', 'CODIGO')
								  ->setCellValue('B5', 'CEDULA')
								  ->setCellValue('C5', 'COMPROBANTE2')
								  ->setCellValue('D5', 'NOMBRE')
								  ->setCellValue('E5', 'RATA X')
								  ->setCellValue('F5', 'REGULAR')
								  ->setCellValue('G5', 'TOTAL')
								  ->setCellValue('H5', 'NOVENA')
								  ->setCellValue('I5', 'T. NOVENA')
								  ->setCellValue('J5', 'NACIONAL')
								  ->setCellValue('K5', 'T. NACIONAL')
								  ->setCellValue('L5', 'Cumbre')
								  ->setCellValue('M5', 'Total Cumbre')
								  ->setCellValue('N5', 'DOMINGO')
								  ->setCellValue('O5', 'T. DOMINGO')
								  ->setCellValue('P5', 't. horas')
								  ->setCellValue('Q5', 'BRUTO')
								  ->setCellValue('R5', 'TARDANZAS')
								  ->setCellValue('S5', 'AUSENCIAS')
								  ->setCellValue('T5', 'T. DEVENGADO')
								  ->setCellValue('U5', 'SOCIAL')
								  ->setCellValue('V5', 'EDUCATIVO')
								  ->setCellValue('W5', 'OTROS')
								  ->setCellValue('X5', 'ACREEDOR')
								  ->setCellValue('Y5', 'T. DESCUENTO')
								  ->setCellValue('Z5', 'NETO');

	// Celdas con color de fondo
	cellColor('A5:Z5', '0F243E');  

	// Celdas con color de texto diferente
	colorTexto('A5:Z5', 'FFFFFF');

	// Celdas combinadas (Merge)
	$objPHPExcel->getActiveSheet()->mergeCells('I1:M1');	
	$objPHPExcel->getActiveSheet()->mergeCells('A2:X2');
	$objPHPExcel->getActiveSheet()->mergeCells('R3:U3');							  

	// Celdas con texto en negrita
	$objPHPExcel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('Q1:T1')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('R3')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('N4')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('U4')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('V4')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('A5:Z5')->getFont()->setBold(true);

	// Celdas con tamaño de texto diferente
	$objPHPExcel->getActiveSheet()->getStyle('E5:Z5')->getFont()->setSize(9); 
	$objPHPExcel->getActiveSheet()->getStyle('U4')->getFont()->setSize(9); 

	// Celdas con texto alineado horizontalmente en el centro
	$objPHPExcel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
	$objPHPExcel->getActiveSheet()->getStyle('R3:U3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// Celdas contexto alineado verticalmente en el centro
	$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	

	// Celdas con borde completo
	$objPHPExcel->getActiveSheet()->getStyle('R3:Y3')->applyFromArray(borderExterno());
	$objPHPExcel->getActiveSheet()->getStyle('R4:U4')->applyFromArray(borderExterno());
	$objPHPExcel->getActiveSheet()->getStyle('V4:Y4')->applyFromArray(borderExterno());

	$objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(23); // 21 = 28 px / 22 = 29 px

	$i=1; $letra='A';
	while($i<=17)
	{
		$objPHPExcel->getActiveSheet()->setCellValue($letra.'3', $i);

		if(in_array($i, array(7, 8, 9, 11, 12, 13, 14, 15, 16)))
			$objPHPExcel->getActiveSheet()->getStyle($letra.'3')->getFont()->setBold(true);

		$letra++; 
		$i++;		
	}

	// Consulta acreedores
	$sql = "SELECT concepto FROM caa_conceptos WHERE variable='acreedores'";
	$res = $conexion->query($sql);

	$where_acreedores = '';	
	if($fila = $res->fetch_array())
	{
		$limite = explode(':', $fila['concepto'] );

		if(count($limite) > 1)
			$where_acreedores = 'n.codcon BETWEEN '.$limite[0].' AND '.$limite[1];
		else
			$where_acreedores = 'n.codcon = ' . $fila['concepto'];
	}

	//----------------------------------------------------------------------------------------------
	// Escala de color escalonada (Escala de 3 colores)
		$objConditional = new PHPExcel_Style_Conditional();

		$red    = new PHPExcel_Style_Color("F8696B"); // Valor más bajo
		$yellow = new PHPExcel_Style_Color("FFEB84"); // Percentil (Punto medio)
		$green  = new PHPExcel_Style_Color("63BE7B"); // Valor más alto

		$objConditional->setConditionType(PHPExcel_Style_Conditional::CONDITION_COLORSCALE)
   					->setColorScaleStop(1, $red,    PHPExcel_Style_Conditional::STOP_MIN, 0)
           			->setColorScaleStop(2, $yellow, PHPExcel_Style_Conditional::STOP_PERCENTILE, 50)
           			->setColorScaleStop(3, $green,  PHPExcel_Style_Conditional::STOP_MAX, 100);
	//----------------------------------------------------------------------------------------------

	$sql = "SELECT DISTINCT n1.codorg, n1.descrip 
			FROM nom_movimientos_nomina n
			INNER JOIN nomnivel1 n1 ON n.codnivel1=n1.codorg
			WHERE n.codnom='{$codnom}'
			ORDER BY 1";
	$res = $conexion->query($sql);

	$i=6; $num_comprobante = 1;
	$i1 = 3; // Variable i de la hoja 1 => COMPROBANTES ADV
	$i5 = 4; // Variable i de la hoja 5 => VALES
	$i6 = 3; // Variable i de la hoja 6 => Hoja 1
	while($codnivel1 = $res->fetch_object())
	{
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $codnivel1->descrip);
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':Y'.$i);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
		cellColor('A'.$i.':Z'.$i, '0F243E');  // Color de fondo de las celdas
		colorTexto('A'.$i.':Z'.$i, 'FFFFFF'); // Color de texto

		$sql2 = "SELECT DISTINCT n.ficha, p.cedula, p.apenom as nombre_completo, p.suesal, p.codcat, p.hora_base
				 FROM nom_movimientos_nomina n
				 INNER JOIN nompersonal p ON n.ficha=p.ficha
				 WHERE n.codnom='{$codnom}' AND p.codnivel1='{$codnivel1->codorg}'";

		$res2  = $conexion->query($sql2);

		$j=0;

		while($personal = $res2->fetch_object())
		{
			$i++;

			$salario_hora = $personal->suesal;

			if($personal->codcat != 8 && $personal->hora_base>0)
				$salario_hora = ($personal->suesal*12/52)/$personal->hora_base;

			$horas_regular = $horas_nacional = $horas_tardanza = $horas_ausencia = 0;
			$valor_otros = $valor_acreedores = 0;

			$sql3 = "SELECT n.valor as horas 
					 FROM   nom_movimientos_nomina n
					 WHERE  n.codnom='{$codnom}' AND n.ficha='{$personal->ficha}' 
					 AND    n.codcon=(SELECT concepto FROM caa_conceptos WHERE variable='regular')";

			$res3 = $conexion->query($sql3);

			if($res3->num_rows > 0) 
				$horas_regular = $res3->fetch_object()->horas;			

			$sql3 = "SELECT n.valor as horas 
					 FROM   nom_movimientos_nomina n
					 WHERE  n.codnom='{$codnom}' AND n.ficha='{$personal->ficha}' 
					 AND    n.codcon=(SELECT concepto FROM caa_conceptos WHERE variable='nacional')";

			$res3 = $conexion->query($sql3);

			if($res3->num_rows > 0)
				$horas_nacional = $res3->fetch_object()->horas;

			$sql3 = "SELECT n.valor as horas 
					 FROM   nom_movimientos_nomina n
					 WHERE  n.codnom='{$codnom}' AND n.ficha='{$personal->ficha}' 
					 AND    n.codcon=(SELECT concepto FROM caa_conceptos WHERE variable='tardanza')";

			$res3 = $conexion->query($sql3);

			if($res3->num_rows > 0)
				$horas_tardanza = $res3->fetch_object()->horas;

			$sql3 = "SELECT n.valor as horas 
					 FROM   nom_movimientos_nomina n
					 WHERE  n.codnom='{$codnom}' AND n.ficha='{$personal->ficha}' 
					 AND    n.codcon=(SELECT concepto FROM caa_conceptos WHERE variable='ausencia')";

			$res3 = $conexion->query($sql3);

			if($res3->num_rows > 0)
				$horas_ausencia = $res3->fetch_object()->horas;

			$sql3 = "SELECT n.valor
					 FROM   nom_movimientos_nomina n
					 WHERE  n.codnom='{$codnom}' AND n.ficha='{$personal->ficha}' 
					 AND    n.codcon=(SELECT concepto FROM caa_conceptos WHERE variable='otros')";

			$res3 = $conexion->query($sql3);

			if($res3->num_rows > 0)
				$valor_otros = $res3->fetch_object()->valor;

			$sql3 = "SELECT COALESCE(SUM(n.valor),0) as valor
					 FROM   nom_movimientos_nomina n
					 WHERE  n.codnom='{$codnom}' AND n.ficha='{$personal->ficha}' 
					 AND    " . $where_acreedores;

			$res3 = $conexion->query($sql3);

			if($res3->num_rows > 0)
				$valor_acreedores = $res3->fetch_object()->valor;

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $personal->ficha);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $personal->cedula);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $num_comprobante);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $personal->nombre_completo);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $salario_hora);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $horas_regular);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, '=E'.$i.'*F'.$i);
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $horas_nacional);
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, '=E'.$i.'*J'.$i.'*2.5');
			$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, '=F'.$i.'+J'.$i);
			$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=G'.$i.'+K'.$i);
			$objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $horas_tardanza);
			$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $horas_ausencia);
			$objPHPExcel->getActiveSheet()->setCellValue('T'.$i, '=Q'.$i);
			$objPHPExcel->getActiveSheet()->setCellValue('U'.$i, '=T'.$i.'*9.75%');
			$objPHPExcel->getActiveSheet()->setCellValue('V'.$i, '=T'.$i.'*1.25%');
			$objPHPExcel->getActiveSheet()->setCellValue('W'.$i, ($valor_otros==0 ? '' : $valor_otros) );
			$objPHPExcel->getActiveSheet()->setCellValue('X'.$i, ($valor_acreedores==0 ? '' : $valor_acreedores) );
			$objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, '=U'.$i.'+V'.$i.'+W'.$i.'+X'.$i);
			$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '=T'.$i.'-Y'.$i);

			// Celdas en negrita
			$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->getFont()->setBold(true);

			// Celdas con formato de numero
			$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('Q'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('U'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('Y'.$i)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');

			$objPHPExcel->getActiveSheet()->getStyle('W'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->getNumberFormat()->setFormatCode('#,##0.00');

			// Celdas alineadas a la izquierda
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			// Celdas alineadas al centro
			$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			if($j%2!=0)
				cellColor('A'.$i.':Y'.$i, 'D9D9D9');

			cellColor('W'.$i.':X'.$i, 'CCECFF'); // Azul columnas otros y acreedores

			//==========================================================================================
			//==========================================================================================
			// Con los datos de la primera hoja de cálculo debemos crear las otras hojas de cálculo
			// Hoja de cálculo 1 => COMPROBANTES ADV
			$objPHPExcel->setActiveSheetIndex(1);

			$objPHPExcel->getActiveSheet()
						->setCellValue('A'.($i1),   'BRENTWOOD')
						->setCellValue('A'.($i1+1), $titulo_hoja0)
						->setCellValue('A'.($i1+3), 'COMPROBANTE DE PAGO No')
						->setCellValue('G'.($i1+3), "='".$titulo_hoja0."'!C".$i) // $num_comprobante);
					    ->setCellValue('A'.($i1+5), 'CODIGO')
					    ->setCellValue('B'.($i1+5),	"='".$titulo_hoja0."'!A".$i) // $personal->ficha
					    ->setCellValue('D'.($i1+5), "='".$titulo_hoja0."'!D".$i) // $personal->nombre_completo
					    ->setCellValue('F'.($i1+5), 'CEDULA')
					    ->setCellValue('G'.($i1+5),  "='".$titulo_hoja0."'!B".$i) // $personal->cedula
						->setCellValue('A'.($i1+6), 'SALARIO BRUTO')
				    	->setCellValue('D'.($i1+6), "='".$titulo_hoja0."'!Q".$i) // "='".$titulo_hoja0."'!T".$i
					    ->setCellValue('A'.($i1+7), 'RxH')
					    ->setCellValue('D'.($i1+7), "='".$titulo_hoja0."'!E".$i) // $salario_hora
					    ->setCellValue('F'.($i1+7), 'TOTAL HORAS')
					    ->setCellValue('G'.($i1+7), "='".$titulo_hoja0."'!P".$i)
					    ->setCellValue('A'.($i1+9), 'SALARIO')
					    ->setCellValue('E'.($i1+9), 'DEDUCCIONES')
					    ->setCellValue('A'.($i1+10), 'REGULAR')
					    ->setCellValue('D'.($i1+10), "='".$titulo_hoja0."'!G".$i)
					    ->setCellValue('E'.($i1+10), 'SEGURO SOCIAL')
					    ->setCellValue('G'.($i1+10), "='".$titulo_hoja0."'!U".$i)
					    ->setCellValue('A'.($i1+11), 'DOMINGO')
					    ->setCellValue('D'.($i1+11), 0)
					    ->setCellValue('E'.($i1+11), 'SEGURO EDUCATIVO')
					    ->setCellValue('G'.($i1+11), "='".$titulo_hoja0."'!V".$i)
					    ->setCellValue('A'.($i1+12), 'NACIONALES')
					    ->setCellValue('D'.($i1+12), "='".$titulo_hoja0."'!K".$i)
					    ->setCellValue('A'.($i1+13), 'AUSENCIAS')
					    ->setCellValue('D'.($i1+13), 0)
					    ->setCellValue('E'.($i1+13), 'OTROS')
					    ->setCellValue('G'.($i1+13), "='".$titulo_hoja0."'!W".$i."+'".$titulo_hoja0."'!X".$i)
						->setCellValue('A'.($i1+14), 'TOTAL SALARIO')
					    ->setCellValue('D'.($i1+14), "='".$titulo_hoja0."'!T".$i)
					    ->setCellValue('E'.($i1+14), 'TOTAL DEDUCCIONES')
					    ->setCellValue('G'.($i1+14), "='".$titulo_hoja0."'!Y".$i)
					    ->setCellValue('E'.($i1+16), 'TOTAL NETO RECIBIDO:')
					    ->setCellValue('G'.($i1+16), "='".$titulo_hoja0."'!Z".$i)
					    ->setCellValue('A'.($i1+19), 'RECIBIDO CONFORME')
					    ->setCellValue('E'.($i1+19), 'FECHA');			    			

			// Celdas con modificaciones en la fuente (tipo diferente, tamaño)
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i1)->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+3).':G'.($i1+3))->getFont()->setName('Arial');
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+5).':G'.($i1+5))->getFont()->setName('Arial')->setSize(12);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+7).':G'.($i1+7))->getFont()->setName('Arial')->setSize(12);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+1))->getFont()->setName('Calibri');
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+9))->getFont()->setName('Calibri');	
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+9))->getFont()->setName('Calibri');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+14))->getFont()->setName('Calibri');
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+14))->getFont()->setName('Calibri');
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+16))->getFont()->setName('Calibri');

			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+10))->getFont()->setName('Consolas');	
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+10))->getFont()->setName('Consolas');	
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+11))->getFont()->setName('Consolas');
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+11))->getFont()->setName('Consolas');
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+12))->getFont()->setName('Consolas');
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+13))->getFont()->setName('Consolas');
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+13))->getFont()->setName('Consolas');

			$objPHPExcel->getActiveSheet()->getStyle('A'.$i1)->getFont()->setSize(28)->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+1))->getFont()->setSize(12)->setBold(true);	
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+3) )->getFont()->setSize(10)->setBold(true)->setItalic(true);
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+3))->getFont()->setSize(12);
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+16))->getFont()->setSize(16);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+19))->getFont()->setName('Calibri');	
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+19))->getFont()->setName('Calibri');
			
			// Celdas combinadas 
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$i1.':G'.$i1);
			$objPHPExcel->getActiveSheet()->mergeCells('A'.($i1+1).':G'.($i1+1));
			$objPHPExcel->getActiveSheet()->mergeCells('A'.($i1+3).':F'.($i1+3)); 
			$objPHPExcel->getActiveSheet()->mergeCells('A'.($i1+9).':C'.($i1+9)); 
			$objPHPExcel->getActiveSheet()->mergeCells('E'.($i1+9).':G'.($i1+9)); 
			$objPHPExcel->getActiveSheet()->mergeCells('A'.($i1+19).':B'.($i1+19));

			// Celdas en negrita
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+5))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('F'.($i1+5))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+6).':D'.($i1+6))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+7).':G'.($i1+7))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+9).':G'.($i1+9))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+10))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+10))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+11))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+11))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+12))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+13))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+13))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+14).':G'.($i1+14))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+16).':G'.($i1+16))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+19).':G'.($i1+19))->getFont()->setBold(true);

			// Celdas con borde completo 
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+3))->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_MEDIUM));
			$objPHPExcel->getActiveSheet()->getStyle('B'.($i1+5))->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_MEDIUM));
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+7))->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_MEDIUM));

			// Celdas con borde superior
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+9).':G'.($i1+9))->applyFromArray(borderTop(PHPExcel_Style_Border::BORDER_MEDIUM));
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+14).':G'.($i1+14))->applyFromArray(borderTop(PHPExcel_Style_Border::BORDER_MEDIUM));
		    $objPHPExcel->getActiveSheet()->getStyle('A'.($i1+17).':G'.($i1+17))->applyFromArray(borderTop(PHPExcel_Style_Border::BORDER_THICK));


			// Celdas con borde inferior
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+1).':G'.($i1+1))->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_THICK)); 
			$objPHPExcel->getActiveSheet()->getStyle('C'.($i1+19).':D'.($i1+19))->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_THIN));
			$objPHPExcel->getActiveSheet()->getStyle('F'.($i1+19).':G'.($i1+19))->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_THIN));
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+17).':G'.($i1+17))->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_THICK));
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+21).':G'.($i1+21))->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_THICK));
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+26).':G'.($i1+26))->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_MEDIUMDASHDOTDOT));

			// Celdas con borde izquierdo
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+9).':A'.($i1+17))->applyFromArray(borderLeft(PHPExcel_Style_Border::BORDER_MEDIUM));
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+9).':E'.($i1+17))->applyFromArray(borderLeft(PHPExcel_Style_Border::BORDER_MEDIUM));
			$objPHPExcel->getActiveSheet()->getStyle('H'.($i1+9).':H'.($i1+17))->applyFromArray(borderLeft(PHPExcel_Style_Border::BORDER_MEDIUM));

			// Celdas con texto alineado al centro
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i1.':G'.($i1+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+3))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+7))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+9).':C'.($i1+9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+9).':G'.($i1+9))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			// Celdas con texto alineado a la izquierda
			$objPHPExcel->getActiveSheet()->getStyle('B'.($i1+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			// Celdas con texto alineado a la derecha
			$objPHPExcel->getActiveSheet()->getStyle('A'.($i1+3))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getStyle('E'.($i1+19))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			// Formato Personalizado Celdas
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+6))->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+10))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+10))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+11))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+11))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+12))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+13))->getNumberFormat()->setFormatCode('#,##0.00');	
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+13))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('D'.($i1+14))->getNumberFormat()->setFormatCode('#,##0.00');			
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+14))->getNumberFormat()->setFormatCode('#,##0.00');
			$objPHPExcel->getActiveSheet()->getStyle('G'.($i1+16))->getNumberFormat()->setFormatCode('#,##0.00');

			// Ajustar Texto
			// $objPHPExcel->getActiveSheet()->getStyle('D'.($i1+5))->getAlignment()->setWrapText(true); 

			// Add a page break
			// $objPHPExcel->getActiveSheet()->setBreak('A58', PHPExcel_Worksheet::BREAK_ROW );
			// $objPHPExcel->getActiveSheet()->setBreak('G1', PHPExcel_Worksheet::BREAK_COLUMN );

			$i1 = $i1 + 35; // $i1 = 3 + 35; $i1 = 38; Primera pasada
			//======================================================================================
			// Hoja de cálculo => Administrativos
			if($codnivel1->codorg==2)
			{
				$objPHPExcel->setActiveSheetIndex(4);		

				$objPHPExcel->getActiveSheet()->setCellValue('A'.($j+4), $personal->nombre_completo)
											  ->setCellValue('B'.($j+4), "='".$titulo_hoja0."'!Z".$i);
				$objPHPExcel->getActiveSheet()->getStyle('A'.($j+4).':H'.($j+4))->getFont()->setName('Century Gothic');
				$objPHPExcel->getActiveSheet()->getStyle('B'.($j+4))->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('B'.($j+4))->getNumberFormat()->setFormatCode('"B/." * #,##0.00');				
				$objPHPExcel->getActiveSheet()->getStyle('A'.($j+4).':H'.($j+4))->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));	
				$objPHPExcel->getActiveSheet()->getStyle('C'.($j+4))->applyFromArray(borderRight(PHPExcel_Style_Border::BORDER_MEDIUM));			
			}
			//======================================================================================
			// Hoja de cálculo => VALES
			if($valor_otros>0)
			{
				$objPHPExcel->setActiveSheetIndex(5);

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i5, $personal->nombre_completo)
											  ->setCellValue('B'.$i5, $valor_otros);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$i5)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$i5)->getNumberFormat()->setFormatCode('#,##0.00');

				$i5++;
			}
			//======================================================================================
			// Hoja de cálculo => Hoja 1
			if($codnivel1->codorg!=2)
			{
				$objPHPExcel->setActiveSheetIndex(6);

				if($i6%45==0)
				{
					// Add a page break
					$objPHPExcel->getActiveSheet()->setBreak('A'.($i6-1), PHPExcel_Worksheet::BREAK_ROW );
					//$objPHPExcel->getActiveSheet()->setBreak('G'.($i6-1), PHPExcel_Worksheet::BREAK_COLUMN );

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i6, 'CÓDIGO')
												  ->setCellValue('B'.$i6, 'NOMBRE')
												  ->setCellValue('C'.$i6, 'REGULAR')
												  ->setCellValue('D'.$i6, 'BRUTO EXTRA')
												  ->setCellValue('F'.$i6, 'TOTAL EXTRA');
					cellColor('A'.$i6.':F'.$i6, '0F243E');  // Celdas con color de fondo	
					colorTexto('A'.$i6.':F'.$i6, 'FFFFFF'); // Celdas con color de texto diferente	
					$i6++;				
				}

				$objPHPExcel->getActiveSheet()->getStyle('A'.$i6.':F'.$i6)->getFont()->setName('Century Gothic');

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i6, $personal->ficha)
											  ->setCellValue('B'.$i6, $personal->nombre_completo)
											  ->setCellValue('C'.$i6, "='".$titulo_hoja0."'!Z".$i);

				$objPHPExcel->getActiveSheet()->getStyle('C'.$i6)->getNumberFormat()->setFormatCode('#,##0.00');

				$i6++;
			}
			//======================================================================================
			$objPHPExcel->setActiveSheetIndex(0);
			$num_comprobante++;
			$j++;
		}

		if($codnivel1->codorg==2)
		{
			$objPHPExcel->setActiveSheetIndex(4);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.($j+4), '=SUM(B4:B'.($j-3).')');
			$objPHPExcel->getActiveSheet()->getStyle('B'.($j+4))->getNumberFormat()->setFormatCode('"B/." * #,##0.00');	
			$objPHPExcel->setActiveSheetIndex(0);
		}

		$i++;
	}

	//----------------------------------------------------------------------------------------------
	// Escala de color escalonada (Escala de 3 colores)
		$conditionalStyles = $objPHPExcel->getActiveSheet()
										 ->getStyle('Z7:Z'.$i)->getConditionalStyles();
		array_push($conditionalStyles, $objConditional);
		$objPHPExcel->getActiveSheet()->getStyle('Z7:Z'.$i)->setConditionalStyles($conditionalStyles);
	//----------------------------------------------------------------------------------------------

	$objPHPExcel->getActiveSheet()->getStyle('A6:Z'.($i-1))->applyFromArray(allBorders());

	$objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, '=SUM(Z7:Z'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->getNumberFormat()->setFormatCode('#,##0.00');
	$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray(borderRight(PHPExcel_Style_Border::BORDER_THIN));

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':Z'.$i)->applyFromArray(borderTop(PHPExcel_Style_Border::BORDER_DOUBLE));
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':Z'.$i)->applyFromArray(borderBottom(PHPExcel_Style_Border::BORDER_THIN));

	$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(70);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11); // 10,29 (77 px)  => Original: 10,43 (78 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14); // 13,43 (99 px)  => Original: 13,71 (101 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12); // 11,29 (84 px)  => Original: 11,14 (83 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(39); // 38,29 (273 px) => Original: 38,14 (272 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17); // 16,29 (119 px) => Original: 16,00 (117 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(14); // 13,43 (99 px)  => Original: 13,14 (97 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16); // 15,14 (111 px) => Original: 15,29 (112 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(0);  // 0
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(0);  // 0 
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(16); // 15,14 (111 px) => Original: 15,43 (113 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12); // 11,29 (84 px)  => Original: 11,71 (87 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(0);  // 0
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(0);  // 0
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(0);  // 0
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(0);  // 0
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14); // 13,43 (99 px) => Original: 13,71 (101 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(16); // 15,14 (111 px) => Original: 15,29 (112 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10); // 9,29 (70 px)  => Original: 9,29 (70 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(11); // 10,29 (77 px) => Original: 9,86 (74 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20); // 19,29 (140 px) => Original: 19,00 (138 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(14); // 13,43 (99 px) => Original: 13,71 (101 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(14); // 13,43 (99 px) => Original: 13,71 (101 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(11); // 10,29 (77 px) => Original: 10,71 (80 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12); // 11,29 (84 px) => Original: 11,29 (84 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(13); // 12,29 (91 px) => Original: 12,57 (93 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(21); // 20,29 (147 px) => Original: 20,00 (145 px)

	//----------------------------------------------------------------------------------------------
	// Ancho de las columnas de la Hoja 1 => COMPROBANTES ADV
	$objPHPExcel->setActiveSheetIndex(1);

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(17);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);

	$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(77);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(70);
	$objPHPExcel->getActiveSheet()->getSheetView()->setView(PHPExcel_Worksheet_SheetView::SHEETVIEW_PAGE_BREAK_PREVIEW);
	//$objPHPExcel->getActiveSheet()->getSheetView()->setView(PHPExcel_Worksheet_SheetView::SHEETVIEW_PAGE_LAYOUT);

	//==============================================================================================
	// Hoja de cálculo => TOTAL A PAGAR AGS -BRENTWOOD
	$objPHPExcel->setActiveSheetIndex(3);

	$objPHPExcel->getActiveSheet()
				->setCellValue('A2', 'BRENTWOOD')
				->setCellValue('A3', 'REGULAR')
				->setCellValue('C3', '')
				->setCellValue('A4', 'EXTRAS')
				->setCellValue('A5', 'SERVICIOS PROFESIONALES')
				->setCellValue('A6', 'VALES Y PRESTAMOS')
				->setCellValue('A7', 'RECLAMOS')
				->setCellValue('A8', 'TOTAL')
				->setCellValue('C8', '')
				->setCellValue('A13', 'AGS')
				->setCellValue('A14', 'REGULAR')
				->setCellValue('A15', 'EXTRAS')
				->setCellValue('A16', 'SERVICIOS PROFESIONALES')
				->setCellValue('A17', 'VALES DESCONTADOS')
				->setCellValue('A18', 'RECLAMOS')
				->setCellValue('A19', 'TOTAL')
				->setCellValue('C19', '');

	// Celdas con color de fondo
	cellColor('A2:C2', '95B3D7'); // Azul
	cellColor('A13:C13', 'DA9694'); // Granate

	// Celdas en negrita
	$objPHPExcel->getActiveSheet()->getStyle('A2:A8')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('C8')->getFont()->setBold(true)->setSize(12);
	$objPHPExcel->getActiveSheet()->getStyle('A13:A19')->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle('C19')->getFont()->setBold(true)->setSize(14);

	// Celdas combinadas
	$objPHPExcel->getActiveSheet()->mergeCells('A2:C2');
	$objPHPExcel->getActiveSheet()->mergeCells('A3:B3');
	$objPHPExcel->getActiveSheet()->mergeCells('A4:B4');
	$objPHPExcel->getActiveSheet()->mergeCells('A5:B5');
	$objPHPExcel->getActiveSheet()->mergeCells('A6:B6');
	$objPHPExcel->getActiveSheet()->mergeCells('A7:B7');
	$objPHPExcel->getActiveSheet()->mergeCells('A8:B8');
	$objPHPExcel->getActiveSheet()->mergeCells('A13:C13');
	$objPHPExcel->getActiveSheet()->mergeCells('A14:B14');
	$objPHPExcel->getActiveSheet()->mergeCells('A15:B15');
	$objPHPExcel->getActiveSheet()->mergeCells('A16:B16');
	$objPHPExcel->getActiveSheet()->mergeCells('A17:B17');
	$objPHPExcel->getActiveSheet()->mergeCells('A18:B18');
	$objPHPExcel->getActiveSheet()->mergeCells('A19:B19');

	// Celdas con borde completo
	$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_MEDIUM));
	$objPHPExcel->getActiveSheet()->getStyle('A3:C8')->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));
	$objPHPExcel->getActiveSheet()->getStyle('A13:C13')->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_MEDIUM));
	$objPHPExcel->getActiveSheet()->getStyle('A14:C19')->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));

	// Celdas con formato de numero
	$objPHPExcel->getActiveSheet()->getStyle('C3:C8')->getNumberFormat()->setFormatCode('#,##0.00');
	$objPHPExcel->getActiveSheet()->getStyle('C14:C19')->getNumberFormat()->setFormatCode('"B/." * #,##0.00');

	// Celdas alineadas horizontalmente al centro
	$objPHPExcel->getActiveSheet()->getStyle('A2:A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A13:A19')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// Ancho de las columnas
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(19); // 18,29 (133 px) => Original: 18,43 (134 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17); // 16,29 (119 px) => Original: 16,57 (121 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(21); // 20,29 (147 px) => Original: 20,00 (145 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11); // 10,29 (77 px)  => Original: 10,71 (80 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(11); // 10,29 (77 px)  => Original: 10,71 (80 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(11); // 10,29 (77 px)  => Original: 10,71 (80 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(23); // 22,29 (161 px) => Original: 23,14 (167 px)
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18); // 17,29 (126 px)=> Original: 17,00 (124 px)


	// Nivel de zoom y vista previa de salto pagina
	$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(98);	
	$objPHPExcel->getActiveSheet()->getSheetView()->setView(PHPExcel_Worksheet_SheetView::SHEETVIEW_PAGE_BREAK_PREVIEW);

	// Ajustar área de impresión
	$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(63);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPrintArea('A1:H40');

	$objPHPExcel->getActiveSheet()->setSelectedCells('G14');
	//----------------------------------------------------------------------------------------------
	// Borde hoja de cálculo => VALES
	$objPHPExcel->setActiveSheetIndex(5);

	$objPHPExcel->getActiveSheet()->getStyle('A3:B'.($i5-1))
	                              ->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));

	if($i5>4)
	{
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i5, '=SUM(B4:B'.($i5-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i5, '=SUM(B4:B'.($i5-1).')');
		$objPHPExcel->getActiveSheet()->getStyle('B'.$i5)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');	
		$objPHPExcel->getActiveSheet()->getStyle('B'.$i5)->getFont()->setBold(true)->setSize(12);
	}

	//----------------------------------------------------------------------------------------------

	$objPHPExcel->setActiveSheetIndex(6);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setScale(94);

	if($i6>3)
	{
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i6, '=SUM(C3:C'.($i6-1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i6, '=SUM(F3:F'.($i6-1).')');
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i6.':F'.$i6)->getNumberFormat()->setFormatCode('"B/." * #,##0.00');
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i6.':F'.$i6)->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('A3:F'.($i6-1))
	                              ->applyFromArray(allBorders(PHPExcel_Style_Border::BORDER_THIN));
			
	}
	
	//==============================================================================================
	//==============================================================================================
	// Ubicarse en la primera hoja

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setSelectedCells('D140');

	$filename = str_replace(array("\"", "Á", "É", "Í", "Ó", "Ú", "á", "é", "í", "ó", "ú"), 
						    array('' , "A", "E", "I", "O", "U", "a", "e", "i", "o", "u"), 
						    $titulo_hoja0);

	$filename = 'excel/' . $filename . '.xlsx';

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');	

	$objWriter->save($filename);

	echo $filename;
}
else
{
   echo "<script>alert('Acceso Denegado');</script>";  
   echo "<script>document.location.href = 'config_rpt_nomina_brentwood.php';</script>";
}

exit;

function traducir_mes($fecha)
{
	$meses_in = array('january', 'february', 'march', 'april', 'may', 'june', 'july', 
					  'august', 'september', 'october', 'november', 'december');	
	$meses_es = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio',
					  'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');

	return str_replace($meses_in, $meses_es, $fecha);	
}

function str_replace_first($from, $to, $subject)
{
    $from = '/'.preg_quote($from, '/').'/';

    return preg_replace($from, $to, $subject, 1);
}