<?php 
require_once('../lib/database.php');

$db = new Database($_SESSION['bd']);

$registro_id=isset($_POST['registro_id']) ? $_POST['registro_id'] : '';	
$op=isset($_POST['op']) ? $_POST['op'] : '';

if ($op==3) //Se presiono el boton de Eliminar
{	
	$sql="DELETE FROM nompersonal WHERE personal_id='{$registro_id}'";	
		
	$res=$db->query($sql);	
	echo "<script> window.location.href='maestro_personal.php';</script>"; 
}  
/*
$sql  = "SELECT i.personal_id, i.foto, i.ficha, i.cedula, i.apenom, i.estado, i.nomposicion_id as posicion
		 FROM   nomvis_integrantes i 
	     WHERE  i.descrip = '".$_SESSION['nomina']."'";
$res  = $db->query($sql);
*/
$res1 = $db->query("SELECT tipo_empresa FROM nomempresa")->fetch_array();
$tipo_empresa = $res1['tipo_empresa']; 
?>
<?php include("../header4.php"); ?>
<link href="../../includes/assets/css/custom-datatables.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
/*.portlet > .portlet-title > .actions > .btn.btn-sm {
	margin-top: -9px !important;
}
*/
.portlet > .portlet-title > .actions > .btn {
    padding: 4px 10px !important;
    margin-top: -14px !important;
}
#table_datatable tbody tr td{
	vertical-align: middle;
}

#table_datatable_length .form-control {
    padding: 0px;
}

div.dataTables_filter label {
    float: left;
}

#table_datatable_wrapper .dataTables_filter input {
   margin-top: 5px;
   margin-bottom: 5px;
}

#table_datatable_wrapper div.dataTables_length label {
	margin-top: 5px;
}

@media (min-width: 513px) {
	#search_situ.input-small {
	    width: 122px !important;
	}
}
<?php
//if($tipo_empresa!=1)
//{ ?>
/*	.portlet > .portlet-title > .actions > .btn.btn-sm {
	    margin-top: -9px !important;
	}*/
<?php
// }
?>
</style>
<script type="text/javascript">
function enviar(op, id){

	if (op==3){		// Opcion de Eliminar
		if (confirm("\u00BFEst\u00E1 seguro que desea eliminar el registro?"))
		{
			document.frmPrincipal.op.value=op;			
			document.frmPrincipal.registro_id.value=id;
  			document.frmPrincipal.submit();
		}		
	}
}
</script>
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								Personal
							</div>
							<div class="actions">
<!-- manuel -->
<?php /*if ($tipo_empresa == 1) { ?>
<div class="btn-group">
  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Reportes Generales <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
	<li><a target="_blank" href="../../reporte_pub/pantalla_cambio_cedula.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe"
	href="../tcpdf/reportes/reporte_cambios_cedula.php?id=<?= $fila['ficha'] ?>" title="" style="cursor: pointer">
	<img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Modif. O Cambios de Cedula</a></li>
	<li><a target="_blank" href="../../reporte_pub/pantalla_baja.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
	<img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Bajas</a></li>
	<li><a target="_blank" href="../../reporte_pub/pantalla_activacion_baja.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
	<img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Activacion de Bajas</a></li>
	<li><a target="_blank" href="../../reporte_pub/pantalla_traslados.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
	<img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Traslados</a></li>
	<li><a target="_blank" href="../../reporte_pub/pantalla_solicitud_descuentos.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
	<img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Solicicitud Descuentos</a></li>
	<li><a target="_blank" href="../../reporte_pub/pantalla_cambio_cedula2.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
	<img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Modificaciones</a></li>
  </ul>
</div>
<?php } */?>						<!--
								<a class="btn btn-sm blue"  onclick="javascript: window.location='IntegrantesForm.php'">
									<i class="fa fa-plus"></i>
									Agregar
								</a>-->
								
								<a class="btn btn-sm blue"  onclick="javascript: window.location='ag_integrantes.php'">
									<i class="fa fa-plus"></i>
									Agregar
								</a>
								

								
								<!--
								<a class="btn btn-sm blue"  onclick="javascript: window.location='menu_configuracion.php'">
									<i class="fa fa-arrow-left"></i> Regresar
								</a>
								-->
							</div>
						</div>
						<div class="portlet-body">
							<div id="div_search_situ" style="display: inline;">
								<select id="search_situ" class="form-control input-inline input-small">
									<option value="Todos">Buscar por situación</option>
									<?php 
										$sql1 = "SELECT * FROM nomsituaciones";
										$res1 = $db->query($sql1);

										while($fila = $res1->fetch_array())
										{ ?>
											<option value="<?php echo $fila['situacion']; ?>"><?php echo $fila['situacion']; ?></option>
										  <?php
										}
									?>										
								</select>
							</div>
							<!-- <div class="table-container"> -->
							<form action="" method="post" name="frmPrincipal" id="frmPrincipal">
							<table class="table table-striped table-bordered table-hover" id="table_datatable">
							<thead>
							<tr>
								<th>Foto</th>
								<th># Colab.</th>
								<th>C&eacute;dula</th>
								<th>Apellidos y nombres</th>
								<th>Situaci&oacute;n</th>
								<?php if($tipo_empresa == 1){?><th>Posici&oacute;n</th><?php } ?>				
								<th>Salario</th>		
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<!-- manuel -->
								<?php/* if ($tipo_empresa == 1) { ?>
								<th>&nbsp;</th>	
								<th>&nbsp;</th>
								<?php }*/ ?>
							</tr>
							</thead>
							<tbody>
							<?php
								/*
								while( $fila = $res->fetch_array() )
								{
									$foto = $fila['foto'];
									$foto = ( !empty($foto) && is_file($foto) && file_exists($foto) ) ? $foto : 'fotos/silueta.gif' ;
								?>

									<tr class="odd gradeX">
										<td><img width="48" height="48" src="<?php echo $foto; ?>" /></td>
										<td><?php echo $fila['ficha']; ?></td>
										<td><?php echo $fila['cedula']; ?></td>
										<td><?php echo $fila['apenom']; ?></td>
										<td><?php echo $fila['estado']; ?></td>
										<?php if($tipo_empresa == 1){?><td><?php echo $fila['posicion']; ?></td><?php } ?>		
										<td style="text-align: center; width: 22px !important">
											<!--
									    	<a href="IntegrantesForm.php?edit&ficha=<?php echo $fila['ficha']; ?>" title="Editar">
									     	<img src="../../includes/imagenes/icons/pencil.png" width="16" height="16"></a>
									      
									    	<br> -->
									    	<a href="ag_integrantes.php?ficha=<?php echo $fila['ficha']; ?>&edit" title="Editar"><img src="../../includes/imagenes/icons/pencil.png" width="16" height="16"></a>
											<!--<br>
											<a href="datos_integrantes/editar_integrante.php?personal_id=<?php echo $fila['personal_id']; ?>&editar" title="Editar"><img src="../../includes/imagenes/icons/pencil.png" width="16" height="16"></a>
											-->
										</td>
										<td style="text-align: center; width: 22px !important">
									      <a href="calendarios_personal.php?anio=<?php echo ''; ?>&ficha=<?php echo $fila['ficha']; ?>" title="Ver Calendario">
									      <img src="../../includes/imagenes/icons/calendar.png" width="16" height="16"></a>
										</td>
										<td style="text-align: center; width: 22px !important">
									      <a href="familiares.php?cedula=<?php echo $fila['cedula']; ?>&txtficha=<?php echo $fila['ficha']; ?>" title="Cargas Familiares">
									      <img src="../../includes/imagenes/icons/group.png" width="16" height="16"></a>
										</td>
										<td style="text-align: center; width: 22px !important">
									      <a href="otrosdatos_integrantes.php?txtficha=<?php echo $fila['ficha']; ?>" title="Campos Adicionales">
									      <img src="../../includes/imagenes/icons/table_multiple.png" width="16" height="16"></a>
										</td>
										<td style="text-align: center; width: 22px !important">
									      <a href="../fpdf/datos_personal.php?cedula=<?php echo $fila['cedula']; ?>&ficha=<?php echo $fila['ficha']; ?>" title="Imprimir">
									      <img src="../../includes/imagenes/icons/printer.png" width="16" height="16"></a>
										</td>
										<td style="text-align: center; width: 22px !important">
									      <a href="../expediente/expediente_list_ajax.php?cedula=<?php echo $fila['cedula']; ?>" title="Ver Expediente">
									      <img src="../../includes/imagenes/icons/folder_page.png" width="16" height="16"></a>
										</td>
										<td style="text-align: center; width: 22px !important">
											  <!-- href="javascript:enviar(<?php // echo(3); ?>,<?php // echo($fila['ficha']); ?>);" -->
										      <a  title="Eliminar" style="cursor: pointer">
										      <img src="../../includes/imagenes/icons/delete.png" alt="Eliminar" width="16" height="16"></a>
					                    </td>
										<!-- manuel-->
										<?php /*if ($tipo_empresa == 1) { ?>
										<td style="text-align: left; width: 22px !important">
											<a target="_blank" href="../../reporte_pub/inclusiones.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe"  style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Incluciones</a><br>

											<a target="_blank" href="../../reporte_pub/descuentos.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Descuentos</a><br>

											<a target="_blank" href="../../reporte_pub/envios.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
											<img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Envio Licencia</a><br>

											<a target="_blank" href="../../reporte_pub/ajustes.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
											<img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Ajuste Sueldo</a><br>

											<a target="_blank" href="../../reporte_pub/retornos.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
											<img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Retorno Licencia</a><br>
											
											<a target="_blank" href="../../reporte_pub/adicional.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe" style="cursor: pointer">
											<img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Adicional</a><br>

											<a target="_blank" href="../../reporte_pub/modificaciones.php?id=<?= $fila['ficha'] ?>" class="fancybox fancybox.iframe"  style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/application.png" alt="" width="16" height="16"> Modificaciones</a><br>
											
										</td>
										<td style="text-align: left; width: 22px !important">
											<a target="_blank" href="../tcpdf/reportes/reporte_inclusiones.php?id=<?= $fila['ficha'] ?>" title="" style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Incluciones</a><br>
											<a target="_blank" href="../tcpdf/reportes/reporte_ajuste_sueldo.php?id=<?= $fila['ficha'] ?>" title="" style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Ajust.Sueldo</a><br>
											<a target="_blank" href="../tcpdf/reportes/reporte_envio_licencia.php?id=<?= $fila['ficha'] ?>" title="" style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Env.Licencia</a><br>
											<a target="_blank" href="../tcpdf/reportes/reporte_retorno_licencia.php?id=<?= $fila['ficha'] ?>" title="" style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Ret.Licencia</a><br>
										    <a target="_blank" href="../tcpdf/reportes/reporte_adicional_pago.php?id=<?= $fila['ficha'] ?>" title="" style="cursor: pointer">
										    <img src="../../includes/imagenes/icons/printer.png" alt="" width="16" height="16"> Adicional Pago</a><br>
					                    </td>
										<?php }?>					
									
									</tr>
								  <?php							
								} */
							?>
							</tbody>
							</table>
							    <input name="registro_id" type="hidden" value="">
							    <input name="op" type="hidden" value="">	
							</form>
							<!-- </div> -->
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<?php include("../footer4.php"); ?>
<script type="text/javascript">
<?php
$data = "{ 'bSortable':   false, 'aTargets': [0, 5, 6, 7, 8, 9, 10, 11] }, " .
        "{ 'bSearchable': false, 'aTargets': [0, 5, 6, 7, 8, 9, 10, 11] }";

if($tipo_empresa=='1')
{
	$data = "{ 'bSortable':   false, 'aTargets': [0, 6, 7, 8, 9, 10, 11, 12] }, " .
	        "{ 'bSearchable': false, 'aTargets': [0, 6, 7, 8, 9, 10, 11, 12] }";
}
?>

var dataColumnDefs = [<?php echo $data; ?>];

$(document).ready(function() { 

 	var oTable = $('#table_datatable').DataTable({
		        "bProcessing": true,
		        "bServerSide": true,
				"sAjaxSource": "ajax/server_processing_listado_integrantes.php", 
 				"sDom": "<'row'<'col-md-3 col-sm-12'l><'col-md-9 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            	"iDisplayLength": 25,
            	"sPaginationType": "bootstrap_extended",
            	"aaSorting": [[ 1, "asc" ]], // Ordenar por columna 1 (Ficha) 
                "oLanguage": {
                	"sSearch": "",
                    "sLengthMenu": "Mostrar _MENU_",
                    "sInfoEmpty": "",
                    "sInfo":"Total _TOTAL_ registros",
                    "sInfoFiltered": "",
          		    "sEmptyTable":  "No hay datos disponibles", 
                    "sZeroRecords": "No se encontraron registros",
                    "oPaginate": {
                        "sPrevious": "P&aacute;gina Anterior",
                        "sNext": "P&aacute;gina Siguiente",
                        "sPage": "P&aacute;gina",
                        "sPageOf": "de",
                    }
                },
                "aLengthMenu": [ 
                    [5, 10, 25, 50,  -1],
                    [5, 10, 25, 50, "Todos"]
                ],                
                "aoColumnDefs": dataColumnDefs,
				"fnDrawCallback": function() {
				        $('#table_datatable_filter input').attr("placeholder", "Escriba frase para buscar");
				}
            });

            $('#table_datatable_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); 
            $('#table_datatable_wrapper .dataTables_length select').addClass("form-control input-small"); 
            $('#table_datatable_wrapper .dataTables_length select').select2({
                showSearchInput : false //hide search box with special css class
            }); // initialize select2 dropdown

            $('#div_search_situ').insertBefore("#table_datatable_wrapper .dataTables_filter input");

            $('#table_datatable_wrapper .dataTables_filter input').after(' <a class="btn blue" id="btn-search"><i class="fa fa-search"></i> Buscar</a> ');

            
		    $("#btn-search").click( function()
		    {
		    	 var valor_buscar =$('#search_situ').val();

		    	 if( valor_buscar == 'Todos' )
		    	 {
		    	 	valor_buscar = '';
		    	 }

		    	 // Se filtra por la columna 4 - Situación
		    	 oTable.fnFilter( valor_buscar, 4 );
		    });
});
</script>
<!-- manuel -->
<script type="text/javascript" src="../../reporte_pub/js/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../../reporte_pub/css/jquery.fancybox.css" media="screen" />
<script>
    $(document).ready(function() {
        $('.fancybox').fancybox( {topRatio:0,width:1000} );
    });
</script>
</body>
</html>