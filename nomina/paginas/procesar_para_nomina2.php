<?php
session_start();
ob_start();
?>
<?php
require_once '../lib/config.php';
require_once '../lib/common.php';
include ("../header.php");
include ("func_bd.php");
$conexion=conexion();

$reg=$_GET[reg];


//$vaciar="TRUNCATE TABLE reloj_procesar";
//$query=query($vaciar,$conexion);

#$datos="select * from control_acceso where cod_enca=".$_GET['reg'];
/*
$datos="select * from reloj_detalle rd inner join reloj_encabezado re on rd.id_encabezado=re.cod_enca where re.cod_enca='$reg' and rd.concepto='Entrada'";
$rs = query($datos,$conexion);
if((num_rows($rs)%2)!=0)
{
	header("Location:control_acceso2.php?listo=2");
	exit;
}

$datos="select * from reloj_detalle rd inner join reloj_encabezado re on rd.id_encabezado=re.cod_enca where re.cod_enca='$reg' and rd.concepto='Salida'";
$rs = query($datos,$conexion);
if((num_rows($rs)%2)!=0)
{
	header("Location:control_acceso2.php?listo=2");
	exit;
}
*/
$datos="SELECT
     reloj_encabezado.cod_enca,
     reloj_encabezado.fecha_reg,
     reloj_encabezado.fecha_ini,
     reloj_encabezado.fecha_fin,
     reloj_detalle.id,
     reloj_detalle.id_encabezado,
     reloj_detalle.ficha,
     reloj_detalle.fecha,
     nomturnos.turno_id,
     nomturnos.descripcion,
     nomturnos.entrada,
     nomturnos.tolerancia_entrada,
     nomturnos.inicio_descanso,
     nomturnos.salida_descanso,
     nomturnos.tolerancia_descanso,
     nomturnos.salida,
     nomturnos.tolerancia_salida,
     nompersonal.apenom,
     reloj_detalle.entrada,
     reloj_detalle.salmuerzo,
     reloj_detalle.ealmuerzo,
     reloj_detalle.salida,
     reloj_detalle.ordinaria,
     reloj_detalle.extra,
     reloj_detalle.extraext,
     reloj_detalle.extranoc,
     reloj_detalle.extraextnoc,
     reloj_detalle.domingo,
     reloj_detalle.tardanza,
     reloj_detalle.nacional,
     reloj_detalle.extranac,
     reloj_detalle.extranocnac,
     reloj_detalle.descextra1,
     reloj_detalle.mixtodiurna,
     reloj_detalle.mixtonoc,
     reloj_detalle.mixtoextdiurna,
     reloj_detalle.mixtoextnoc,
     reloj_detalle.dialibre,
     reloj_detalle.emergencia,
     reloj_detalle.descansoincompleto
FROM
     nomturnos INNER JOIN nompersonal ON nomturnos.turno_id = nompersonal.turno_id
     INNER JOIN reloj_detalle ON nompersonal.ficha = reloj_detalle.ficha
     INNER JOIN reloj_encabezado ON reloj_detalle.id_encabezado = reloj_encabezado.cod_enca
where reloj_encabezado.cod_enca='$reg' order by ficha, fecha";
$rs = query($datos,$conexion);
//if((num_rows($rs)%2)==0)
//{
$i=1;
$fichaaux='';
$color=0;
$conv = 0 ;
$insert ="insert into reloj_procesar (ficha, fecha, minutos, concepto, id_encabezado) values " ;
while($fila=fetch_array($rs))
{
	$conv = 1 ;
	if(($fila[entrada]=="")||($fila[salida]==""))
		$color=1;
	if((($fila[salmuerzo]!="")&&($fila[ealmuerzo]==""))||(($fila[salmuerzo]=="")&&($fila[ealmuerzo]!="")))
		$color=1;
	$entrada=$fila[entrada];
	$tolerancia_entrada=$fila[tolerancia_entrada];
	$inicio_descanso=$fila[inicio_descanso];
 	$salida_descanso=$fila[salida_descanso];
 	$tolerancia_descanso=$fila[tolerancia_descanso];
 	$salida=$fila[salida];
 	$tolerancia_salida=$fila[tolerancia_salida];
 	
 	$ordinaria=$fila[ordinaria];
 	$extra=$fila[extra];
 	$extraext=$fila[extraext];
 	$extranoc=$fila[extranoc];
 	$extraextnoc=$fila[extraextnoc];
 	$dom=$fila[domingo];
 	$fecha = $fila[fecha];
 	$tardanza = $fila[tardanza];
 	$nacional = $fila[nacional];
 	$extranac = $fila[extranac];
 	$extranocnac = $fila[extranocnac];
 	$descextra1 = $fila[descextra1];

 	$dialibre=$fila[dialibre];

 	$mixtodiurna = $fila[mixtodiurna];
    $mixtonoc = $fila[mixtonoc];
    $mixtoextdiurna = $fila[mixtoextdiurna];
    $mixtoextnoc = $fila[mixtoextnoc];
    $dialibre = $fila[dialibre];
    $emergencia = $fila[emergencia];
    $descansoincompleto = $fila[descansoincompleto];

 	if(($dom!="00:00")&&($dom!=""))
	{
		$min=explode(":",$dom);
		$minutos=($min[0]*60)+$min[1];
		if(($minutos>=1)&&($minutos!=''))
			$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','domingos', '".$fila[id_encabezado]."'),";
		//$guardar = query($insert,$conexion);
		
		if(($extra!="00:00")&&($extra!=""))
		{
			$min=explode(":",$extra);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasdom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranoc!="00:00")&&($extranoc!=""))
		{
			$min=explode(":",$extranoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasnocdom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraext!="00:00")&&($extraext!=""))
		{
			$min=explode(":",$extraext);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextdom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraextnoc!="00:00")&&($extraextnoc!=""))
		{
			$min=explode(":",$extraextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextnocdom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($tardanza!="00:00")&&($tardanza!=""))
		{
			$min=explode(":",$tardanza);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','tardanza', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtodiurna!="00:00")&&($mixtodiurna!=""))
		{
			$min=explode(":",$mixtodiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtodiurnadom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextdiurna!="00:00")&&($mixtoextdiurna!=""))
		{
			$min=explode(":",$mixtoextdiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextdiurnadom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtonoc!="00:00")&&($mixtonoc!=""))
		{
			$min=explode(":",$mixtonoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtonocdom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextnoc!="00:00")&&($mixtoextnoc!=""))
		{
			$min=explode(":",$mixtoextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextnocdom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($dialibre!="00:00")&&($dialibre!=""))
		{
			$min=explode(":",$dialibre);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','dialibredom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($emergencia!="00:00")&&($emergencia!=""))
		{
			$min=explode(":",$emergencia);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','emergenciadom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($descansoincompleto!="00:00")&&($descansoincompleto!=""))
		{
			$min=explode(":",$descansoincompleto);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','descansoincompletodom', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
	}
	elseif(($nacional!="00:00")&&($nacional!=""))
	{
		if(($nacional!="00:00")&&($nacional!=""))
		{
			$min=explode(":",$ordinaria);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','nacional', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);
		}
		
		if(($extra!="00:00")&&($extra!=""))
		{
			$min=explode(":",$extra);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraext!="00:00")&&($extraext!=""))
		{
			$min=explode(":",$extraext);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranoc!="00:00")&&($extranoc!=""))
		{
			$min=explode(":",$extranoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasnocnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraextnoc!="00:00")&&($extraextnoc!=""))
		{
			$min=explode(":",$extraextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextnocnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($tardanza!="00:00")&&($tardanza!=""))
		{
			$min=explode(":",$tardanza);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','tardanza', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($nacional!="00:00")&&($nacional!=""))
		{
			$min=explode(":",$nacional);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','nacionalnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranac!="00:00")&&($extranac!=""))
		{
			$min=explode(":",$extranac);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extranac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranocnac!="00:00")&&($extranocnac!=""))
		{
			$min=explode(":",$extranocnac);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','extranocnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($descextra1!="00:00")&&($descextra1!=""))
		{
			$min=explode(":",$descextra1);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','descextra1', '".$fila[id_encabezado]."'),";
			
		}
		if(($mixtodiurna!="00:00")&&($mixtodiurna!=""))
		{
			$min=explode(":",$mixtodiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtodiurnanac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextdiurna!="00:00")&&($mixtoextdiurna!=""))
		{
			$min=explode(":",$mixtoextdiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextdiurnanac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtonoc!="00:00")&&($mixtonoc!=""))
		{
			$min=explode(":",$mixtonoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtonocnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextnoc!="00:00")&&($mixtoextnoc!=""))
		{
			$min=explode(":",$mixtoextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextnocnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($dialibre!="00:00")&&($dialibre!=""))
		{
			$min=explode(":",$dialibre);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','dialibrenac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($emergencia!="00:00")&&($emergencia!=""))
		{
			$min=explode(":",$emergencia);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','emergencianac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($descansoincompleto!="00:00")&&($descansoincompleto!=""))
		{
			$min=explode(":",$descansoincompleto);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','descansoincompletonac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
	}
	elseif(($dialibre!="00:00")&&($dialibre!=""))
	{
		if(($dialibre!="00:00")&&($dialibre!=""))
		{
			$min=explode(":",$dialibre);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','dialibre', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);
		}
		
		if(($extra!="00:00")&&($extra!=""))
		{
			$min=explode(":",$extra);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasdl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraext!="00:00")&&($extraext!=""))
		{
			$min=explode(":",$extraext);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextdl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranoc!="00:00")&&($extranoc!=""))
		{
			$min=explode(":",$extranoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasnocdl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraextnoc!="00:00")&&($extraextnoc!=""))
		{
			$min=explode(":",$extraextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextnocdl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($tardanza!="00:00")&&($tardanza!=""))
		{
			$min=explode(":",$tardanza);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','tardanza', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		
		if(($descextra1!="00:00")&&($descextra1!=""))
		{
			$min=explode(":",$descextra1);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','descextra1', '".$fila[id_encabezado]."'),";
			
		}
		if(($mixtodiurna!="00:00")&&($mixtodiurna!=""))
		{
			$min=explode(":",$mixtodiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtodiurnadl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextdiurna!="00:00")&&($mixtoextdiurna!=""))
		{
			$min=explode(":",$mixtoextdiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextdiurnadl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtonoc!="00:00")&&($mixtonoc!=""))
		{
			$min=explode(":",$mixtonoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtonocdl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextnoc!="00:00")&&($mixtoextnoc!=""))
		{
			$min=explode(":",$mixtoextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextnocdl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($emergencia!="00:00")&&($emergencia!=""))
		{
			$min=explode(":",$emergencia);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','emergenciadl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($descansoincompleto!="00:00")&&($descansoincompleto!=""))
		{
			$min=explode(":",$descansoincompleto);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','descansoincompletodl', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
	}
	else
	{
		if(($ordinaria!="00:00")&&($ordinaria!=""))
		{
			$min=explode(":",$ordinaria);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','ordinarias', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);
		}
		
		if(($extra!="00:00")&&($extra!=""))
		{
			$min=explode(":",$extra);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extras', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraext!="00:00")&&($extraext!=""))
		{
			$min=explode(":",$extraext);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasext', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranoc!="00:00")&&($extranoc!=""))
		{
			$min=explode(":",$extranoc);
			$minutos=($min[0]*60)+$min[1];
			$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasnoc', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extraextnoc!="00:00")&&($extraextnoc!=""))
		{
			$min=explode(":",$extraextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extrasextnoc', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($tardanza!="00:00")&&($tardanza!=""))
		{
			$min=explode(":",$tardanza);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','tardanza', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($nacional!="00:00")&&($nacional!=""))
		{
			$min=explode(":",$nacional);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','nacional', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranac!="00:00")&&($extranac!=""))
		{
			$min=explode(":",$extranac);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','extranac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($extranocnac!="00:00")&&($extranocnac!=""))
		{
			$min=explode(":",$extranocnac);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','extranocnac', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($descextra1!="00:00")&&($descextra1!=""))
		{
			$min=explode(":",$descextra1);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .=" ('".$fila[ficha]."','".$fecha."','".$minutos."','descextra1', '".$fila[id_encabezado]."'),";
			
		}
		if(($mixtodiurna!="00:00")&&($mixtodiurna!=""))
		{
			$min=explode(":",$mixtodiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtodiurna', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextdiurna!="00:00")&&($mixtoextdiurna!=""))
		{
			$min=explode(":",$mixtoextdiurna);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextdiurna', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtonoc!="00:00")&&($mixtonoc!=""))
		{
			$min=explode(":",$mixtonoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtonoc', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($mixtoextnoc!="00:00")&&($mixtoextnoc!=""))
		{
			$min=explode(":",$mixtoextnoc);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','mixtoextnoc', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($dialibre!="00:00")&&($dialibre!=""))
		{
			$min=explode(":",$dialibre);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','dialibre', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($emergencia!="00:00")&&($emergencia!=""))
		{
			$min=explode(":",$emergencia);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','emergencia', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
		if(($descansoincompleto!="00:00")&&($descansoincompleto!=""))
		{
			$min=explode(":",$descansoincompleto);
			$minutos=($min[0]*60)+$min[1];
			if(($minutos>=1)&&($minutos!=''))
				$insert .="('".$fila[ficha]."','".$fecha."','".$minutos."','descansoincompleto', '".$fila[id_encabezado]."'),";
			//$guardar = query($insert,$conexion);	
		}
	}
	$i++;

	$ordinaria="";
 	$extra="";
 	$extraext="";
 	$extranoc="";
 	$extraextnoc="";
 	$dom="";
 	$fecha = "";
 	$tardanza = "";
 	$nacional = "";
 	$extranac = "";
 	$extranocnac = "";
 	$descextra1 = "";
}

$insert .= '****';
$insert = str_replace(',****', ';', $insert);

if($conv == 1) 
	$guardar = query($insert,$conexion);


$datos="SELECT turno_id FROM nomturnos WHERE tipo=6;";
$rsLibre = query($datos,$conexion);
$filaLibre = fetch_array($rsLibre);
$turnoLibre = $filaLibre[turno_id];
//if((num_rows($rs)%2)==0)	

$datos="SELECT
 reloj_detalle.ficha,
 reloj_encabezado.cod_enca,
 reloj_encabezado.fecha_reg,
 reloj_encabezado.fecha_ini,
 reloj_encabezado.fecha_fin,
 reloj_detalle.id_encabezado,
 reloj_detalle.fecha
FROM
 nomturnos INNER JOIN nompersonal ON nomturnos.turno_id = nompersonal.turno_id
 INNER JOIN reloj_detalle ON nompersonal.ficha = reloj_detalle.ficha
 INNER JOIN reloj_encabezado ON reloj_detalle.id_encabezado = reloj_encabezado.cod_enca
where reloj_encabezado.cod_enca='$reg' group by reloj_detalle.ficha order by ficha, fecha";
$rs = query($datos,$conexion);
//if((num_rows($rs)%2)==0)
//{
$i = 1;
$fichaaux = ''; 
$color = 0;
$conv1 = 0 ;
$insert="insert into reloj_procesar (ficha, fecha, minutos, concepto, id_encabezado) values ";
while($fila=fetch_array($rs))
{

	//$conv1 = 1 ;
	$fecha_ini = $fila[fecha_ini];
 	$fecha_fin = $fila[fecha_fin];
 	$fecha = $fila[fecha];

	$consulta ="select ifnull(count(fecha),0) as cantidad from nomcalendarios_personal where turno_id<>'$turnoLibre' and fecha not in ( SELECT reloj_detalle.fecha FROM reloj_detalle INNER JOIN reloj_encabezado ON reloj_detalle.id_encabezado = reloj_encabezado.cod_enca  WHERE reloj_encabezado.cod_enca='$reg' AND reloj_detalle.ficha='".$fila[ficha]."' ) and fecha between '$fecha_ini' and '$fecha_fin' AND ficha='".$fila[ficha]."'";
	//exit;
 	$resultadox = query($consulta,$conexion);
 	$fetch = fetch_array($resultadox);
 	$cantidad = $fetch[cantidad];
 	if($cantidad>=1)
 	{
 		$insert .=" ('".$fila[ficha]."','".$fecha_fin."','".$cantidad."','inasistencia', '".$fila[id_encabezado]."'),";
		//$guardar = query($insert,$conexion);
 	}
}
$insert .= '****';
$insert = str_replace(',****', ';', $insert);


if($conv1 == 0)
	$guardar = query($insert,$conexion);

if($color==0)
	header("Location:control_acceso2.php?listo=3");
elseif($color==1)
	header("Location:control_acceso2.php?listo=2");
?>