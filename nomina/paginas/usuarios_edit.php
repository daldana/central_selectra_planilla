<?php 
session_start();
ob_start();

require_once '../lib/common.php';
$conexion=conexion();

$url="usuarios_list";
$modulo="Usuarios";
$tabla=SELECTRA_CONF_PYME.".nomusuarios";
$titulos=array("Nombre", "Nombre de Usuario");
$atributos=array("descrip", "login_usuario");
//$indices=array("1","35"); // 1-descrip, 35-login_usuario

if(isset($_POST['aceptar']))
{
	$consulta  = "SELECT * FROM ".$tabla;
	$resultado = query($consulta, $conexion);

	//CREAR TABLA temporal
	$sql = "CREATE TEMPORARY TABLE tbtmp (
		    `cod_modulo` int(10)
	        );";
	$resultado = query($sql, $conexion);

	$items="";
	foreach($_POST as $key => $campo )
	{
		if((int)substr($key,1)!=0)
		{
			$items .= (int)substr($key,1).",";
			$sql = "insert into tbtmp values(".((int)substr($key,1)).")";
			$res = query($sql, $conexion);
		}
	}
	$items = substr($items,0,strlen($items)-1);

	$consulta = "UPDATE ".$tabla." SET 
				acce_configuracion=0, 
				acce_elegibles=0, 
				acce_personal=0, 
				acce_prestamos=0,
				acce_consultas=0,
				acce_transacciones=0, 
				acce_procesos=0, 
				acce_reportes=0, 
				acce_usuarios=0, 
				acce_autorizar_nom=0, 
				acce_enviar_nom=0, 
				acce_estuaca=0, 
				acce_xestuaca=0, 
				acce_permisos=0, 
				acce_logros=0,  
				acce_penalizacion=0, 
				acce_movpe=0, 
				acce_evalde=0,
				acce_experiencia=0,
				acce_antic=0, 
				acce_uniforme=0,
				acceso_sueldo=".((isset($_POST['acceso_sueldo']))?1:0)." 
				WHERE coduser='".$_POST["codigo"]."'";
	$resultado = query($consulta, $conexion);

	function make_thumb($src, $dest, $desired_width) {
      /* read the source image */
      $source_image = imagecreatefromjpeg($src);
      $width = imagesx($source_image);
      $height = imagesy($source_image);

      /* find the “desired height” of this thumbnail, relative to the desired width  */
      $desired_height = floor($height * ($desired_width / $width));

      /* create a new, “virtual” image */
      $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

      /* copy source image at a resized size */
      imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

      /* create the physical thumbnail image to its destination */
      imagejpeg($virtual_image, $dest);
    }
    $imgfile = "";

    if($_FILES['img']['name'] != "")
    {
        $uploaddir  = '../img_sis/avatars/';
        $fileName   = $_POST["login_usuario"]."_".basename($_FILES['img']['name']);
        $fileName   = str_replace(' ', '', $fileName);
        $uploadfile = $uploaddir . $fileName;

        if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) 
        {
        	$imgfile = " img='{$fileName}', "; 
            make_thumb($uploadfile, $uploaddir."thumbs/".$fileName, 40);
        }
    }

	//eliminar items que se dejaron de seleccionar 
	if($items!="")
	{	
		//verificamos el alias_opcion para asignarle 1 de haber seleccionado un checkbox
		$sql = "select alias_opcion from nom_modulos where cod_modulo in (".$items.") and alias_opcion <> ''";
		$resultado = query($sql, $conexion);
		$campos = "";
		while($campo=mysqli_fetch_assoc($resultado))
		{
			$campos .= $campo["alias_opcion"]."=1,";
		}	
		$campos = substr($campos,0,strlen($campos)-1);

		if($campos!="")
		{
			$consulta="
				UPDATE ".$tabla." SET 
				 ".$campos."
				where 
				coduser='".$_POST["codigo"]."'";
			$resultado = query($consulta, $conexion);
		}

		$consulta = "UPDATE ".$tabla." SET 
					 descrip='{$_POST['descrip']}', 
					 ".$imgfile." 
					 login_usuario='{$_POST['login_usuario']}',
	        		 correo='{$_POST['correo']}'
					 WHERE coduser='".$_POST['codigo']."'";
		$resultado = query($consulta, $conexion);

        if(trim($_POST['clave'])!="")
        {
            	$consulta2="UPDATE ".$tabla." SET 
				clave='".hash("sha256",$_POST['clave'])."' 
				where coduser='".$_POST["codigo"]."'";
        	$resultado=query($consulta2,$conexion);
        }
	}

	$sql = "delete from ".SELECTRA_CONF_PYME.".nom_modulos_usuario where coduser='".$_POST["codigo"]."'";
    $resultado= query($sql,$conexion);
	$consulta="SELECT cod_modulo, nom_menu 
                        FROM nom_modulos
                        WHERE 1
                        ORDER BY cod_modulo ";
    $resultado2=query($consulta,$conexion);
    //$count=mysqli_num_rows($resultado);
    
    while($campoPadre=mysqli_fetch_assoc($resultado2)){
        if(isset($_POST['c'.$campoPadre['cod_modulo']]) && $_POST['c'.$campoPadre['cod_modulo']]!=""){
            $sql = "insert into ".SELECTRA_CONF_PYME.".nom_modulos_usuario (coduser, cod_modulo) values ('".$_POST["codigo"]."','".$campoPadre['cod_modulo']."')";
            //echo $sql."<br>";
            $resultado= query($sql,$conexion);
        }
        
    }
    $sql = "delete from ".SELECTRA_CONF_PYME.".nomusuario_empresa where id_usuario='".$_POST["codigo"]."'";
    $resultado= query($sql,$conexion);
    $consulta="SELECT codigo, nombre 
                        FROM ".SELECTRA_CONF_PYME.".nomempresa 
                        WHERE 1
                        ORDER BY codigo ";
    $resultado2=query($consulta,$conexion);
    $count=mysqli_num_rows($resultado);
    
    while($campoPadre=mysqli_fetch_assoc($resultado2)){
        if(isset($_POST['e'.$campoPadre['codigo']]) && $_POST['e'.$campoPadre['codigo']]!=""){
            $acceso = 1;
        }
        else{
            $acceso = 0;
        }
        $sql = "insert into ".SELECTRA_CONF_PYME.".nomusuario_empresa (id_usuario,id_empresa,acceso) values ('".$_POST["codigo"]."','".$campoPadre['codigo']."','".$acceso."')";
        //echo $sql."<br>";
        $resultado= query($sql,$conexion);
    }


	$sql = "delete from ".SELECTRA_CONF_PYME.".nomusuario_nomina where id_usuario='".$_POST["codigo"]."'";
    $resultado= query($sql,$conexion);
    $consulta="SELECT codtip, descrip 
                        FROM nomtipos_nomina
                        WHERE 1
                        ORDER BY codtip ";
    $resultado2=query($consulta,$conexion);
    $count=mysqli_num_rows($resultado);
    while($campoPadre=mysqli_fetch_assoc($resultado2)){
        if(isset($_POST['n'.$campoPadre['codtip']]) && $_POST['n'.$campoPadre['codtip']]!=""){
            $acceso = 1;
        }
        else{
            $acceso = 0;
        }
        $sql = "insert into ".SELECTRA_CONF_PYME.".nomusuario_nomina (id_usuario,id_nomina,acceso) values ('".$_POST["codigo"]."','".$campoPadre['codtip']."','".$acceso."')";
        //echo $sql."<br>";
        $resultado= query($sql,$conexion);
    }


	
	if($items==""):
	$consulta="delete from ".SELECTRA_CONF_PYME.".nom_modulos_usuario  where coduser = ".$_POST["codigo"];$resultado=query($consulta,$conexion);		
	endif;

    //=========================================================================================================
    // Acceso Vacaciones
    // Consultar primero si el modulo de vacaciones está activo
    $sql = "SELECT cod_modulo, cod_modulo_padre FROM nom_modulos WHERE nom_menu='Vacaciones' OR nom_menu='Planilla'";   
    $obj =  mysqli_fetch_object(query($sql, $conexion));
    $cod_mod_reporte = $obj->cod_modulo;

    if(isset($_POST['c'.$cod_mod_reporte]))
    {
        // Eliminar permisos anteriores de este usuario
        $sql = "DELETE FROM ".SELECTRA_CONF_PYME.".nom_paginas_usuario
                WHERE coduser='{$_POST["codigo"]}' 
                AND   id_pagina IN (SELECT id_pagina FROM nom_paginas WHERE cod_modulo='217' OR cod_modulo='45')";
        query($sql, $conexion);
        
        foreach ($_POST as $campo => $valor) 
        {
            if(strpos($campo, "pagina_") !== FALSE)
            {
                $segments  = explode('_', $campo);
                $id_pagina = $segments[1];

                $sql = "INSERT INTO ".SELECTRA_CONF_PYME.".`nom_paginas_usuario` (`coduser`, `id_pagina`) 
                        VALUES ('{$_POST["codigo"]}', '{$id_pagina}')";
                query($sql, $conexion);
            }
        }
    }    
    //=========================================================================================================
	//Eliminar database
	$sql="drop table tbtmp";
	$resultado= query($sql,$conexion);
	cerrar_conexion($conexion);
	header("Location: ".$url.".php?pagina=1");
	exit;
	echo "<SCRIPT language=\"JavaScript\" type=\"text/javascript\">
	document.location.href=\"".$url.".php?pagina=1\"
	</SCRIPT>";
	

}
	if(isset($_GET['codigo'])){$codigo = $_GET['codigo'];}
	if(isset($_POST['codigo'])){$codigo = $_POST['codigo'];}		

	
	
	$consulta="select * from ".$tabla." where coduser='".$codigo."'";

	$resultado=query($consulta,$conexion);
	$fila=fetch_array($resultado);

?>

<!doctype html>
<html lang="en" class="fondo">
<head>

<link href="../estilos.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="../../includes/css/jquery-ui.css">
<script src="../../includes/css/jquery-1.10.2.js"></script>
<script src="../../includes/css/jquery-ui.js"></script>

  <link href="../estilos.css" rel="stylesheet" type="text/css">
  <SCRIPT language="JavaScript" type="text/javascript" src="../lib/common.js">
  </SCRIPT>
  <!--SCRIPT language="JavaScript" type="text/javascript" src="../lib/jquery.js">
  </SCRIPT-->
<SCRIPT language="JavaScript" type="text/javascript">

function cerrar(retorno){
	document.location.href=retorno+".php?pagina=1"
}

function contra(){
		
		if(this.document.forms.sampleform.clave.value==this.document.forms.sampleform.comprobar.value){
				
				this.document.forms.sampleform.aceptar.focus();
				
		
			}
		else{
				alert("Las contraseñas introducidas no coinciden");
				this.document.forms.sampleform.clave.focus();
				this.document.forms.sampleform.clave.value="";
				this.document.forms.sampleform.comprobar.value="";
		
		}

}

$(function() {
    $( "#tabs" ).tabs();
  });
</SCRIPT>
<style>
.control{
    background: url("../imagenes/group-expand-sprite.gif") no-repeat scroll 3px -3px transparent;
    float: left;
    margin-top: 4px;
    padding: 6px 4px 4px 10px;
}

.controlShow {
    background: url("../imagenes/group-expand-sprite.gif") no-repeat scroll 3px -53px transparent;
    float: left;line-style-type:none;
    margin-top: 4px;
    padding: 6px 4px 4px 10px;
}

.lcontrol {
cursor: pointer;
}

ul li{
display: block;
line-style-type:none;
}

</style>

<!--[if IE ]>
<style>
.control{
    background: url("../imagenes/group-expand-sprite.gif") no-repeat scroll 3px -3px transparent;
    float: left;
    margin-top: 4px;
    padding: 0px;
    padding-left:25px;
}

.controlShow {
    background: url("../imagenes/group-expand-sprite.gif") no-repeat scroll 3px -53px transparent;
    float: left;
    line-style-type:none;
    margin-top: 4px;
    padding-left:25px;
}


ul li{
display: block;
line-style-type:none;
}

</style>
<![endif]-->

</head>
<body>
<?php

function getCountItemByCodItem($codigo,$conexion){
 $consulta="
    SELECT cod_modulo, nom_menu
    FROM `nom_modulos`
    WHERE cod_modulo_padre  = ".$codigo."
    AND activo =1
    ORDER BY orden";
    $r=query($consulta,$conexion);
    $count=mysqli_num_rows($r);
return $count;
}

function verifyIfExistChecked($codigo,$conexion){
 $consulta="
    SELECT cod_modulo
    FROM ".SELECTRA_CONF_PYME.".nom_modulos_usuario 
    WHERE cod_modulo  = ".$codigo." and coduser=".$_GET['codigo'];
    $r=query($consulta,$conexion);
    $count=mysqli_num_rows($r);
return $count;
}

function verifyIfExistCheckedPlanilla($codigo,$conexion){
 $consulta="
    SELECT usuario_nomina_id
    FROM ".SELECTRA_CONF_PYME.".nomusuario_nomina 
    WHERE id_nomina  = ".$codigo." and id_usuario=".$_GET['codigo']." and acceso=1 ";
    $r=query($consulta,$conexion);
    $count=mysqli_num_rows($r);
return $count;
}
function verifyIfExistCheckedEmpresa($codigo,$conexion){
 $consulta="
    SELECT usuario_empresa_id
    FROM ".SELECTRA_CONF_PYME.".nomusuario_empresa 
    WHERE id_empresa  = ".$codigo." and id_usuario=".$_GET['codigo']." and acceso=1 ";
    $r=query($consulta,$conexion);
    $count=mysqli_num_rows($r);
return $count;
}

function verifyIfExistCheckedVacacion($id_pagina, $conexion)
{
	$sql = "SELECT * FROM ".SELECTRA_CONF_PYME.".nom_paginas_usuario 
	        WHERE coduser='{$_GET['codigo']}' AND id_pagina='{$id_pagina}'";
	$res = query($sql, $conexion);
	return mysqli_num_rows($res);
}

function verifyIfExistCheckedReporte($id_pagina, $conexion)
{
    $sql = "SELECT * FROM ".SELECTRA_CONF_PYME.".nom_paginas_usuario 
            WHERE coduser='{$_GET['codigo']}' AND id_pagina='{$id_pagina}'";
    $res = query($sql, $conexion);
    return mysqli_num_rows($res);
}

function childrenItem($codigo,$conexion){
    $consulta="
    SELECT cod_modulo, nom_menu
    FROM `nom_modulos`
    WHERE cod_modulo_padre  = ".$codigo."
    AND activo =1
    ORDER BY orden";
    $resultado2=query($consulta,$conexion);
    $stringMenu .= "<ul class=\"".$codigo."\">";
        while($padreItem=mysqli_fetch_assoc($resultado2)){
            $stringMenu .= "
                <li>";
	    if(getCountItemByCodItem($padreItem["cod_modulo"],$conexion)>0){$stringMenu .= "<div class=\"lcontrol control\"></div>";}
	    else{$stringMenu .= "<div style=\"float: left;padding: 6px 4px 4px 10px;\"></div>";}
            $stringMenu .= "
                    <label for=\"c".$padreItem["cod_modulo"]."\">
                        <input type=\"checkbox\" value=\"1\" ".((verifyIfExistChecked($padreItem["cod_modulo"],$conexion)==1)?"checked":"")." name=\"c".$padreItem["cod_modulo"]."\" id=\"c".$padreItem["cod_modulo"]."\"/>
                        ".utf8_encode($padreItem["nom_menu"])."
                    </label>
  	 	    <div class=\"".$padreItem["cod_modulo"]."\" style=\"display:none;\">
                    ".childrenItem($padreItem["cod_modulo"],$conexion)."
		    </div>
                </li>
          ";
        }
    $stringMenu .= "</ul>";
    
    return $stringMenu;
}
?>
  
<FORM name="sampleform" method="POST" target="_self" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">

<TABLE  width="100%" height="100">
<TBODY>

<tr>
<td colspan="2" height="30" class="tb-tit"><strong>Editar un Registro de <?php echo $modulo; ?></strong></td>
<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
</tr>

<?php
$i=0;
foreach($titulos as $nombre)
{
	$campo = $atributos[$i]; //mysqli_fetch_field_direct($resultado, $indices[$i])->name;
?>
	<tr>
  		<td class=tb-head ><?php echo $nombre; ?></td>
		<td><INPUT type="text" name="<?php echo $campo; ?>" size="100" value="<?php echo $fila["$campo"]; ?>"></td>
	</tr>
<?php
	$i++;	
}
?>
	<TR><td class=tb-head >Contrase&#241;a</td>
		<td colspan="3"><INPUT type="password" name="clave" size="100"></td>
		
	</tr>
		<TR><td class=tb-head >Confirmar Contrase&#241;a</td>
		<td colspan="3"><INPUT type="password" name="comprobar" size="100" onblur="contra()"></td> </tr>
        <tr>
        	<td class=tb-head >Correo Electr&oacute;nico</td>
			<td colspan="3">
				<INPUT type="text" value="<?php echo $fila['correo']; ?>" name="correo" size="100">
			</td>
		</tr>
		<tr>
        	<td class=tb-head >Acceso a Sueldo</td>
			<td colspan="3">
				<input type="checkbox" <?php echo(($fila['acceso_sueldo']==1) ? "checked" : ""); ?> value="<?php echo $fila['acceso_sueldo']; ?>" name="acceso_sueldo">
			</td>
		</tr>
        <tr>
        	<td class=tb-head >Foto</td>
			<td colspan="3">
				<input type="hidden" name="img_actual" value="<?php echo $fila['img']; ?>">
				<input type="file" name="img" size="100">
			</td>
		</tr>
    </table>
	
	<table  width="100%" >
	    	<tr><td colspan="7" height="30" class="tb-tit"><!--strong>Accesos al Sistema: </strong></td></tr>
		<tr align="center"> <td><strong>Administrador</strong>
		<input type="checkbox"  name="ADMIN" value="1"  size="100"-->
		</TD>
		</tr>	
	    
</table>


<?php
$consulta="
SELECT cod_modulo, nom_menu
FROM `nom_modulos`
WHERE cod_modulo_padre IS NULL
AND activo =1
ORDER BY orden";
$resultado=query($consulta,$conexion);
	$count=mysqli_num_rows($resultado);
?>
<div>  
<div id="tabs">
              <ul>
                <li><a href="#tabs-1"><strong>Accesos al Sistema: </strong></a></li>
                <li><a href="#tabs-2"><strong>Accesos a Empresas: </strong></a></li>
                <li><a href="#tabs-3"><strong>Accesos a Planillas: </strong></a></li>
                <li><a href="#tabs-4"><strong>Accesos a Vacaciones:</strong></a></li>
                <li><a href="#tabs-5"><strong>Accesos a Reportes:</strong></a></li>
              </ul>
              <div id="tabs-1">
                    <?php
                        $consulta="
                        SELECT cod_modulo, nom_menu
                        FROM `nom_modulos`
                        WHERE cod_modulo_padre IS NULL
                        AND activo =1
                        ORDER BY orden";
                        $resultado=query($consulta,$conexion);
                        $count=mysqli_num_rows($resultado);
                        ?>
                        <ul>
                            <li>
                                <strong>Administrador</strong>
                                <INPUT type="checkbox"  name="ADMIN" value="1"  size="100">
                            </li>
                        </ul>
                        <ul id="panel_opciones">
						    <?php while($campoPadre=mysqli_fetch_assoc($resultado)): ?>
						        <li>
							    <?php if(getCountItemByCodItem($campoPadre["cod_modulo"],$conexion)>0): ?>
								<div class="lcontrol controlShow"></div>
							    <?php else: ?>
								<div style="float: left;padding-left: 25px;"></div>
							    <?php endif;?>
						            <label for="c<?php echo $campoPadre["cod_modulo"]?>">
								
						                <input type="checkbox" value="1" <?php echo (verifyIfExistChecked($campoPadre["cod_modulo"],$conexion)==1)?"checked":"" ?> name="c<?php echo $campoPadre["cod_modulo"]?>" id="c<?php echo $campoPadre["cod_modulo"]?>"/>
						                <?php echo  utf8_encode($campoPadre["nom_menu"]) ?>
						            </label>
						 	    <div class="<?php echo  $campoPadre["cod_modulo"] ?> cpanel" style="display:'';">
						            <?php echo  childrenItem($campoPadre["cod_modulo"],$conexion) ?>
							    </div>
						        </li>
						    <?php endwhile ?>
						</ul>
              </div>
              <div id="tabs-2">
                <?php
                        $consulta="
                        SELECT codigo, nombre 
                        FROM ".SELECTRA_CONF_PYME.".nomempresa 
                        WHERE 1
                        ORDER BY nombre ";
                        $resultado2=query($consulta,$conexion);
                        $count=mysqli_num_rows($resultado);
                        ?>
                        <div>    
                        <ul id="panel_opciones2">
                            <?php while($campoPadre=mysqli_fetch_assoc($resultado2)): ?>
                                <li>
                                        <label for="e<?php echo $campoPadre["codigo"]; ?>">                                    
                                            <input type="checkbox" value="1" <?php echo  (verifyIfExistCheckedEmpresa($campoPadre["codigo"],$conexion)==1)?"checked":"" ?> name="e<?php echo $campoPadre["codigo"]?>" id="e<?php echo $campoPadre["codigo"]?>"/>
                                            <?php echo  utf8_encode($campoPadre["nombre"]) ?>
                                        </label>
                                </li>
                            <?php endwhile ?>
                        </ul>
                        </div>
              </div>
              <div id="tabs-3">
                <?php
                        $consulta="
                        SELECT codtip, descrip 
                        FROM nomtipos_nomina
                        WHERE 1
                        ORDER BY descrip ";
                        $resultado2=query($consulta,$conexion);
                        $count=mysqli_num_rows($resultado);
                        ?>
                        <div>    
                        <ul id="panel_opciones3">
                            <?php while($campoPadre=mysqli_fetch_assoc($resultado2)): ?>
                                <li>
                                        <label for="n<?php echo $campoPadre["codtip"]?>">                                    
                                            <input type="checkbox" value="1" <?php echo  (verifyIfExistCheckedPlanilla($campoPadre["codtip"],$conexion)==1)?"checked":"" ?> name="n<?php echo $campoPadre["codtip"]?>" id="n<?php echo $campoPadre["codtip"]?>"/>
                                            <?php echo  utf8_encode($campoPadre["descrip"]) ?>
                                        </label>
                                </li>
                            <?php endwhile ?>
                        </ul>
                        </div>
              </div>
              <div id="tabs-4">
                <?php
                    $consulta = "SELECT id_pagina, descripcion 
                                 FROM   nom_paginas 
                                 WHERE  cod_modulo=(SELECT cod_modulo FROM nom_modulos WHERE nom_menu='Vacaciones')";

                    $res = query($consulta, $conexion);
                ?>
                <div>
                    <ul id="panel_opciones4">
                        <?php
                            while($fila=mysqli_fetch_assoc($res))
                            {
                            ?>
                                <li>
                                    <label for="pagina_<?php echo $fila['id_pagina']; ?>">
                                        <input type="checkbox" name="pagina_<?php echo $fila['id_pagina']; ?>" id="pagina_<?php echo $fila['id_pagina']; ?>" value="<?php echo $fila['id_pagina']; ?>" <?php echo(verifyIfExistCheckedVacacion($fila['id_pagina'], $conexion)==1) ? "checked" : ""; ?> >
                                        <?php echo utf8_encode($fila['descripcion']); ?>
                                    </label>
                                </li>
                            <?php
                            }
                        ?>
                    </ul>
                </div>
              </div>
              <div id="tabs-5">
                <?php
                    $consulta = "SELECT id_pagina, descripcion 
                                 FROM   nom_paginas 
                                 WHERE  cod_modulo=(SELECT cod_modulo FROM nom_modulos WHERE nom_menu='Planilla')";

                    $res = query($consulta, $conexion);
                ?>
                <div>
                    <ul id="panel_opciones5">
                        <?php
                            while($fila=mysqli_fetch_assoc($res))
                            {
                            ?>
                                <li>
                                    <label for="pagina_<?php echo $fila['id_pagina']; ?>">
                                        <input type="checkbox" name="pagina_<?php echo $fila['id_pagina']; ?>" id="pagina_<?php echo $fila['id_pagina']; ?>" value="<?php echo $fila['id_pagina']; ?>" <?php echo(verifyIfExistCheckedReporte($fila['id_pagina'], $conexion)==1) ? "checked" : ""; ?> >
                                        <?php echo utf8_encode($fila['descripcion']); ?>
                                    </label>
                                </li>
                            <?php
                            }
                        ?>
                    </ul>
                </div>
              </div>
            </div>


<div> 
	<table width="100%" border="0">
	<tr class="tb-tit">
      <td></td>
      <td align="right"><INPUT type="submit" name="aceptar" value="Aceptar">&nbsp;<INPUT type="button" name="cancelar" value="Cancelar" onclick="javascript:cerrar('<?php echo $url?>');"></td>
    </tr>
  </tbody>
</table>

</FORM>
<script>
$(document).ready(function(){

$("input[type='checkbox']").click(function(){
    codItem=$(this).attr("id");
    $("input[id='"+codItem+"']").parents("li").find("ul."+codItem+" li input[type='checkbox']").attr("checked",$(this).attr("checked"));

});

$(".lcontrol").click(function(){
	codItem=$(this).parents("li").find("input[type='checkbox']").attr("id");

	if($(this).hasClass("control")){
	  $(this).removeClass("control").addClass("controlShow");
	  mostrar="";
	}else{
	  $(this).removeClass("controlShow").addClass("control");
	  mostrar="none";
	}
	$("."+codItem).css("display",mostrar);
});

$("input[name='ADMIN']").click(function(){
	$(this).attr("checked")
	$("#panel_opciones").find("input[type='checkbox']").attr("checked",$(this).attr("checked"));
});

});
</script>
</body>
</html>
<?php
cerrar_conexion($conexion);
?>
