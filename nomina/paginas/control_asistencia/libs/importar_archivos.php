<?php

function cargar_archivo($directorio, $max_size, $_files)
{	
	if ($_files["size"] <= $max_size)
	{
		if ($_files["error"] > 0)
			return array(false, "¡Error al subir el archivo! Código: " . $_files["error"], "");
		else
		{
			$archivo = $directorio . time() . '_' . basename($_files['name']);

			if (move_uploaded_file($_files['tmp_name'], $archivo)) 
			    return array(true, "", $archivo);
			else
				return array(false, "¡Error! Al mover el archivo", "");
		}
	}
	else
		return array(false, "¡Error! Límite de tamaño de archivo superado", "");
}

function obtener_extension($archivo)
{
	$partes_nombre = explode(".", $archivo);
	$extension     = end($partes_nombre);	

	return $extension;
}