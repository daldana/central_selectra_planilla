<?php 
session_start();
ob_start();
//error_reporting(0);
require_once '../lib/common.php';
include ("func_bd.php");
$conexion=conexion();
//error_reporting(0);

$id  = (isset($_GET['id'])) ? $_GET['id'] : '';

if( isset($_POST['btn-guardar']) )
{
	$descripcion      = ( isset($_POST['descripcion']) )      ? $_POST['descripcion']      : '' ;
	$sueldo_propuesto = ( isset($_POST['sueldo_propuesto']) ) ? $_POST['sueldo_propuesto'] : '' ; 
	$sueldo_anual     = ( isset($_POST['sueldo_anual']) )     ? $_POST['sueldo_anual']     : '' ;
	$partida          = ( isset($_POST['partida']) )          ? $_POST['partida']          : '' ;
	$gastos_repr      = ( isset($_POST['gastos_repr']) )      ? $_POST['gastos_repr']      : '' ;
	$paga_gr          = ( isset($_POST['paga_gr']) )          ? $_POST['paga_gr']          : '' ;
	$categoria_id     = ( isset($_POST['categoria_id']) )     ? $_POST['categoria_id']     : '' ;
	$cargo_id         = ( isset($_POST['cargo_id']) )         ? $_POST['cargo_id']         : '' ; 
    $nomposicion_id   = ( isset($_POST['id']) )               ? $_POST['id']               : '' ;
	if( empty($id) )
	{
		$sql = "INSERT INTO nomposicion (nomposicion_id, descripcion_posicion, sueldo_propuesto, sueldo_anual,
			                             partida, gastos_representacion, paga_gr,categoria_id,cargo_id) 
                VALUES ('{$nomposicion_id}','{$descripcion}','{$sueldo_propuesto}', '{$sueldo_anual}', 
                	   '{$partida}', '{$gastos_repr}', '{$paga_gr}', '{$categoria_id}', '{$cargo_id}')";

	}
	else
	{
		$sql = "UPDATE nomposicion SET 
				nomposicion_id        = '".$_POST['id']."',
				descripcion_posicion  = '{$descripcion}',
				sueldo_propuesto      = '{$sueldo_propuesto}',
				sueldo_anual          = '{$sueldo_anual}',
				partida               = '{$partida}',
				gastos_representacion = '{$gastos_repr}',
				paga_gr               = '{$paga_gr}',
				categoria_id          = '{$categoria_id}',
				cargo_id              = '{$cargo_id}'
				WHERE nomposicion_id  = " . $id;
	}
	$res = query($sql, $conexion);

	activar_pagina("maestro_posicion.php");	 
}

$descripcion = $sueldo_propuesto = $sueldo_anual = $partida = $gastos_repr = $paga_gr = $categoria_id = $cargo_id = '';

if(isset($_GET['edit']))
{
	$sql = "SELECT nomposicion_id, descripcion_posicion, sueldo_propuesto, sueldo_anual,  
	               partida, gastos_representacion, paga_gr, categoria_id, cargo_id 
	        FROM   nomposicion WHERE nomposicion_id = " . $id;

	$res = query($sql, $conexion);

	if( $fila=fetch_array($res)  )
	{
		$descripcion      = $fila['descripcion_posicion'];
		$sueldo_propuesto = $fila['sueldo_propuesto'];
		$sueldo_anual     = $fila['sueldo_anual'];
		$partida          = $fila['partida'];
		$gastos_repr      = $fila['gastos_representacion'];
		$paga_gr          = $fila['paga_gr'];
		$categoria_id     = $fila['categoria_id'];
		$cargo_id         = $fila['cargo_id'];
	}
}
?>
<?php include("../header4.php"); // <html><head></head><body> ?>
<link href="../../includes/assets/css/custom-datatables.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.portlet > .portlet-body.blue, .portlet.blue {
    background-color: #ffffff !important;
}

.portlet > .portlet-title > .caption {
    font-size: 13px;
    font-weight: bold;
    font-family: helvetica, arial, verdana, sans-serif;
    margin-bottom: 3px;
}

.form-horizontal .control-label {
    text-align: left;
    padding-top: 3px;
}

.form-control {
    /* border: 1px solid silver; */
}

.btn{
	border-radius: 3px !important;
}

.form-body{
	padding-bottom: 5px;
}
</style>
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								Posici&oacute;n
							</div>
						</div>
						<div class="portlet-body form">
							<form class="form-horizontal" id="formPrincipal" name="formPrincipal" method="post" role="form" style="margin-bottom: 5px;">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3 control-label" for="descripcion">ID:</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm" 
										       id="id" name="id" value="<?php echo $id; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="descripcion">Descripci&oacute;n:</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm" 
										       id="descripcion" name="descripcion" value="<?php echo $descripcion; ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="sueldo_propuesto">Sueldo Propuesto:</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm" 
										       id="sueldo_propuesto" name="sueldo_propuesto" value="<?php echo $sueldo_propuesto; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="sueldo_anual">Sueldo Anual:</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm" 
										       id="sueldo_anual" name="sueldo_anual" value="<?php echo $sueldo_anual; ?>" required>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="partida">Partida:</label>
										<div class="col-md-9">
										<?php 
											$query = "SELECT concat(CodCue,' - ',Denominacion) CuentaDescrip, id, CodCue Cuenta, Tipocta, 
											                 Denominacion Descrip, Tipopuc 
											          FROM   cwprecue";
											$res = query($query, $conexion);
										?>
											<select name="partida" id="partida" class="select2 form-control">
												<option value="">Seleccione una partida</option>
												<?php
												while( $fila = fetch_array($res) )
												{ 
													if( $partida == $fila['Cuenta'] )
													{ ?>
														<option value="<?php echo $fila['Cuenta']; ?>" selected><?php echo $fila['CuentaDescrip']; ?></option>
													  <?php
													}
													else
													{ ?>
														<option value="<?php echo $fila['Cuenta']; ?>"><?php echo $fila['CuentaDescrip']; ?></option>
													  <?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="gastos_repr">Gastos de representaci&oacute;n:</label>
										<div class="col-md-9">
											<input type="text" class="form-control input-sm" 
										       id="gastos_repr" name="gastos_repr" value="<?php echo $gastos_repr; ?>" >
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label" for="gastos_repr">Paga gastos representaci&oacute;n:</label>
										<div class="col-md-9">											
											<select name="paga_gr" id="paga_gr" class="select2 form-control">
												<option value="">Seleccione paga GR</option>
												<option value="0" <?php echo ($paga_gr=='0') ? 'selected' : ''; ?> >No</option>
												<option value="1" <?php echo ($paga_gr=='1') ? 'selected' : ''; ?> >Si</option>
											</select>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label" for="categoria_id">Categor&iacute;a:</label>
										<div class="col-md-9">
										<?php 
											$query = "SELECT * FROM nomcategorias";
											$res = query($query, $conexion);
										?>
											<select name="categoria_id" id="categoria_id" class="select2 form-control">
												<option value="">Seleccione una categor&iacute;a</option>
												<?php
												while( $fila = fetch_array($res) )
												{ 
													if( $categoria_id == $fila['codorg'] )
													{ ?>
														<option value="<?php echo $fila['codorg']; ?>" selected><?php echo $fila['descrip']; ?></option>
													  <?php
													}
													else
													{ ?>
														<option value="<?php echo $fila['codorg']; ?>"><?php echo $fila['descrip']; ?></option>
													  <?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3 control-label" for="cargo_id">Cargo:</label>
										<div class="col-md-9">
										<?php 
											$query = "SELECT * FROM nomcargos";
											$res   = query($query, $conexion);
										?>
											<select name="cargo_id" id="cargo_id" class="select2 form-control">
												<option value="">Seleccione un cargo</option>
												<?php
												while( $fila = fetch_array($res) )
												{ 
													if( $cargo_id == $fila['cod_car'] )
													{ ?>
														<option value="<?php echo $fila['cod_car']; ?>" selected><?php echo $fila['des_car']; ?></option>
													  <?php
													}
													else
													{ ?>
														<option value="<?php echo $fila['cod_car']; ?>"><?php echo $fila['des_car']; ?></option>
													  <?php
													}
												}
												?>
											</select>
										</div>
									</div>
									<button type="submit" class="btn btn-sm blue active" id="btn-guardar" name="btn-guardar">Guardar</button>
									<button type="button" class="btn btn-sm default active" 
									        onclick="javascript: document.location.href='maestro_posicion.php'">Cancelar</button>
									
								</div>
							</form>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<?php include("../footer4.php"); ?>
<script src="../../includes/assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$('#formPrincipal').validate({
	            rules: {
	                descripcion: {
	                    required: true
	                },
	                sueldo_propuesto: { required: true },
	                sueldo_anual: { required: true }
	            },

	            messages: {
	                descripcion: { required: " Campo requerido" },
	                sueldo_propuesto: { required: "Campo requerido "},
	                sueldo_anual: { required: " Campo requerido" }
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },
	    });

		$("#partida").select2({
		 	placeholder: 'Seleccione una partida',
            allowClear: true,
        });
/*
		$("#paga_gr").select2({
		 	placeholder: 'Seleccione paga GR',
            allowClear: true,
        });
*/
		$("#categoria_id").select2({
		 	placeholder: 'Seleccione una categor&iacute;a',
            allowClear: true,
        });

		$("#cargo_id").select2({
		 	placeholder: 'Seleccione un cargo',
            //allowClear: true,
        });

		$( "#btn-guardar" ).click(function() {
			/*
		    var descripcion = $('#descripcion').val();

		    if( descripcion == '' )
		    {
		    	alert('Debe llenar los campos obligatorios');
		    	return false;
		    }
		    */
		});
	});
</script>
</body>
</html>