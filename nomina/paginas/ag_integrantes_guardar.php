<?php
//=================================================================================================================
// Subir foto del integrante
$archivo_foto = isset($_FILES['foto']['name']) ? $_FILES['foto']['name'] : '';
$setFoto = "";

if($archivo_foto!='')
{
    $nombre_foto  = $_POST['ficha']."_".$archivo_foto;
    $dir_fotos    = "fotos/";
    if (copy($_FILES['foto']['tmp_name'], $dir_fotos . $nombre_foto)) 
    {
        chmod( $dir_fotos . $nombre_foto, 0777);
        $insertFoto = $dir_fotos . $nombre_foto;
        $setFoto    = "foto='". $dir_fotos . $nombre_foto."', ";
    } 
    else
        throw new Exception("Error al subir la foto", 1);            				
}

// Imagen cedula del integrante
$archivo_cedula  = isset($_FILES['imagen_cedula']['name']) ? $_FILES['imagen_cedula']['name'] : '';
$setImagenCedula = "";

if($archivo_cedula!='')
{
    $nombre_imagen  = $_POST['ficha']."_".$archivo_cedula;
    $dir_imagen     = "fotos/";
    if (copy($_FILES['imagen_cedula']['tmp_name'], $dir_imagen . $nombre_imagen)) 
    {
        chmod( $dir_imagen  . $nombre_imagen, 0777);
        $insertImagenCedula = $dir_imagen . $nombre_imagen;
        $setImagenCedula    = "imagen_cedula='fotos/".$nombre_imagen."', ";
    } 
    else
        throw new Exception("Error al subir la imagen de la cedula", 1);            				
}

$niveles = array('codnivel1', 'codnivel2', 'codnivel3', 'codnivel4', 'codnivel5', 'codnivel6', 'codnivel7');

foreach ($niveles as $nivel) {
	$$nivel = 0;

	if(isset($_POST[$nivel])  &&  $_POST[$nivel] != '')
	{
		$$nivel = $_POST[$nivel];
	}
}

$suesal             = (isset($_POST['suesal']) ? $_POST['suesal'] : 0);
$sueldopro          = (isset($_POST['suesal']) ? $_POST['suesal'] : 0);
$sueldo_original = (isset($_POST['sueldo_original'])) ? $_POST['sueldo_original'] : 0 ;

$fecnac = DateTime::createFromFormat('d-m-Y', $_POST['fecnac']); // 16-01-1962
$fecnac = ($fecnac !== false) ? $fecnac->format('Y-m-d') : '';	

$fecing = DateTime::createFromFormat('d-m-Y', $_POST['fecing']);
$fecing = ($fecing !== false) ? $fecing->format('Y-m-d') : '';	

$fecha_decreto = DateTime::createFromFormat('d-m-Y', $_POST['fecha_decreto']);
$fecha_decreto = ($fecha_decreto !== false) ? $fecha_decreto->format('Y-m-d') : '';	

$fecha_decreto_baja = DateTime::createFromFormat('d-m-Y', $_POST['fecha_decreto_baja']);
$fecha_decreto_baja = ($fecha_decreto_baja !== false) ? $fecha_decreto_baja->format('Y-m-d') : '';	

$inicio_periodo = (isset($_POST['inicio_periodo'])) ? $_POST['inicio_periodo']  : '';
$inicio_periodo = DateTime::createFromFormat('d-m-Y', $inicio_periodo);
$inicio_periodo = ($inicio_periodo !== false) ? $inicio_periodo->format('Y-m-d') : '';	

$fin_periodo = (isset($_POST['fin_periodo'])) ? $_POST['fin_periodo']  : '';
$fin_periodo = DateTime::createFromFormat('d-m-Y', $fin_periodo);
$fin_periodo = ($fin_periodo !== false) ? $fin_periodo->format('Y-m-d') : '';	
$fecharetiro = '0000-00-00';

//=================================================================================================================

if($operacion=='agregar')
{
	$sql ="INSERT INTO nompersonal (foto, nomposicion_id, nacionalidad, cedula, apellidos, nombres, apenom, sexo,
                estado_civil, fecnac, lugarnac, codpro, direccion, telefonos, email, estado, fecing, ficha, tipopres,
                forcob, codbancob, cuentacob, codbanlph, cuentalph, tipemp, suesal, tipnom, codcat, codcargo, 
                codnivel1, codnivel2, codnivel3, codnivel4, codnivel5, codnivel6, codnivel7, inicio_periodo, fin_periodo, 
                turno_id, seguro_social, hora_base, segurosocial_sipe, dv, num_decreto, fecha_decreto, num_decreto_baja, 
                fecha_decreto_baja, siacap, puesto_id, imagen_cedula,sueldopro,fecharetiro) 
			VALUES 
			(NULLIF('". (isset($insertFoto) ? $insertFoto : '') ."',''),
	 		 NULLIF('". (isset($_POST['nomposicion_id']) ? $_POST['nomposicion_id'] : '' ) ."',''),
			 '". $_POST['nacionalidad'] ."',
			 '". $_POST['cedula'] ."',
			 '". $_POST['apellidos'] ."',
			 '". $_POST['nombres'] ."',
			 '". $_POST['apellidos'] . ", " . $_POST['nombres'] ."',
			 '". $_POST['sexo'] ."',
			 '". $_POST['estado_civil'] ."',
			 '". $fecnac ."',
			 '". $_POST['lugarnac'] ."',
			 '". $_POST['codpro'] ."',
			 '". $_POST['direccion'] ."',
			 '". $_POST['telefonos'] ."',
			 NULLIF('". $_POST['email'] ."',''),
			 '". $_POST['estado'] ."',
			 '". $fecing ."',
			 '". $_POST['ficha'] ."',
			 NULLIF('". (isset($_POST['tipopres']) ? $_POST['tipopres'] : '' ) ."',''),
			 '". $_POST['forcob'] ."',
			 NULLIF('". (isset($_POST['codbancob']) ? $_POST['codbancob'] : '') ."',''),
			 NULLIF('". (isset($_POST['cuentacob']) ? $_POST['cuentacob'] : '') ."',''),
			 NULLIF('". (isset($_POST['codbanlph']) ? $_POST['codbanlph'] : '') ."',''),
			 NULLIF('". (isset($_POST['cuentalph']) ? $_POST['cuentalph'] : '') ."',''),
			 '". $_POST['tipemp'] ."',
			 '". $suesal ."',
			 '". $_POST['tipnom'] ."',
			 '". $_POST['codcat'] ."',
			 '". $_POST['codcargo'] ."',
			 '". $codnivel1 ."',
			 '". $codnivel2 ."',
			 '". $codnivel3 ."',
			 '". $codnivel4 ."',
			 '". $codnivel5 ."',
			 '". $codnivel6 ."',
			 '". $codnivel7 ."',
			 NULLIF('". $inicio_periodo ."',''),
			 NULLIF('". $fin_periodo ."',''),
			 NULLIF('". $_POST['turno_id'] ."',''),
			 NULLIF('". $_POST['seguro_social'] ."',''),
			 '". $_POST['hora_base'] ."',
			 NULLIF('". $_POST['segurosocial_sipe'] ."',''),
			 NULLIF('". $_POST['dv'] ."',''),
			 NULLIF('". $_POST['num_decreto'] ."',''),
			 NULLIF('{$fecha_decreto}',''),
			 NULLIF('". $_POST['num_decreto_baja'] ."',''),
			 NULLIF('{$fecha_decreto_baja}',''),
			 NULLIF('". $_POST['siacap'] ."',''),
			 NULLIF('". (isset($_POST['puesto_id']) ? $_POST['puesto_id'] : '') ."',''),
			 NULLIF('". (isset($insertImagenCedula) ? $insertImagenCedula : '') ."',''), 
			 '". $sueldopro ."',
			 '". $fecharetiro ."'
			)";

	$res = $db->query($sql);

	if($res)
		echo "<script>document.location.href = 'ag_integrantes.php';</script>";
	else
		echo "<script>alert('¡Hay errores en el proceso!');</script>";
}
else
{
	$sql = "UPDATE nompersonal SET
				".$setFoto."
				".$setImagenCedula."
	            nacionalidad       = '".$_POST['nacionalidad']."',
	            nomposicion_id     = NULLIF('". (isset($_POST['nomposicion_id']) ? $_POST['nomposicion_id'] : '' )."',''),
	            cedula             = '".$_POST['cedula']."',
	            apellidos          = '".$_POST['apellidos']."',
	            nombres            = '".$_POST['nombres']."',
	            apenom             = '".$_POST['apellidos'].", ".$_POST['nombres']."',
	            sexo               = '".$_POST['sexo']."',
	            estado_civil       = '".$_POST['estado_civil']."',
	            fecnac             = '".$fecnac."',
	            lugarnac           = '".$_POST['lugarnac']."',
	            codpro             = '".$_POST['codpro']."',
	            direccion          = '".$_POST['direccion']."',
	            telefonos          = '".$_POST['telefonos']."',
	            email              = NULLIF('".$_POST['email']."',''),
	            estado             = '".$_POST['estado']."',
	            fecing             = '".$fecing."',
	            ficha              = '".$_POST['ficha']."',
	            tipopres           = NULLIF('". (isset($_POST['tipopres']) ? $_POST['tipopres'] : '' ) ."',''),
	            forcob             = '".$_POST['forcob']."',
	            codbancob          = NULLIF('". (isset($_POST['codbancob']) ? $_POST['codbancob'] : '') ."',''),
	            cuentacob          = NULLIF('". (isset($_POST['cuentacob']) ? $_POST['cuentacob'] : '') ."',''),
	            codbanlph          = NULLIF('". (isset($_POST['codbanlph']) ? $_POST['codbanlph'] : '' ) ."',''),
	            cuentalph          = NULLIF('". (isset($_POST['cuentalph']) ? $_POST['cuentalph'] : '' ) ."',''),
	            tipemp             = '".$_POST['tipemp']."',
	            suesal             = '".$suesal."',
	            tipnom             = '".$_POST['tipnom']."',
	            codcat             = '".$_POST['codcat']."',
	            codcargo           = '".$_POST['codcargo']."',
	            codnivel1	       = '".$codnivel1."',
	            codnivel2	       = '".$codnivel2."',
	            codnivel3	       = '".$codnivel3."',
	            codnivel4          = '".$codnivel4."',
	            codnivel5          = '".$codnivel5."',
	            codnivel6          = '".$codnivel6."',
	            codnivel7          = '".$codnivel7."',
	            inicio_periodo     = NULLIF('".$inicio_periodo."',''),
	            fin_periodo        = NULLIF('".$fin_periodo."',''), 
	            turno_id           = NULLIF('".$_POST['turno_id']."',''),
	            seguro_social      = NULLIF('".$_POST['seguro_social']."',''),
	            hora_base          = '".$_POST['hora_base']."',
	            segurosocial_sipe  = NULLIF('".$_POST['segurosocial_sipe']."',''),
	            dv                 = NULLIF('".$_POST['dv']."',''),
	            num_decreto        = NULLIF('".$_POST['num_decreto']."',''),
	            fecha_decreto      = NULLIF('{$fecha_decreto}','') ,
	            num_decreto_baja   = NULLIF('".$_POST['num_decreto_baja']."',''),
	            fecha_decreto_baja = NULLIF('{$fecha_decreto_baja}',''),
	            siacap             = NULLIF('".$_POST['siacap']."',''),
	            puesto_id          = NULLIF('". (isset($_POST['puesto_id']) ? $_POST['puesto_id'] : '')."',''),
	            sueldopro             = '".$suesal."' 		                    
		   WHERE  personal_id = '".$_POST['personal_id']."'";

	$res = $db->query($sql);

	if($res)
	{
		if($suesal != $sueldo_original)
		{
			$descripcion = 'Modificacion de sueldo a ficha ' . $_POST['ficha'] . '. Sueldo anterior '. $sueldo_original;

			$sql = "INSERT INTO log_transacciones (cod_log, descripcion, fecha_hora, modulo, url, accion, valor, usuario) 
		            VALUES (NULL, '".$descripcion."', now(), 'Datos-Integrantes', 'ag_integrantes.php', 'editar','".$suesal."','".$_SESSION['nombre'] ."')";

		    $res = $db->query($sql);
		}

		if($_POST['codcargo'] != $_POST['cargo_original'])
		{
			$descripcion = 'Modificacion de cargo a ficha '.$_POST['ficha'].'. Cargo anterior '.$_POST["cargo_original"];

			$sql = "INSERT INTO log_transacciones (cod_log, descripcion, fecha_hora, modulo, url, accion, valor, usuario) 
		            VALUES (NULL, '".$descripcion."', now(), 'Datos-Integrantes', 'ag_integrantes.php', 'editar','".$_POST['codcargo']."','".$_SESSION['nombre'] ."')";

		    $res = $db->query($sql);	
		}

		if(isset($_POST['nomposicion_id']) && $_POST['nomposicion_id'] != $_POST['posicion_original'])
		{
			$descripcion = 'Modificacion de posicion a ficha '.$_POST['ficha'].'. Posicion anterior '.$_POST["posicion_original"];

			$sql = "INSERT INTO log_transacciones (cod_log, descripcion, fecha_hora, modulo, url, accion, valor, usuario) 
		            VALUES (NULL, '".$descripcion."', now(), 'Datos-Integrantes', 'ag_integrantes.php', 'editar','".$_POST['nomposicion_id']."','".$_SESSION['nombre'] ."')";

		    $res = $db->query($sql);		
		}

		echo "<script>document.location.href = 'ag_integrantes.php?ficha=".$_POST['ficha']."&edit';</script>";
	}
	else
		echo "<script>alert('¡Hay errores en el proceso!');</script>";
}