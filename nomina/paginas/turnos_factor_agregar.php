
<?php 
require_once '../../generalp.config.inc.php';
session_start();
ob_start();

//error_reporting(E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR);
//nomturnos_factor        	factor_id,    turno_id,    descripcion,  valor,  fecha...

require_once '../lib/common.php';
include ("func_bd.php");
$conexion=conexion();

if( isset($_POST['btn-guardar']) )
{
	$factor_id  = ( isset($_POST['factor_id'])  ) ? $_POST['factor_id']  : '';
	$turno_id = ( isset($_POST['turno_id']) ) ? $_POST['turno_id'] : '';
	$descripcion = ( isset($_POST['descripcion']) ) ? $_POST['descripcion'] : '';
	$valor = ( isset($_POST['valor']) ) ? $_POST['valor'] : '';
//	$fecha = ( isset($_POST['fecha']) ) ? $_POST['fecha'] : '';

	$sql = "UPDATE nomturnos_factor SET turno_id='".$turno_id."',descripcion='".$descripcion."',valor='".$valor."' WHERE factor_id='".$factor_id."'";
	$res = query($sql, $conexion);
	activar_pagina("turnos_factor_list.php");    
}

if( isset($_GET['factor_id']) || isset($_POST['factor_id']) )
{
	$factor_id = ( isset($_GET['factor_id']) ) ? $_GET['factor_id'] : $_POST['factor_id'] ;

	$sql = "SELECT * FROM nomturnos_factor WHERE factor_id=".$factor_id;
	$res = query($sql, $conexion);

	if( $fila = fetch_array($res) )
	{
		$turno_id = $fila['turno_id'];
		$descripcion = $fila['descripcion'];
		$valor = $fila['valor'];
//		$fecha = $fila['fecha'];
	}
}
?>
<?php include("../header4.php"); // <html><head></head><body> ?>
<link href="../../includes/assets/css/custom-datatables.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
.portlet > .portlet-body.blue, .portlet.blue {
    background-color: #ffffff !important;
}
</style>
<div class="page-container"> 
	<!-- BEGIN SIDEBAR -->
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i> Modificar Factor Turnos
							</div>
						</div>
						<div class="portlet-body form">
							<form name="form-profesion" method="post" role="form" style="margin-bottom: 5px;">
								<input type="hidden" id="factor_id" name="factor_id" value="<?php echo $factor_id; ?>">
								<div class="form-body">
									<div class="form-group">
										<label for="descripcion">Id Factor</label>
										<input type="text" class="form-control" 
										       id="factor_id" name="factor_id" value="<?php echo $factor_id; ?>" disabled>
									</div>
									
			
									
									<div class="form-group">
										<label for="descripcion">Id Turno</label>
										<input type="text" class="form-control" 
										       id="turno_id" name="turno_id" value="<?php echo $turno_id; ?>" required>
									</div>
									<div class="form-group">
										<label for="descripcion">Descripci&oacute;n</label>
										<input type="text" class="form-control" 
										       id="descripcion" name="descripcion" value="<?php echo $descripcion; ?>" required>
									</div>
									<div class="form-group">
										<label for="descripcion">Valor</label>
										<input type="text" class="form-control" 
										       id="valor" name="valor" value="<?php echo $valor; ?>" required>
									</div>
			<!--						<div class="form-group">
										<label for="fecha">Fecha</label>
										<input type="text" class="form-control" 
										       id="fecha" name="fecha" value="</div>/?php echo// $fecha; ?>" required>
									</div>
									-->
									<button type="submit" class="btn blue" id="btn-guardar" name="btn-guardar">Guardar</button>
									<button type="button" class="btn default" 
									        onclick="javascript: document.location.href='nomturnos_factor_list.php'">Cancelar</button>
								</div>
							</form>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<?php include("../footer4.php"); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$( "#btn-guardar" ).click(function() {
		    var descripcion = $('#descripcion').val();

		    if( descripcion == '' )
		    {
		    	alert('Debe llenar los campos obligatorios');
		    	return false;
		    }
		});
	});
</script>
</body>
</html>