<?php
/**
 * Created by PhpStorm.
 * User: Luis J Montero G
 * Date: 06/07/2016
 * Time: 12:24 PM
 */
session_start();
ob_start();
$termino = $_SESSION['termino'];
?>
<?php
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // Always modified
header("Cache-Control: private, no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache"); // HTTP/1.0
?>


<?php
include ("../header.php");
include("../lib/common.php");
include("func_bd.php");
$conexion=conexion();
?>
<!DOCTYPE html>
<html lang="es">
<script type="text/javascript">
    function enviarprocesar()
    {
        txtFechaFin=document.getElementsByTagName('txtFechaFin');
        txtFechaInicio=document.getElementsByTagName('txtFechaInicio');
        AbrirVentana('barraprogreso_2.php?txtFechaInicio='+txtFechaInicio+'&txtFechaFin='+txtFechaFin, 150, 500, 0);
    }
</script>
<form action="procesar_control_asistencia.php" method="GET" name="frmAgregar" id="frmAgregar" enctype="multipart/form-data"  >
    <?php
    titulo("Parametros Control de Asistencia", "", "importar_asistencia.php", "acceso");
    ?><br><br><br><br><br><br><br><br>
    <div id="content" >
        <table width="400" align=center border="1" >
            <tr>
                <td>
                    <table width="400" align=center border="0" >
                        <tr class="tb-fila">
                            <td><font size="2" face="Arial, Helvetica, sans-serif">Fecha de Inicio:</font></td>
                            <td>
                                <input name="txtFechaInicio" type="text" id="txtFechaInicio" style="width:100px" value="" maxlength="60" onblur="javascript:actualizar('txtFechaInicio','fila_edad');">
                                <input name="image2" type="image" id="d_fechainicio" src="../lib/jscalendar/cal.gif" alt=""/>
                                <script type="text/javascript">Calendar.setup({inputField:"txtFechaInicio",ifFormat:"%Y-%m-%d",button:"d_fechainicio"});</script>
                            </td>
                        </tr>
                        <tr >
                            <td><font size="2" face="Arial, Helvetica, sans-serif">Fecha de Fin:</font></td>
                            <td>
                                <input name="txtFechaFin" type="text" id="txtFechaFin" style="width:100px" value="" maxlength="60" onblur="javascript:actualizar('txtFechaFin','fila_edad');">
                                <input name="image2" type="image" id="d_fechafin" src="../lib/jscalendar/cal.gif" alt=""/>
                                <script type="text/javascript">Calendar.setup({inputField:"txtFechaFin",ifFormat:"%Y-%m-%d",button:"d_fechafin"});</script>
                                <input name="codigo_nomina" type="hidden" value="<?= $_SESSION['codigo_nomina']; ?>">
                                <input name="registro_id" type="hidden" value="">
                            </td>
                        </tr>
                        <!--

                            <tr class="tb-fila">
                        <td><font size="2" face="Arial, Helvetica, sans-serif">Archivo: </font></td>
                        <td><input type="file" name="archivo" id="archivo">
                        </td>
                        </tr>
                        -->
                    </table>
                    <table align=center>
                        <tr >
                            <td width="400">
                                <div align="center">
                                    <input type="submit" name="procesar" id="procesar" value="Procesar">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</form>

</body>
</html>