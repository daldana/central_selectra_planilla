<?php
session_start();
ob_start();

require('fpdf.php');
include("../lib/common.php");

function fecha($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY
    if (!empty($value))
        return substr($value, 8, 2) . "/" . substr($value, 5, 2) . "/" . substr($value, 0, 4);
}
$nomina_id = $_GET['nomina_id'];
$codt = $_GET['codt'];

class PDF extends FPDF {

    function header()
    {
        $nomina_id = $_GET['nomina_id'];
        $codt = $_GET['codt'];
        $Conn = conexion();
        $var_sql = "select * from nomempresa";
        $rs = query($var_sql, $Conn);
        $row_rs = fetch_array($rs);
        $var_izquierda = '../imagenes/' . $row_rs['imagen_izq'];

        $consulta = "SELECT periodo_ini,periodo_fin FROM nom_nominas_pago WHERE codnom = '".$nomina_id."' AND codtip = '".$codt."'";
        $resultado = query($consulta,$Conn);
        $fetch = fetch_array($resultado);
        $fecha_ini = $fetch['periodo_ini'];
        $fecha_fin = $fetch['periodo_fin'];

        $this->SetFont("Arial", "B", 14);
        $this->Cell(0, 5, $row_rs['nom_emp'], 0, 1, 'C');
        
        $this->SetFont("Arial", "B", 12);
        $this->Cell(89, 5, 'Fecha: '.date('Y-m-d'), 0, 0, 'C');
        $this->SetFont("Arial", "B", 14);
        $this->Cell(100, 5, 'LISTADO DE LA PLANILLA DEL PERIODO', 0, 0, 'C');
        $this->SetFont("Arial", "B", 12);
        $this->Cell(89, 5,$_SESSION['nombre'].' '.date('h:m:s'), 0, 1, 'C');

        $this->Cell(89, 5,'', 0, 0, 'C');
        $this->Cell(100, 5, $fecha_ini.' al '.$fecha_fin, 0, 0, 'C');
        $this->Cell(89, 5, utf8_decode('Página ') . $this->PageNo() . '/{nb}', 0, 1, 'C');

        $this->SetFont("Arial", "", 10);
        $this->Ln(5);
        $this->SetFont("Arial", "B", 8);
        $this->SetWidths(array(10, 80, 18,15,18, 18,18,18, 18,18,20,20,20));
        $this->SetAligns(array('L', 'C', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'C' ));
        $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
        $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
        $this->Row(array('No', 'Nombre', 'Salario Quincenal', 'Renta hora', 'Total Devengado', 'F.Compl', 'Seguro Educativo', 'Seguro Social', 'Otros Descuentos', 'Total Descuentos', 'Sueldo Neto'));
        $this->Ln(4);
    }
//Hacer que sea multilinea sin que haga un salto de linea
    var $widths;
    var $aligns;
    var $celdas;
    var $ancho;

    function SetWidths($w) {
        //Set the array of column widths
        $this->widths = $w;
    }

    function SetAligns($a) {
        //Set the array of column alignments
        $this->aligns = $a;
    }

// Marco de la celda
    function Setceldas($cc) {

        $this->celdas = $cc;
    }

// Ancho de la celda
    function Setancho($aa) {
        $this->ancho = $aa;
    }

    function CheckPageBreak($h) {
        //If the height h would cause an overflow, add a new page immediately
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt) {
        //Computes the number of lines a MultiCell of width w will take
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                }
                else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

    function Row($data) {
        //Calculate the height of the row
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            //Save the current position
            $x = $this->GetX();
            $y = $this->GetY();
            //Draw the border
            //$this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w, $this->ancho[$i], $data[$i], $this->celdas[$i], $a);
            //Put the position to the right of the cell
            $this->SetXY($x + $w, $y);
        }
        //Go to the next line
        $this->Ln($h);
    }

//fin

    function personas($nomina_id, $codt, $pdf)
    {
        $conexion = conexion();
        $sql = "SELECT MAX(codorg) AS codorg FROM nomnivel1";
        $result1 = query($sql, $conexion);
        $max = fetch_array($result1);

        $query2 = "SELECT a.ficha,a.nombres, a.apellidos,a.cedula,a.seguro_social,a.suesal,c.codorg,c.descrip FROM nompersonal AS a,nom_movimientos_nomina AS b,nomnivel1 AS c WHERE a.ficha = b.ficha AND a.estado = 'Activo' AND b.tipnom = '".$codt."' AND a.codnivel1 = c.codorg GROUP BY a.ficha ORDER BY a.ficha";
        $result = query($query2, $conexion);
        $contador = 1;
        $num = 1;
        $posicion= "";
        $persona = 0;
        $totalpersonas = 0;
        $totalbd = num_rows($result);
        $pers = 0;
        $i=0;
        $sub_i=0;
        $j=1;
        $lineas=2;
        while ($fila = fetch_array($result))
        {
            $lineas=$lineas+1;
            $this->Ln(2);
            if ($lineas == 3)
            {
                $this->AddPage('L', 'A4');
                $lineas=0;
            }
                $persona+=1;
                $sficha = $fila['ficha'];
                $squincenal=0;
                $query = "select monto from nom_movimientos_nomina 
            where ficha = '" . $sficha . "' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND codcon=100";
                $result2 = query($query, $conexion);                
                $fetch2=fetch_array($result2);
                $squincenal=$fetch2[monto];

                $ss=0;
                $query = "select monto from nom_movimientos_nomina 
            where ficha = '" . $sficha . "' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND codcon=200";
                $result4 = query($query, $conexion);                
                $fetch4=fetch_array($result4);
                $ss=$fetch4[monto];

                $se=0;
                $query = "select monto from nom_movimientos_nomina 
            where ficha = '" . $sficha . "' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND codcon=201";
                $result5 = query($query, $conexion);
                $fetch5=fetch_array($result5);
                $se=$fetch5[monto];

                $isr=0;
                $query = "select monto from nom_movimientos_nomina 
            where ficha = '" . $sficha . "' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND codcon=202";
                $result3 = query($query, $conexion);                
                $fetch3=fetch_array($result3);
                $isr=$fetch3[monto];
                
                $fc=0;
                $query = "select monto from nom_movimientos_nomina 
            where ficha = '" . $sficha . "' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND codcon=204";
                $result6 = query($query, $conexion);
                $fetch6=fetch_array($result6);
                $fc=$fetch6[monto];
                $ot=0;
                $query = "select ifnull(sum(monto),0) as monto from nom_movimientos_nomina where ficha = '".$sficha."' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND tipcon='D' and codcon not in (200,201,202,204)";
                $result7 = query($query, $conexion);
                $fetch7=fetch_array($result7);
                $ot=$fetch7[monto];

                $total_deduccion = $isr+$ss+$se+$fc+$ot;
                $monto_cheque =  $squincenal-$total_deduccion;

                if ($fila["codorg"] > $j)
                {
                $this->SetFont("Arial", "B", 9);
                $this->SetWidths(array(50,40,15,18,18,18,18,18,18,18,18,18));
                $this->SetAligns(array('L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
                $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
                $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
                $this->Row(array("Planilla No.  ".$j,$sub_total_per,'','',$sub_total_sq,$sub_total_fc,$sub_total_se,$sub_total_ss,$sub_total_ot,$sub_total_de,$sub_total_s,''));
                $this->SetWidths(array(270));
                $this->SetAligns(array('C' ));
                $this->Setceldas(array('B'));
                $this->Setancho(array(2));
                $this->Row(array(''));
                    $j=$fila["codorg"];
                    $sub_i=0;
                    $sub_total_per = 0;
                    $sub_total_sq  = 0;
                    $sub_total_fc  = 0;
                    $sub_total_se  = 0;
                    $sub_total_ss  = 0;
                    $sub_total_ot  = 0;
                    $sub_total_de  = 0;
                    $sub_total_s   = 0;
                }
            if ($fila[codorg] >= $j)
            {
                    
                $sub_i=$sub_i+1;
                $this->SetFont('Arial', '', 8);
                $this->SetWidths(array(10,40,40,15,18,18,18,18,18,18,18,18,18));
                $this->SetAligns(array('L', 'L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
                $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
                $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
                $this->Row(array($sficha,$fila[nombres],$fila[apellidos],$squincenal,'',$squincenal,$fc,$se,$ss,'','',$monto_cheque,''));
                
                $this->SetWidths(array(30,30,30,15,18,18,18,18,18,18,18,18,18));
                $this->SetAligns(array('L', 'L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
                $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
                $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
                $this->Row(array('Ced: '.$fila[cedula],'','','','','','','','','','','',''));
                if($fila[seguro_social] != ''){$dat=' S.S: '.$fila[seguro_social];}else{$dat=' S.S: '.'S/S';}
                $this->Row(array($dat,'','','','','','','','','','','',''));

                if($ot>0)
                {
                    $query = "select codcon, descrip, monto from nom_movimientos_nomina where ficha = '".$sficha."' and tipnom='".$codt."' and codnom = '" . $nomina_id . "' AND tipcon='D' and codcon not in (200,201,202,204)";
                    $result8 = query($query, $conexion);
                    $prestamos=0;
                    while($fetch8=fetch_array($result8))
                    {
                        $prestamos=$prestamos+1;
                        if ($prestamos == 4){$lineas=$lineas+1;}
                        $query = "SELECT * FROM nomprestamos WHERE descrip = '".$fetch8[descrip]."'";
                        $r = query($query, $conexion);
                        $fil=fetch_array($r);
                        
                        $this->SetFont('Arial', '', 8);
                        $this->SetWidths(array(10,40,15,6,6,6,6,6,100,18,18,18,18));
                        $this->SetAligns(array('L', 'L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
                        $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
                        $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
                        if ((isset($fil[formula])) && (isset($fil[descrip]))) {
                            $dato=$fil[formula].' '.$fil[descrip];
                            $this->Row(array('', '','', '', '', '', '', '', $dato, $fetch8[monto], '','', ''));
                        }elseif ((isset($fil[formula])) && (!isset($fil[descrip]))) {
                            $dato=$fil[formula].' Sin Descripcion';
                            $this->Row(array('', '','', '', '', '', '', '',$dato, $fetch8[monto], '','', ''));
                        }elseif ((!isset($fil[formula])) && (isset($fil[descrip]))) {
                            $dato='Sin Formula '.$fil[descrip];
                            $this->Row(array('', '','', '', '', '', '', '',$dato, $fetch8[monto], '','', ''));
                        }elseif ((!isset($fil[formula])) && (!isset($fil[descrip]))) {
                            $dato='Sin Formula, Sin Descripcion';
                            $this->Row(array('', '','', '', '', '', '', '',$dato, $fetch8[monto], '','', ''));
                        }
                    }
                }
                $this->SetWidths(array(270));
                $this->SetAligns(array('C' ));
                $this->Setceldas(array('B'));
                $this->Setancho(array(2));
                $this->Row(array(''));
                $this->Row(array(''));

                $this->SetFont("Arial", "B", 8);
                $this->SetWidths(array(10,40,40,15,18,18,18,18,18,18,18,18,18));
                $this->SetAligns(array('L', 'L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
                $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
                $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
                
                $this->Row(array('','','','','Total:',$squincenal,$fc,$se,$ss,$ot,$total_deduccion,$monto_cheque,''));

                $this->SetWidths(array(270));
                $this->SetAligns(array('C' ));
                $this->Setceldas(array('B'));
                $this->Setancho(array(2));
                $this->Row(array(''));
                $i=$i+1;
                //-----------------------------------------------------------------------------------------------------------------------------------------
                $sub_total_per = $sub_i;
                $sub_total_sq  = $sub_total_sq+$squincenal;
                $sub_total_fc  = $sub_total_fc+$fc;
                $sub_total_se  = $sub_total_se+$se;
                $sub_total_ss  = $sub_total_ss+$ss;
                $sub_total_ot  = $sub_total_ot+$ot;
                $sub_total_de  = $sub_total_de+$total_deduccion;
                $sub_total_s   = $sub_total_s+$monto_cheque;
                //-----------------------------------------------------------------------------------------------------------------------------------------
                $total_per = $i;
                $total_sq  = $total_sq+$squincenal;
                $total_fc  = $total_fc+$fc;
                $total_se  = $total_se+$se;
                $total_ss  = $total_ss+$ss;
                $total_ot  = $total_ot+$ot;
                $total_de  = $total_de+$total_deduccion;
                $total_s   = $total_s+$monto_cheque;
                //-----------------------------------------------------------------------------------------------------------------------------------------
            }
        }
        $this->SetFont("Arial", "B", 9);
        $this->SetWidths(array(50,40,15,18,18,18,18,18,18,18,18,18));
        $this->SetAligns(array('L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
        $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
        $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
        $this->Row(array("Planilla No.  ".$j,$sub_total_per,'','',$sub_total_sq,$sub_total_fc,$sub_total_se,$sub_total_ss,$sub_total_ot,$sub_total_de,$sub_total_s,''));
        $this->SetWidths(array(270));
        $this->SetAligns(array('C' ));
        $this->Setceldas(array('B'));
        $this->Setancho(array(2));
        $this->Row(array(''));
            $j=$fila["codorg"];
            $sub_i=0;
            $sub_total_per = 0;
            $sub_total_sq  = 0;
            $sub_total_fc  = 0;
            $sub_total_se  = 0;
            $sub_total_ss  = 0;
            $sub_total_ot  = 0;
            $sub_total_de  = 0;
            $sub_total_s   = 0;

        $this->SetFont("Arial", "B", 9);
        $this->SetWidths(array(45,45,15,18,18,18,18,18,18,18,18,18));
        $this->SetAligns(array('L', 'L', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R' ));
        $this->Setceldas(array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'));
        $this->Setancho(array(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
        $this->Row(array('Totales:',$total_per,'','',$total_sq,$total_fc,$total_se,$total_ss,$total_ot,$total_de,$total_s,''));
        $this->Ln(25);
        $this->SetFont("Arial", "B", 12);
        $this->SetWidths(array(92, 92, 92));
        $this->SetAligns(array('C', 'C', 'C'));
        $this->Setceldas(array('0', '0', '0'));
        $this->Setancho(array(5, 5, 5));
        $this->Row(array('Revisado Por ________________', 'Confeccionado Por: ________________', 'Directora de Recursos  POR: ________________'));
        $this->Ln();
        $this->Ln();
    }
    function Vacaciones($fecha_ini, $fecha_fin)
    {
        $conexion = conexion();
        $consulta_vac = "SELECT per.ficha, apenom, vaca.fechareivac FROM nompersonal as per, nom_progvacaciones as vaca WHERE per.tipnom=vaca.tipnom and per.tipnom =" . $_SESSION['codigo_nomina'] . " and per.ficha=vaca.ficha and '" . $fecha_ini . "'<=vaca.fechareivac and vaca.fechareivac<='".$fecha_fin."' ORDER BY per.ficha";
        $resultado_vac = query($consulta_vac, $conexion);

        $this->Ln(20);
        $this->SetFont('Arial', 'I', 12);
        if (num_rows($resultado_vac) != 0) {
            $this->Ln(300);
            $this->Cell(188, 8, 'PERSONAL DE VACACIONES', 0, 0, 'C');
            $this->Ln(10);
            $this->SetFont('Arial', '', 10);
            $cantidad_registros = 40;
            $totalwhile = num_rows($resultado_vac);
            $contar = 1;
            while ($totalwhile >= $contar) {
                $fetchvac = fetch_array($resultado_vac);
                $this->Cell(60, 5, $fetchvac['ficha'], 0, 0, 'R');
                $this->Cell(80, 5, '   ' . $fetchvac['apenom'], 0, 0, 'L');
                $this->Cell(20, 5, '   ' . fecha($fetchvac['fechareivac']), 0, 0, 'L');
                $this->Ln();
                if ($contar == $cantidad_registros) {
                    $this->Ln(300);
                    $this->SetFont('Arial', 'I', 12);
                    $this->Cell(188, 8, 'PERSONAL DE VACACIONES', 0, 0, 'C');
                    $this->SetFont('Arial', 'I', 10);
                    $this->Ln(10);
                }
                $contar++;
            }
        }
    }
    function firmas($total_asig, $total_dedu, $cantidad)
    {

        $conexion = conexion();
        $consulta_vac = "SELECT ficha, apenom FROM nompersonal WHERE tipnom =" . $_SESSION['codigo_nomina'] . " and estado='Vacaciones' ORDER BY ficha";

        $resultado_vac = query($consulta_vac, $conexion);

        $consultaa = "SELECT ficha, apenom FROM nompersonal WHERE tipnom =" . $_SESSION['codigo_nomina'] . " and estado<>'Egresado'";
        $resultadooo = query($consultaa, $conexion);
        $cant_personal+=num_rows($resultadooo);

        $query = "select * from nom_nominas_pago where codnom = '".$_GET['nomina_id']."' AND codtip= '".$_SESSION['codigo_nomina']."'";
        $result2 = query($query, $conexion);
        $fila2 = fetch_array($result2);

        $this->SetFont('Arial', '', 10);

        $queda = $cantidad - 5;
        if ($queda < 0) {
            $this->Ln(300);
            $this->Cell(188, 8,'Desde: '.fecha($fila2['periodo_ini']).' Hasta: '.fecha($fila2['periodo_fin']).' Pago: '.fecha($fila2['fechapago']), 0, 0, 'C');
            $this->Ln();
        }

        $this->Cell(60, 5, 'Cant. de Personas: ' . $cant_personal, 0, 1, 'L');
        $this->Ln(10);
        $this->SetFont('Arial', 'I', 7);
        // llamado para hacer multilinea sin que haga salto de linea
        $this->SetWidths(array(92, 92, 92));
        $this->SetAligns(array('C', 'C', 'C'));
        $this->Setceldas(array('0', '0', '0'));
        $this->Setancho(array(5, 5, 5));
        $this->Row(array('RECIBIDO POR: ____________________', 'AUTORIZADO POR: ____________________', 'PREPARADO POR: ____________________'));
    }
    function huellasPersonal() {
        $conexion = conexion();
        $sql = "SELECT apenom FROM nompersonal WHERE tipnom = {$_SESSION['codigo_nomina']} AND estado<>'Egresado'";
        $result = query($sql, $conexion);
        $cant = num_rows($result);
        $cont = 1;
        $x = 10;
        $y = $this->GetY();
        $this->SetFont('Arial', '', 4);

        while ($empleados = fetch_array($result)) {
            $n = $empleados['apenom'];
            $apenom = explode(',', $n);
            $apellidos = explode(' ', trim($apenom[0]));
            $nombres = explode(' ', trim($apenom[1]));

            $this->SetXY($x, $y);
            $this->Cell(20, 20, '', 1, 1);
            $this->SetXY($x, $y + 20);
            $this->Cell(20, 10, $apellidos[0].' '.  substr($apellidos[1], 0, 1) . '., ' . $nombres[0].' '.  substr($nombres[1], 0, 1) . '.', 1, 1);
            $x+=23;
            if ($cont == $cant) {
                $y+=30;
            }
            $cont++;
        }
    }
}
//Creación del objeto de la clase heredada
$pdf = new PDF();
$pdf->AliasNbPages();

$pdf->AddFont('Sanserif', '', 'sanserif.php');
$pdf->SetFont('Sanserif', '', 10);
$pdf->personas($nomina_id, $codt, $pdf);
$pdf->Output();
?>
