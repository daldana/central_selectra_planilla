<?php 
session_start();
ob_start();
?>
<?php 
$url="expediente_list_ajax";
$modulo="Expediente";
$tabla="nomexpediente";
$titulos=array("Codigo","Tipo","Descripcion"," ","Fecha","Acciones");
$indices=array("cod_expediente_det","tipo_registro","tipo_tiporegistro","descripcion","fecha");

//DECLARACION DE LIBRERIAS
require_once '../lib/common.php';
include ("../paginas/funciones_nomina.php");

$conexion=conexion();
$cedula=$_GET['cedula'];
$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$pagina=@$_GET['pagina'];
?>
<script type="text/javascript">
function confirmar2(valor,cedula)
{
	if (confirm("\u00BFSeguro desea eliminar este registro?") == true) 
		window.location.href="expediente_list_ajax.php?cod_eliminar="+valor+"&cedula="+cedula
}

</script>
<link href="../estilos.css" rel="stylesheet" type="text/css" />
<?

if(isset($_GET['cod_eliminar']))
{
	$consulta="SELECT * FROM nomexpediente WHERE cod_expediente_det=$_GET[cod_eliminar] AND cedula='$_GET[cedula]'";
	$resultadoCon=query($consulta,$conexion);
	$fetchCon=fetch_array($resultadoCon,$conexion);
	
	if(($fetchCon['tipo_registro']=="Experiencia")&&($fetchCon['tipo_tiporegistro']=="Trabajo realizado")&&($fetchCon['institucion_publica']==1))
	{
		$antiguedad=antiguedad($fetchCon['fecha_salida'],$fetchCon['fecha_retorno'],"A");
		echo $consulta="UPDATE nompersonal set antiguedadap=antiguedadap-(".$antiguedad.") WHERE cedula='$_GET[cedula]' AND tipnom='$_SESSION[codigo_nomina]'";
		$resultado5=query($consulta,$conexion);
	}
	$consulta="DELETE FROM nomexpediente WHERE cod_expediente_det=$_GET[cod_eliminar] AND cedula='$_GET[cedula]'";
	$resultado=query($consulta,$conexion);
}

if(isset($_POST['buscar']) || $tipob!=NULL)
{
	if(!$tipob)
	{
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
	}
	switch($tipob){
		case "exacta": 
			$consulta=buscar_exacta($tabla,$des,"tipo_registro");
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,"tipo_registro");
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,"tipo_registro");
			break;
	}
}
else
{
	$consulta="select * from ".$tabla." WHERE cedula='$cedula' ";
}
//echo $consulta." este es el valor quemuestra ";
$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);

include ("../header.php");
?>
<FORM name="<?echo $url?>" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
titulo($modulo,"expediente_agregar.php?cedula=$cedula","","21");
?>
<table class="tb-head" width="100%">
<tr>
<td><input type="text" name="buscar" size="20"></td>
<td><? btn('search',$url,1); ?></td>
<td><? btn('show_all',$url.".php?cedula=".$cedula,0); ?></td>
<td width="120"><input onclick="javascript:actualizar(this);"  type="radio" name="palabra" value="exacta">Palabra exacta</td>
<td width="140"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="todas">Todas las palabras</td>
<td width="150"><input onclick="javascript:actualizar(this);" checked="true" type="radio" name="palabra" value="cualquiera">Cualquier palabra</td>
<td colspan="3" width="386"></td>
</tr>
</table>
<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
<tbody>
<tr class="tb-head" >
<?
foreach($titulos as $nombre)
{
	echo "<td><STRONG>$nombre</STRONG></td>";
}
?>
<td></td>
<td></td>
</tr>
<? 
if($num_paginas!=0)
{
	$i=0; 
	while($fila=fetch_array($resultado))
	{
		$i++;
		if($i%2==0)
		{
			?>
			<tr class="tb-fila">
			<?
		}
		else
		{
			echo"<tr>";
		}
		foreach($indices as $campo)
		{
			if($campo=='descripcion' and ($fila[tipo_registro]=='Permisos' or$fila[tipo_registro]=='Licencias') ){
				$con="select * from nomsuspenciones where codigo='$fila[tipo_tiporegistro]'";
				$query=query($con,$conexion);
				$fetch1=fetch_array($query,$conexion);
				$descripcion=utf8_encode($fetch1[descrip]);
				echo"<td>$descripcion</td>";
				}else{
					$var=$fila[$campo];
					echo"<td>$var</td>";
				}
			
		}
		$cedula=$fila['cedula'];
		$codigo=$fila['cod_expediente_det'];
		icono("expediente_agregar.php?cedula=".$cedula."&codigo=".$codigo, "Editar", "edit.gif");
		//icono("expediente_adjunto.php?cedula=".$cedula."&codigo=".$codigo, "Adjuntar", "upload.jpg"); //estaba comentado lm
		$tipo_registro = $fila['tipo_registro'];
		if($tipo_registro=='Documentos')
		{

						$sql = "SELECT nombre_documento, descripcion, url_documento, fecha_registro, fecha_vencimiento 
								FROM   nomexpediente_documentos 
								WHERE  cod_expediente_det='{$codigo}'";

						$res=query($sql,$conexion);
						$documento = $res->fetch_object();

			//icono("expediente_documentos.php?cedula=".$cedula."&codigo=".$codigo, "Ver documento", "list.gif"); // expediente_documentos.php?cedula='.$cedula.'&codigo='.$codigo.'
			echo '<td><a href="'.$documento->url_documento.'" class="fancybox">'.
			     '<img title="Ver documento" src="../imagenes/list.gif" width="16" height="16" border="0"></a></td>';
		}
		else
			icono("expediente_adjunto.php?cedula=".$cedula."&codigo=".$codigo, "Adjuntar", "upload.jpg"); //estaba comentado lm
			echo '<td></td>';
			//icono("expediente_adjunto.php?cedula=".$cedula."&codigo=".$codigo, "Adjuntar", "upload.jpg"); //estaba comentado lm
		icono("javascript:confirmar2('$codigo','$cedula')", "Eliminar", "delete.gif");
	
		echo "</tr>";
	}
}
else
{
	echo "<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
cerrar_conexion($conexion);
?>
</tbody>
</table>
<?
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des,$num_paginas);
?>
</FORM>
<!--
<script src="../../includes/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="../../includes/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>-->
<link href="../../includes/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<script src="../../includes/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script>
$(document).ready(function() {
    $('a.fancybox').fancybox({
    	topRatio: 0,
        type: 'image'
    });
});
</script>
</BODY>
</html>