<?php
session_start();
ob_start();
?>
<?php 
$url="expediente_agregar";
$modulo="Agregar registro de expediente";
	
//DECLARACION DE LIBRERIAS
require_once '../lib/common.php';
require_once '../paginas/func_bd.php';
include ("../paginas/funciones_nomina.php");
include ("../header.php");
$conexion=conexion();
$cedula=$_GET['cedula'];
if(isset($_POST['cedula']))
	$cedula=$_POST['cedula'];
$codigo=$_GET['codigo'];
?>


<link rel="stylesheet" type="text/css" href="../css/jquery-ui-1.7.2.custom.css" />
<!--<script type="text/javascript" src="../lib/jquery.js"></script> -->
<script type="text/javascript" src="../lib/jquery.min.js"></script>
<script type="text/javascript" src="../lib/jquery-ui.min.js"></script>

<script src="../lib/jquery.maskedinput.js" type="text/javascript"></script>

<script type="text/javascript">
var editar = '<?php echo $_POST["editar"]; ?>';
var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];    

function confirmar2(msg)
{
	if(($('#tipo_registro').val()=="Permisos")&&((($('#tipo_tiporegistro').val()==1))||(($('#tipo_tiporegistro').val()==2))))
	{
		var dispo = +$('#dispo').val();
		var dias = +$('#dias').val();
		var horas = +$('#horas').val();
		var min = +$('#minutos').val();
		
		var total = dispo - ((dias * 8) + horas + (min/60));
		
		if(total<0)
		{
			alert("El tiempo disponible es menor al introducido por favor revise!")
			return false;
		}
	}

	if($('#tipo_registro').val()=="Documentos")
	{
		var nombre_documento  = $("#nombre_documento").val();
		var fecha_vencimiento = $("#fecha_vencimiento").val();
		

		if(nombre_documento=='')
		{
			alert("Debe indicar el nombre del documento");
			return false;
		}

		if(fecha_vencimiento=='')
		{
			alert("Debe indicar la fecha de vencimiento del documento");
			return false;
		}

		if(editar==1)
		{
			var archivo = $("#archivo").val();

			if(archivo=='')
			{
				alert("Debe seleccionar un documento");
				return false;
			}

	        var sFileName = document.getElementById("archivo").value;
	        if (sFileName.length > 0) {
	            var blnValid = false;
	            for (var j = 0; j < _validFileExtensions.length; j++) {
	                var sCurExtension = _validFileExtensions[j];
	                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
	                    blnValid = true;
	                    break;
	                }
	            }
	            
	            if (!blnValid) { // " + sFileName + "
	                alert("\u0021Error! El archivo es inv\u00E1lido, las extensiones permitidas son: " + _validFileExtensions.join(", "));
	                return false;
	            }
	        }
		}
	}
	
	if(confirm(msg) == true) 
	{
		document.formulario1.opcion.value=1
		document.formulario1.submit()
	}
}

</script>

<script>
function inicio(){
    jQuery(function($){                                              
	   $.mask.definitions['T']='[PA]'; 
	   $('#desde').mask('99:99 TM',{placeholder:"_"});
	   $('#hasta').mask('99:99 TM',{placeholder:"_"});

    });                                     
}

function vacacion(){
    jQuery(function($){                                              
	   $.mask.definitions['T']='[PA]'; 
	   $('#desde').mask('99/9999',{placeholder:"_"});
	   $('#horas').mask('99/9999',{placeholder:"_"});
	   $('#hasta').mask('99/9999',{placeholder:"_"});
	   $('#fecha').mask('99/99/9999',{placeholder:"_"});
	   $('#aprobado').mask('99/99/9999',{placeholder:"_"});
	   $('#enterado').mask('99/99/9999',{placeholder:"_"});

    });                                     
}

function mostrar_campo(valor){
	if(valor=="Aumenta"){
		$('#efectividad').hide();
		}else{
		$('#efectividad').show();
		}
}
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});    
$(function(){
	$('#fecha_salida').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#fecha_retorno').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#fecha').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#aprobado').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#enterado').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#fecha1').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#fecha2').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#fecha3').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
	$('#fecha_vencimiento').live('click', function() {
	   $(this).datepicker({showOn:'focus'}).focus();
	});
});
</script>

<?php

// if(count($_POST)>0)
// {
// 	require_once "../lib/database.php";
// 	dump($_POST);
// 	ld($_POST);
// 	exit;
// }

if($_POST['opcion']==1)
{
	if($_POST['editar']!=1)
	{
		if ($_POST['tipo_registro']=="Permisos"){
			if($_POST['fecha_retorno']!='')
				$fecha_reintegro=fecha_sql($_POST['fecha_retorno']);
				
			if($_POST['fecha_salida']!='')
				$fecha_salida=fecha_sql($_POST['fecha_salida']);
			if($_POST['aprobado']!='')
				$aprobado=fecha_sql($_POST['aprobado']);
			if($_POST['enterado']!='')
				$enterado=fecha_sql($_POST['enterado']);	
			
			$consulta="INSERT INTO nomexpediente VALUES ('','$cedula','$_POST[tipo_registro]','$_POST[tipo_tiporegistro]','','','','$_POST[dias]','".$fecha_reintegro."','".$fecha_salida."','','','".fecha_sql($_POST['fecha'])."','$_SESSION[nombre]','', '', '', '', '', '', '', '', '', '', '', '', '', '','','', '','','','','','$_POST[desde]','$_POST[hasta]','$_POST[horas]','$_POST[minutos]','".$aprobado."','".$enterado."','','','','','','','','','','$_POST[numero_resolucion]','$_POST[numero_decreto]')";
			$resultado=query($consulta,$conexion);


		}else if ($_POST['tipo_registro']=="Tiempo Compensatorio"){
			
			if($_POST['fecha_salida']!='')
				$fecha_salida=fecha_sql($_POST['fecha_salida']);
			if($_POST['aprobado']!='')
				$aprobado=fecha_sql($_POST['aprobado']);
			if($_POST['enterado']!='')
				$enterado=fecha_sql($_POST['enterado']);	
			if($_POST[tipo_tiporegistro]=='Aumenta'){
			echo	$consulta="INSERT INTO nomexpediente VALUES ('','$cedula','$_POST[tipo_registro]','$_POST[tipo_tiporegistro]','','','','$_POST[dias]','','','','','".fecha_sql($_POST['fecha'])."','$_SESSION[nombre]','', '', '', '', '', '', '', '', '', '', '', '', '', '','','', '','','','','','','','$_POST[horas]','$_POST[minutos]','".$aprobado."','".$enterado."','','','','','','','','','','$_POST[numero_resolucion]','$_POST[numero_decreto]')";
			}else{
				$consulta="INSERT INTO nomexpediente VALUES ('','$cedula','$_POST[tipo_registro]','$_POST[tipo_tiporegistro]','','','','$_POST[dias]','','".$fecha_salida."','','','".fecha_sql($_POST['fecha'])."','$_SESSION[nombre]','', '', '', '', '', '', '', '', '', '', '', '', '', '','','', '','','','','','$_POST[desde]','$_POST[hasta]','$_POST[horas]','$_POST[minutos]','".$aprobado."','".$enterado."','','','','','','','','','','$_POST[numero_resolucion]','$_POST[numero_decreto]')";	
				}
			$resultado=query($consulta,$conexion);

		}else if ($_POST['tipo_registro']=="Vacaciones"){
			if($_POST['fecha_retorno']!='')
				$fecha_reintegro=fecha_sql($_POST['fecha_retorno']);
				
			if($_POST['fecha_salida']!='')
				$fecha_salida=fecha_sql($_POST['fecha_salida']);
			if($_POST['aprobado']!='')
				$aprobado=fecha_sql($_POST['aprobado']);
			if($_POST['enterado']!='')
				$enterado=fecha_sql($_POST['enterado']);	
			
			$consulta="INSERT INTO nomexpediente VALUES ('','$cedula','$_POST[tipo_registro]','$_POST[tipo_tiporegistro]','','','','$_POST[dias]','".$fecha_reintegro."','".$fecha_salida."','','','".fecha_sql($_POST['fecha'])."','$_SESSION[nombre]','', '', '', '', '', '', '', '', '', '', '', '', '', '','','', '','','','','','$_POST[desde]','$_POST[hasta]','$_POST[horas]','','','','$_POST[resol1]','$_POST[resol2]','$_POST[resol3]','".fecha_sql($_POST['fecha1'])."','".fecha_sql($_POST['fecha2'])."','".fecha_sql($_POST['fecha3'])."','$_POST[dr1]','$_POST[dr2]','$_POST[dr3]')";
			$resultado=query($consulta,$conexion);

		}
		elseif($_POST['tipo_registro']=="Documentos")
		{
			$nombre_documento  = (isset($_POST['nombre_documento']))  ? $_POST['nombre_documento']  : NULL;
			$fecha_vencimiento = str_replace('-', '/', $_POST['fecha_vencimiento']);
			$fecha_vencimiento = fecha_sql($fecha_vencimiento);
			$fecha = date('Y-m-d');
			//$descripcion_docu = (isset($_POST['descripcion'])) ? $_POST['descripcion'] : NULL;

			// Cargar el documento

			if(isset($_FILES['archivo']))
			{
				if ($_FILES['archivo']["error"] > 0)
					exit("¡Error al subir el archivo! Código: " . $_FILES['archivo']["error"]);
				else
				{
					$archivo = basename($_FILES['archivo']['name']);
					$archivo = str_replace(' ', '', strtolower($archivo));
					$archivo = "documentos/" . time() . '_' . $archivo ;

					if (! move_uploaded_file($_FILES['archivo']['tmp_name'], $archivo) ) 
						exit("¡Error! Al mover el archivo");
				}
			}

			// Registrar en nomexpediente
			$consulta="INSERT INTO nomexpediente 
					   (`cod_expediente_det`, `cedula`, `tipo_registro`, `tipo_tiporegistro`, `fecha`)
					   VALUES  
			           ('','{$cedula}','{$_POST['tipo_registro']}','{$nombre_documento}','".$fecha."')";

			$resultado=query($consulta,$conexion);

			if($resultado)
			{
				$ultimo_id = mysqli_insert_id($conexion);

				$consulta = "INSERT INTO `nomexpediente_documentos` 
				             (`id`, `nombre_documento`, `descripcion`, `url_documento`, `fecha_registro`, `fecha_vencimiento`, `cod_expediente_det`) 
				             VALUES 
				             ('', '{$nombre_documento}', '{$_POST['descripcion']}', '{$archivo}', '{$fecha}', '{$fecha_vencimiento}', '{$ultimo_id}')";
				$resultado=query($consulta,$conexion);
			}
		}
		else{
			if($_POST['fecha_reintegro']=='')
				$fecha_reintegro='';	
			else
				$fecha_reintegro=fecha_sql($_POST['fecha_reintegro']);
			
			if($_POST['fecha_salida']=='')
				$fecha_salida='';
			else
				$fecha_salida=fecha_sql($_POST['fecha_salida']);
		
		     $consulta="INSERT INTO nomexpediente VALUES ('','$cedula','$_POST[tipo_registro]','$_POST[tipo_tiporegistro]','$_POST[descripcion]','$_POST[monto]','$_POST[monto_nuevo]','$_POST[dias]','".$fecha_reintegro."','".$fecha_salida."','$_POST[cod_cargo]','$_POST[cod_cargo_nuevo]','".date("Y-m-d")."','$_SESSION[nombre]','$_POST[pagado_por_emp]', '$_POST[institucion]', '$_POST[tipo_estudio]', '$_POST[nivel_actual]', '$_POST[costo_persona]', '$_POST[num_participantes]', '$_POST[nombre_especialista]', '$_POST[gerencia_anterior]', '$_POST[gerencia_nueva]', '$_POST[nomina_anterior]', '$_POST[nomina_nueva]', '$_POST[puntaje]', '$_POST[calificacion]', '$_POST[labor]', '$_POST[institucion_publica]','$_POST[tcamisa]', '$_POST[tchaqueta]','$_POST[tbata]','$_POST[tpantalon]','$_POST[tmono]','$_POST[tzapato]','','','','','".$aprobado."','','','','','','','','','','','$_POST[numero_resolucion]','$_POST[numero_decreto]')";
			$resultado=query($consulta,$conexion);
			
			if($_POST['tipo_registro']=="Movimiento de Personal")
			{
				if($_POST['cod_cargo_nuevo']!='')
				{
					$consulta="UPDATE nompersonal set codcargo='$_POST[cod_cargo_nuevo]' WHERE cedula='$cedula' AND tipnom='$_SESSION[codigo_nomina]'";
					$resultado2=query($consulta,$conexion);
				}
				if($_POST['gerencia_nueva']!='')
				{
					$consulta="UPDATE nompersonal set codnivel4='$_POST[gerencia_nueva]' WHERE cedula='$cedula' AND tipnom='$_SESSION[codigo_nomina]'";
					$resultado3=query($consulta,$conexion);
				}
				if($_POST['nomina_nueva']!='')
				{
					$consulta="UPDATE nompersonal set tipnom='$_POST[nomina_nueva]' WHERE cedula='$cedula' AND tipnom='$_SESSION[codigo_nomina]'";
					$resultado4=query($consulta,$conexion);
				}
			}
			if($_POST['tipo_tiporegistro']=="4")
			{
				$consulta="UPDATE nompersonal set codcargo='$_POST[cod_cargo_nuevo]', suesal='$_POST[monto_nuevo]' WHERE cedula='$cedula' AND tipnom='$_SESSION[codigo_nomina]'";
				$resultado5=query($consulta,$conexion);
			}
			if($_POST['tipo_tiporegistro']=="1")
			{
				$consulta="UPDATE nompersonal set suesal='$_POST[monto_nuevo]' WHERE cedula='$cedula' AND tipnom='$_SESSION[codigo_nomina]'";
				$resultado5=query($consulta,$conexion);
			}
			if(($_POST['tipo_registro']=="Experiencia")&&($_POST['tipo_tiporegistro']=="Trabajo realizado")&&($_POST['institucion_publica']==1))
			{
				$fecha1=fecha_sql($_POST['fecha_salida']);
				$fecha2=fecha_sql($_POST['fecha_reintegro']);
				$antiguedad=antiguedad($fecha1,$fecha2,"A");
				$consulta="UPDATE nompersonal set antiguedadap=antiguedadap+$antiguedad WHERE cedula='$cedula' AND tipnom='$_SESSION[codigo_nomina]'";
				$resultado5=query($consulta,$conexion);
			}
		}
		
	}
	else
	{
		if($_POST['fecha_reintegro']=='')
			$fecha_reintegro='';	
		else
			$fecha_reintegro=fecha_sql($_POST['fecha_reintegro']);
		
		if($_POST['fecha_retorno']!='')
				$fecha_reintegro=fecha_sql($_POST['fecha_retorno']);
		
		if($_POST['fecha_salida']=='')
			$fecha_salida='';
		else
			$fecha_salida=fecha_sql($_POST['fecha_salida']);
		
		if($_POST['aprobado']!='')
			$aprobado=fecha_sql($_POST['aprobado']);
		if($_POST['enterado']!='')
			$enterado=fecha_sql($_POST['enterado']);
		if($_POST[tipo_tiporegistro]=='Aumenta'){
			$consulta="UPDATE nomexpediente SET tipo_registro='$_POST[tipo_registro]', tipo_tiporegistro='$_POST[tipo_tiporegistro]',monto='$_POST[monto]', monto_nuevo='$_POST[monto_nuevo]', dias='$_POST[dias]', fecha='".date("Y-m-d")."', fecha_salida='', fecha_retorno='$fecha_reintegro', cod_cargo='$_POST[cod_cargo]', cod_cargo_nuevo='$_POST[cod_cargo_nuevo]', usuario='$_SESSION[nombre]', descripcion='$_POST[descripcion]', pagado_por_emp='$_POST[pagado_por_emp]', institucion='$_POST[institucion]', tipo_estudio='$_POST[tipo_estudio]', nivel_actual='$_POST[nivel_actual]', costo_persona='$_POST[costo_persona]', num_participantes='$_POST[num_participantes]', nombre_especialista='$_POST[nombre_especialista]', gerencia_anterior='$_POST[gerencia_anterior]', gerencia_nueva='$_POST[gerencia_nueva]', nomina_anterior='$_POST[nomina_anterior]', nomina_nueva='$_POST[nomina_nueva]', puntaje='$_POST[puntaje]', calificacion='$_POST[calificacion]', labor='$_POST[labor]', institucion_publica='$_POST[institucion_publica]', tcamisa='$_POST[tcamisa]', tchaqueta='$_POST[tchaqueta]', tbata='$_POST[tbata]',tpantalon='$_POST[tpantalon]', tmono='$_POST[tmono]', tzapato='$_POST[tzapato]', desde='', hasta='', horas='$_POST[horas]', minutos='$_POST[minutos]', aprobado='$aprobado', enterado='$enterado',resol1='$_POST[resol1]',resol2='$_POST[resol2]',resol3='$_POST[resol3]',dr1='$_POST[dr1]',dr2='$_POST[dr2]',dr3='$_POST[dr3]',fecha1='".fecha_sql($_POST['fecha1'])."',fecha2='".fecha_sql($_POST['fecha2'])."',fecha3='".fecha_sql($_POST['fecha3'])."' WHERE cod_expediente_det=$_POST[codigo]";
			}else{
			$consulta="UPDATE nomexpediente SET tipo_registro='$_POST[tipo_registro]', tipo_tiporegistro='$_POST[tipo_tiporegistro]',monto='$_POST[monto]', monto_nuevo='$_POST[monto_nuevo]', dias='$_POST[dias]', fecha='".date("Y-m-d")."', fecha_salida='$fecha_salida', fecha_retorno='$fecha_reintegro', cod_cargo='$_POST[cod_cargo]', cod_cargo_nuevo='$_POST[cod_cargo_nuevo]', usuario='$_SESSION[nombre]', descripcion='$_POST[descripcion]', pagado_por_emp='$_POST[pagado_por_emp]', institucion='$_POST[institucion]', tipo_estudio='$_POST[tipo_estudio]', nivel_actual='$_POST[nivel_actual]', costo_persona='$_POST[costo_persona]', num_participantes='$_POST[num_participantes]', nombre_especialista='$_POST[nombre_especialista]', gerencia_anterior='$_POST[gerencia_anterior]', gerencia_nueva='$_POST[gerencia_nueva]', nomina_anterior='$_POST[nomina_anterior]', nomina_nueva='$_POST[nomina_nueva]', puntaje='$_POST[puntaje]', calificacion='$_POST[calificacion]', labor='$_POST[labor]', institucion_publica='$_POST[institucion_publica]', tcamisa='$_POST[tcamisa]', tchaqueta='$_POST[tchaqueta]', tbata='$_POST[tbata]',tpantalon='$_POST[tpantalon]', tmono='$_POST[tmono]', tzapato='$_POST[tzapato]', desde='$_POST[desde]', hasta='$_POST[hasta]', horas='$_POST[horas]', minutos='$_POST[minutos]', aprobado='$aprobado', enterado='$enterado',resol1='$_POST[resol1]',resol2='$_POST[resol2]',resol3='$_POST[resol3]',dr1='$_POST[dr1]',dr2='$_POST[dr2]',dr3='$_POST[dr3]',fecha1='".fecha_sql($_POST['fecha1'])."',fecha2='".fecha_sql($_POST['fecha2'])."',fecha3='".fecha_sql($_POST['fecha3'])."' WHERE cod_expediente_det=$_POST[codigo]";				
			}
		$resultado=query($consulta,$conexion);

		if($_POST['tipo_registro']=="Documentos")
		{
			$nombre_documento  = (isset($_POST['nombre_documento']))  ? $_POST['nombre_documento']  : NULL;
			$fecha_vencimiento = str_replace('-', '/', $_POST['fecha_vencimiento']);
			$fecha_vencimiento = fecha_sql($_POST['fecha_vencimiento']);

			// Registrar en nomexpediente
			$consulta = "UPDATE nomexpediente SET
					    `tipo_tiporegistro` = '{$nombre_documento}'
					     WHERE cod_expediente_det='{$_POST['codigo']}'";

			$resultado=query($consulta,$conexion);

			$consulta = "UPDATE `nomexpediente_documentos` SET
						 `nombre_documento`  = '{$nombre_documento}',
						 `descripcion`       = '{$_POST['descripcion']}',
						 `fecha_vencimiento` = '{$fecha_vencimiento}'
						 WHERE cod_expediente_det='{$_POST['codigo']}'";
			$resultado=query($consulta,$conexion);
		}

	}
	activar_pagina("expediente_list_ajax.php?cedula=$cedula");
}


	

$editar="";
if(isset($_GET['codigo']))
{
	$modulo="Editar registro de expediente";
	$consulta="SELECT * FROM nomexpediente WHERE cedula='$_GET[cedula]' AND cod_expediente_det='$_GET[codigo]'";
	$resultado=query($consulta,$conexion);
	$fetch33=fetch_array($resultado);
	$editar=1;
}
//<?php echo $_SERVER['PHP_SELF']; 
?>


<FORM name="formulario1" id="formulario1" action="" method="POST" enctype="multipart/form-data">
<?
titulo_mejorada($modulo,"","","");
?>

<table width="100%" border="0">
<BR>
<tr>
<td>
<!--<TD height="25" class="tb-head" align="left"><strong> TIPO DE REGISTRO </strong>-->
<input type="hidden" name="editar" id="editar" value="<? echo $editar;?>">
<input type="hidden" name="cedula" id="cedula" value="<? echo $cedula;?>">
<input type="hidden" name="codigo" id="codigo" value="<? echo $_GET['codigo'];?>">
</TD>
</tr>

<TR class='tb-fila'>
<TD height="50">Tipo:
<select onchange="javascript:cargar_tipo();" name="tipo_registro" id="tipo_registro">
<option value="">Seleccione</option>

<?
if($_SESSION['acce_estuaca']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Estudios Academicos")echo "selected='true'"?> value="Estudios Academicos">Estudios Academicos</option>
<?}
if($_SESSION['acce_xestuaca']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Estudios Extra Academicos")echo "selected='true'"?> value="Estudios Extra Academicos">Estudios Extra Academicos</option>
<?}
if($_SESSION['acce_permisos']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Licencias")echo "selected='true'"?> value="Licencias">Licencias</option>
<?}
if($_SESSION['acce_permisos']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Permisos")echo "selected='true'"?> value="Permisos">Permisos</option>
<?}
if($_SESSION['acce_logros']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Logros")echo "selected='true'"?> value="Logros">Modificaciones</option>
<?}
if($_SESSION['acce_penalizacion']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Penalizaciones")echo "selected='true'"?> value="Penalizaciones">Penalizaciones</option>
<?}
if($_SESSION['acce_movpe']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Movimiento de Personal")echo "selected='true'"?> value="Movimiento de Personal">Movimiento de Personal</option>
<?}
if($_SESSION['acce_evalde']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Evaluacion de desempeño")echo "selected='true'"?> value="Evaluacion de desempeño">Evaluacion de desempeño</option>
<?}
if($_SESSION['acce_experiencia']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Experiencia")echo "selected='true'"?> value="Experiencia">Experiencia</option>
<?}
if($_SESSION['acce_antic']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Antic. prestaciones")echo "selected='true'"?> value="Antic. prestaciones">Antic. prestaciones</option>
<?}
if($_SESSION['acce_uniforme']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Entrega de Uniformes")echo "selected='true'"?> value="Entrega de Uniformes">Entrega de Uniformes</option>
<?}
if($_SESSION['acce_personal']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Vacaciones")echo "selected='true'"?> value="Vacaciones">Vacaciones</option>
<?}
if($_SESSION['acce_personal']==1)
{?>
<option <?if($fetch33['tipo_registro']=="Tiempo Compensatorio")echo "selected='true'"?> value="Tiempo Compensatorio">Tiempo Compensatorio</option>
<?}
?>
<option value="Documentos" <?php echo ($fetch33['tipo_registro']=="Documentos") ? 'selected' : ''; ?>>Documentos</option>

</select>
</td>
</tr>
</table>
<table align="center" width="100%" border="0">
<TR><TD>
<div id="registro">
</div>
</TD></TR>
</table>
<table width="60%" cellspacing="0" border="0" cellpadding="1" align="center">
<tr align="right">
<td align="right">
<?php btn('cancel',"expediente_list_ajax.php?cedula=$cedula",0); ?>
</td>
<td align="left">
<?php btn('ok',"confirmar2('\u00BFSeguro desea registrar estos datos?');",2); ?>
<input type="hidden" name="opcion" id="opcion"/>
</td>
</tr>
</table>
<?php
cerrar_conexion($conexion);
?>
</FORM>
</BODY>
</html>
<?php
if($editar==1)
{
	?>
	<script type="text/javascript">
	cargar_tipo()
	</script>
	<?php
}
?>
