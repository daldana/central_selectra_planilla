<?php 
session_start();
ob_start();

require_once '../lib/common.php';
include ("../paginas/funciones_nomina.php");
include ("../paginas/func_bd.php");
// error_reporting(0);
$conexion=conexion();

$registro_id=$_POST['registro_id'];	
$op=$_POST['op'];

if ($op==3) //Se presiono el boton de Eliminar
{	
	$consulta = "SELECT * 
	             FROM   nomprestamos_detalles 
	             WHERE  numpre={$registro_id} AND estadopre='Cancelada'"; 

	$resultadoCon = query($consulta, $conexion); //  sql_ejecutar($query);

	if( num_rows($resultadoCon) > 0 )
		$estado = 'Anulada con PAGOS';
	else
		$estado = 'Anulada';
	
	$consulta = "UPDATE nomprestamos_cabecera SET 
	             estadopre = '$estado', 
	             usuarioanu = '{$_SESSION["nombre"]}', 
	             fechaanu = '".date('Y-m-d h:i:s')."' 
	             WHERE numpre = {$registro_id}";

	$resultado = query($consulta, $conexion);
		
	$consulta = "UPDATE nomprestamos_detalles SET 
	             estadopre='Anulada' 
	             WHERE numpre={$registro_id} AND estadopre='Pendiente'";

	$resultado = query($consulta, $conexion);	

	activar_pagina("prestamos_list.php");	 
}   

$sql = "SELECT nc.ficha, np.nomposicion_id, np.apenom, nc.monto, nc.codigopr, 
               nc.estadopre, npp.descrip, nc.numpre 
        FROM   nomprestamos_cabecera nc 
        JOIN   nompersonal np ON (np.ficha=nc.ficha) 
        JOIN   nomprestamos npp ON (npp.codigopr=nc.codigopr)
        WHERE  np.tipnom='{$_SESSION['codigo_nomina']}' 
        ORDER BY np.ficha, nc.codigopr";
$res = query($sql, $conexion);
?>
<?php include("../header4.php"); // <html><head></head><body> ?>
<link href="../../includes/assets/css/custom-datatables.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
  .portlet > .portlet-title > .actions > .btn.btn-sm {
    margin-top: -9px !important;
  }
</style>
<script type="text/javascript">
function enviar(op,id)
{
	if (op==3)
	{		// Opcion cancelar préstamo
		if (confirm("\u00BFEst\u00E1 seguro que desea finalizar este pr\u00E9stamo?"))
		{					
			document.frmPrincipal.registro_id.value=id;
			document.frmPrincipal.op.value=op;
  			document.frmPrincipal.submit();
		}		
	}
}
</script>
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
							  <img src="../imagenes/21.png" width="20" height="20" class="icon"> Pr&eacute;stamos
							</div>
							<div class="actions">
								<a class="btn btn-sm blue"  onclick="javascript: window.location='prestamos_agregar.php'">
									<i class="fa fa-plus"></i>
									Agregar
								</a>
								<a class="btn btn-sm blue"  onclick="javascript: window.location='menu_prestamos.php'">
									<i class="fa fa-arrow-left"></i> Regresar
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<form action="" method="post" name="frmPrincipal" id="frmPrincipal">
							<table class="table table-striped table-bordered table-hover" id="table_datatable">
							<thead>
							<tr>
								<th>Prest.</th>
								<th># Colab.</th>
								<th>Posicion</th>
								<th>Nombres y Apellidos</th>
								<th>Monto</th>
								<th>Tipo</th>
								<th>Estado</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
								<th>&nbsp;</th>
							</tr>
							</thead>
							<tbody>
							<?php								
								while( $fila=fetch_array($res) )
								{ 	$codigo=$fila['numpre'];
								?>
									<tr class="odd gradeX">
										<td><?php echo $fila['numpre']; ?></td>
										<td><?php echo $fila['ficha']; ?></td>
										<td><?php echo $fila['nomposicion_id']; ?></td>
										<td><?php echo $fila['apenom']; ?></td>
										<td><?php echo number_format($fila['monto'], 2, ',', '.'); ?></td>
										<td><?php echo $fila['descrip']; ?></td>
										<td><?php echo $fila['estadopre']; ?></td>
										<td style="text-align: center; width: 22px !important">
									      <a href="prestamos_ver.php?numpre=<?php echo $codigo; ?>" title="Ver">
									      <img src="../imagenes/view.gif" width="16" height="16"></a>
										</td>
										<?php
											if($fila['estadopre']=='Pendiente')
											{
											?>
												<td style="text-align: center; width: 22px !important">
											       <a href="prestamos_edit.php?numpre=<?php echo $codigo; ?>" title="Editar">
											       <img src="../../includes/imagenes/icons/pencil.png" width="16" height="16"></a>
												</td>
												<td style="text-align: center; width: 22px !important">
												    <a href="javascript:enviar(<?php echo(3); ?>,<?php echo $codigo; ?>);" title="Cancelar">
												    <img src="../imagenes/29.png" alt="Cancelar" width="16" height="16"></a>
							                    </td>
												<td style="text-align: center; width: 22px !important">
												    <a href="../tcpdf/reporte_prestamo.php?numpre=<?php echo $codigo; ?>" title="Reporte">
												    <img src="../imagenes/pdf.png" alt="Reporte" width="16" height="16"></a>
							                    </td>
											<?php
											}
											else
											{
											?>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											<?php
											}
										?>
									</tr>
								  <?php							
								}
							?>
							</tbody>
							</table>
							    <input name="registro_id" type="hidden" value="">
							    <input name="op" type="hidden" value="">	
							</form>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<?php include("../footer4.php"); ?>
<script type="text/javascript">
	 $(document).ready(function() { 

            $('#table_datatable').DataTable({
            	"iDisplayLength": 25,
                //"sPaginationType": "bootstrap",
            	"sPaginationType": "bootstrap_extended", 
            	"aaSorting": [[ 1, "asc" ]], 
                "oLanguage": {
                	"sSearch": "<img src='../../includes/imagenes/icons/magnifier.png' width='16' height='16' > Buscar:",
                    "sLengthMenu": "Mostrar _MENU_",
                    //"sInfo": "Showing page _PAGE_ of _PAGES_", // Mostrando 1 to 5 de 18 entradas
                    //"sInfoEmpty": "No hay registros para mostrar",
                    "sInfoEmpty": "",
                    //"sInfo": "",
                    "sInfo":"Total _TOTAL_ registros",
                    "sInfoFiltered": "",
          		    "sEmptyTable":  "No hay datos disponibles", // No hay datos para mostrar
                    "sZeroRecords": "No se encontraron registros",
                    "oPaginate": {
                        "sPrevious": "P&aacute;gina Anterior",//"Prev",
                        "sNext": "P&aacute;gina Siguiente",//"Next",
                        "sPage": "P&aacute;gina",//"Page",
                        "sPageOf": "de",//"of"
                    }
                },
                "aLengthMenu": [ // set available records per page
                    [5, 10, 25, 50,  -1],
                    [5, 10, 25, 50, "Todos"]
                ],                
                "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [6] },
                    { "bSearchable": false, "aTargets": [6] },
                   // { "sWidth": "8%", "aTargets": [6] },
                    { 'bSortable': false, 'aTargets': [7] },
                    { "bSearchable": false, "aTargets": [7] },
                   // { "sWidth": "8%", "aTargets": [7] },
                    { 'bSortable': false, 'aTargets': [8] },
                    { "bSearchable": false, "aTargets": [8] },
                   // { "sWidth": "8%", "aTargets": [8] },
                    { 'bSortable': false, 'aTargets': [9] },
                    { "bSearchable": false, "aTargets": [9] },
                   // { "sWidth": "8%", "aTargets": [9] },
                ],
				 "fnDrawCallback": function() {
				        $('#table_datatable_filter input').attr("placeholder", "Escriba frase para buscar");
				 }
            });

            $('#table_datatable').on('change', 'tbody tr .checkboxes', function(){
                 $(this).parents('tr').toggleClass("active");
            });

            $('#table_datatable_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); 
            $('#table_datatable_wrapper .dataTables_length select').addClass("form-control input-xsmall"); 
	 });
</script>
</body>
</html>