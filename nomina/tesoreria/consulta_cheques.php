<?php
$url="consulta_cheques";
$modulo="Consulta de Cheques";
$tabla="nomcheques";
$titulos=array("Status","Cheque","Beneficiario","C.I. / R.I.F","Fecha","Monto","Chequera");
$indices=array("1","2","3","5","6","4","9");
$filtro="Ac";
session_start();
ob_start();

require_once '../lib/config.php';
require_once '../lib/common.php';
include ('../header.php');

$conexion=conexion();

$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$pagina=@$_GET['pagina'];
$aa=$_GET['busqueda'];
//$des=$_GET['buscar'];

if(isset($_POST['buscar']) || $tipob!=NULL){
	if(!$tipob){
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
		$aa=$_POST['sel_busqueda'];
	}
	switch($tipob){
		case "exacta": 
			//$aa=$_POST['sel_busqueda'];
			$consulta=buscar_exacta($tabla,$des,$aa);
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,$_POST['sel_busqueda']);
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,$_POST['sel_busqueda']);
			break;
	}
	//$consulta=$consulta." where status='".$filtro."'";
$consulta.=" and status<>'A' and status<>'D' order by chequera,cheque";
}else{

$consulta="select * from ".$tabla;//." where status='".$filtro."'";

$consulta.=" where status <> 'A' and status <> 'D'  order by chequera,cheque";
}

//echo $consulta." este es el valor quemuestra ";
$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);
?>

<SCRIPT language="JavaScript" type="text/javascript" src="mostrar_cuentas.js"></SCRIPT>
<SCRIPT language="JavaScript" type="text/javascript" src="mostrar_impresion.js"></SCRIPT>
<FORM name="<?echo $url?>" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
	titulo($modulo,"","../paginas/menu_int.php?cod=282","85");
	$array=array("Cheque","Orden","beneficiario");
	busqueda($url,"",$array);
?>

<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
  <tbody>
    <tr class="tb-head" >
<?
foreach($titulos as $nombre){
	echo "<td><STRONG>$nombre</STRONG></td>";
}
?>
      <td></td>
	<td></td>
	<td></td>
	<td></td>
    </tr>

<? 
	
	if($num_paginas!=0){
	$i=0; 
	while($fila=mysqli_fetch_array($resultado)){
   	$i++;
	if($i%2==0){
?>
    	<tr class="tb-fila" onclick="javascript:cuentas()">
<?
	}else{
		echo "<tr>";
	}
	foreach($indices as $campo){
		$nom_tabla=mysqli_fetch_field_direct($resultado, $campo)->name;
		$var=$fila[$nom_tabla];
		if ($nom_tabla=="fecha"){
			if($fila['status']=='An'|| $fila['status']=='Da'){
				//echo "<td>".fecha($fila['anulacion'])."</td>";
				echo "<td>".$fila['anulacion']."</td>";
			}else{
				//echo "<td>".fecha($var)."</td>";
				echo "<td>".$var."</td>";
			}
		}elseif ($nom_tabla=="monto"){
			echo "<td align=right>".number_format($var, 2, ',', '.')."</td>";
			
		}
		else{
			echo "<td title='".$fila['concepto']."'>$var</td>";
		}
	}
	$codigo=$fila['cheque'];
	
	$chequera=$fila['chequera'];
	

	/*icono("javascript:cuentas('codigo=".$codigo."&banco=".$banco."&chequera=".$chequera."&cuenta=".$cuenta."&pagina=".$pagina."')", "Mostrar", "view.gif");
	if($fila['status']=='Im')
		icono("javascript:impresion('codigo=".$codigo."&banco=".$banco."&chequera=".$chequera."&cuenta=".$cuenta."')", "Menu de Impresion", "imprimir.png");
	else
		echo "<td></td>";
	icono("javascript:conceptos('codigo=".$codigo."&banco=".$banco."&chequera=".$chequera."&cuenta=".$cuenta."')", "Mostrar concepto", "view.gif");
	*/
	

    echo "</tr>";
	}
}else{
	echo "<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
//$codigo=0;

//cerrar_conexion($conexion);

?>
<script language="JavaScript" type="text/javascript">
function mostrar_paginas(num_paginas){

}
</script>
  </tbody>
</table>

<?php
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des."&busqueda=".$aa,$num_paginas);
?>
<br>
<table width="100%">
<TBODY>
<TR><TD id="contenido_concepto"> </TD></TR>
<TR><TD id="contenido_cuentas"> </TD></TR>
</TBODY>
</table>
</FORM>
</BODY>
</html>