<?php
$url="seleccionar_chequera";
$modulo="terceros";
$tabla="nomchequera";

require_once '../lib/config.php';
require_once '../lib/common.php';

$conexion=conexion();

$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$codigo_banco=@$_GET['codigo'];
$pagina=@$_GET['pagina'];
$entrada=$_GET[entrada];

if(isset($_POST['buscar']) || $tipob!=NULL){
	if(!$tipob){
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
		$codigo_banco=$_POST['banco'];
	}
	
	switch($tipob){
		case "exacta": 
			$consulta=buscar_exacta($tabla,$des,"numero");
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,"numero");
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,"numero");
			break;
	}
	$consulta=$consulta." AND banco='".$codigo_banco."' and situacion='A";

}else{

$consulta="select * from ".$tabla." where banco='".$codigo_banco."' and situacion='A'";

}
$consulta_banco="select * from nombancos where cod_ban='".$codigo_banco."'";
$resultado_banco=mysqli_query($conexion, $consulta_banco) or die("No se puede obtener el banco");
$banco_fila=mysqli_fetch_array($resultado_banco);
$cuenta= $banco_fila['cuentacob'];
$descripcion=$banco_fila['des_ban'];



$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);

?>
<html class="fondo">
<HEAD>

<link href="../estilos.css" rel="stylesheet" type="text/css">
<TITLE></TITLE>
<SCRIPT language="JavaScript" type="text/javascript" src="transaccion.js">

</SCRIPT>
</HEAD>

<BODY>
<FORM name="<?echo $url?>" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
	titulo($descripcion.", Nro_cuenta: ".$cuenta.". Seleccione una Chequera" ,"", "seleccionar_banco.php?pagina=1&siguiente=seleccionar_chequera","cheque");
?>
<table class="tb-head" width="100%">
  <tr>
	<td><input type="text" name="buscar" size="20"></td>
	<td><? btn('search',$url,1); ?></td>
	<td><? btn('show_all',$url.".php?pagina=".$pagina."&codigo=".$codigo_banco); ?></td>
	<td width="120"><input onclick="javascript:actualizar(this);" checked="true" type="radio" name="palabra" value="exacta">Palabra exacta</td>
	<td width="140"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="todas">Todas las palabras</td>
	<td width="150"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="cualquiera">Cualquier palabra</td>
	<td colspan="3" width="386"></td>
<INPUT type="hidden" name="banco" value="<?echo $codigo_banco?>">
  </tr>
</table>
<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
  <tbody>
    <tr class="tb-head" >
      <td><STRONG>N&uacute;mero</STRONG></td>
      <td><STRONG>Cantidad</STRONG></td>
      <td><STRONG>Situacion</STRONG></td>
      <td><STRONG>Inicio</STRONG></td>
      <td><STRONG>Fin</STRONG></td>
      <td></td>
    </tr>

<? 
	
	if($num_paginas!=0){
	$i=0; 
	while($fila=mysqli_fetch_array($resultado)){
   	$i++;
	if($i%2==0){
?>
    		<tr class="tb-fila">
<?
	}else{
		echo"<tr>";
	}
	$numero=$fila["chequera_id"];
	$cantidad=$fila["cantidad"];
	$inicio=$fila["inicio"];
	$situacion=$fila["situacion"];
	$final=($inicio+$cantidad)-1;

	echo"<td>$numero</td>";
	echo"<td>$cantidad</td>";
	switch($situacion){
		case 'D':
			echo"<td>Deposito</td>";
			break;
		case 'A':
			echo"<td>Activa</td>";
			break;
		case 'C':
			echo"<td>Consumida</td>";
			break;
		default:
			echo"<td>Ninguna</td>";
			break;
	}
	echo"<td>$inicio</td>";
	echo"<td>$final</td>";

	if($entrada==1)
		echo "<td><a href=\"seleccionar_nomina.php?codigo=$numero&banco=$codigo_banco\"><IMG title=\"Seleccionar Nomina\" src=\"../imagenes/add.gif\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";	
	else
		echo "<td><a href=\"cheques_trabajador.php?codigo=$numero&banco=$codigo_banco\"><IMG title=\"Agregar Orden\" src=\"../imagenes/add.gif\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";


    echo"</tr>";
	}
}else{
	echo"<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
cerrar_conexion($conexion);

?>
<script language="JavaScript" type="text/javascript">
function mostrar_paginas(num_paginas){

}
</script>
  </tbody>
</table>
<?php
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des,$num_paginas);
?>
</FORM>
</BODY>
</html>



