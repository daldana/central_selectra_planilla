<?php
session_start();
ob_start();

$url="seleccionar_banco";
$modulo="seleccionar_banco";
$tabla="nombancos";

require_once '../lib/config.php';
require_once '../lib/common.php';


/*$conexion5=conexion_conf();
$consulta5 = "SELECT sobregirof FROM parametros WHERE codigo = '1'";
$resultado3 = query($consulta5,$conexion5);
$fetch2=fetch_array($resultado3);
//echo $fetch2['sobregirof'];
cerrar_conexion($conexion5);*/


$conexion=conexion();
$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$pagina=@$_GET['pagina'];
$siguiente=@$_GET['siguiente'];

$entrada=$_GET[entrada];

if(isset($_POST['buscar']) || $tipob!=NULL){
	if(!$tipob){
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
		$siguiente=$_POST['sig'];
	}
	
	switch($tipob){
		case "exacta": 
			$consulta=buscar_exacta($tabla,$des,"descripcion");
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,"descripcion");
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,"descripcion");
			break;
	}

}else{

$consulta="select * from ".$tabla;

}

$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);
?>
<html class="fondo">
<HEAD>

<link href="../estilos.css" rel="stylesheet" type="text/css">
<TITLE></TITLE>
<SCRIPT language="JavaScript" type="text/javascript" src="transaccion.js">

</SCRIPT>
</HEAD>

<BODY>
<FORM name="<?echo $url?>" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
	titulo("Seleccionar Banco","","../paginas/menu_int.php?cod=282","55");
?>
<table class="tb-head" width="100%">
  <tr>
	<td><input type="text" name="buscar" size="20"></td>
	<td><? btn('search',$url,1); ?></td>
	<td><? btn('show_all',$url.".php?pagina=".$pagina); ?></td>
	<td width="120"><input onclick="javascript:actualizar(this);" checked="true" type="radio" name="palabra" value="exacta">Palabra exacta</td>
	<td width="140"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="todas">Todas las palabras</td>
	<td width="150"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="cualquiera">Cualquier palabra</td>
	<td colspan="3" width="386"></td>
	<INPUT type="hidden" name="sig" value="<?echo $siguiente?>">
  </tr>
</table>
<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
  <tbody>
    <tr class="tb-head" >
      <td><STRONG>C&oacute;digo</STRONG></td>
      <td><STRONG>Nombre del Banco</STRONG></td>
      <td><STRONG>Nro de Cuenta</STRONG></td>
      <td><STRONG>Tipo</STRONG></td>
      <td></td>
    </tr>

<? 
	
	if($num_paginas!=0){
	$i=0; 
	while($fila=mysqli_fetch_array($resultado)){
   	$i++;
	if($i%2==0){
?>
    		<tr class="tb-fila">
<?
	}else{
		echo"<tr>";
	}
	$codigo=$fila["cod_ban"];
	$descripcion=$fila["des_ban"];
	$cuenta=$fila["cuentacob"];
	$tipo=$fila["tipocuenta"];
	$cuenta_contable=$fila["cuenta_contable"];
	echo"<td>$codigo</td>";
	echo"<td>$descripcion</td>";
	echo"<td>$cuenta</td>";
	echo"<td>$tipo</td>";
	if(($fila["saldo"]<=0)&&($fetch2['sobregirof']=='N'))
	{
      echo "<td><IMG title=\"No tiene fondos\" src=\"../imagenes/ico_cancel.gif\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></td>";
}else{
	if($_SESSION[CONSULTA]==1){
		echo "<td></td>";
	}ELSE{
      echo "<td><a href=\"seleccionar_chequera.php?pagina=1&codigo=$codigo&entrada=$entrada\"><IMG title=\"Agregar Banco\" src=\"../imagenes/add.gif\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
	 }

}
    echo"</tr>";
	}
}else{
	echo"<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
cerrar_conexion($conexion);

?>
<script language="JavaScript" type="text/javascript">
function mostrar_paginas(num_paginas){

}
</script>
  </tbody>
</table>
<?php
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des,$num_paginas);
?>
</FORM>
</BODY>
</html>



