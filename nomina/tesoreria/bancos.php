<?php
session_start();
ob_start();

$url="bancos";
$modulo="Bancos";
$tabla="nombancos";
$titulos=array("Código","Nombre","Nro Cuenta","Tipo","Nro Cuenta Presupuestaria");
$indices=array("0","1","2","3","5");

//DECLARACION DE LIBRERIAS
require_once '../lib/config.php';
require_once '../lib/common.php';


$conexion=conexion();
$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$pagina=@$_GET['pagina'];

if(isset($_POST['buscar']) || $tipob!=NULL){
	if(!$tipob){
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
	}
	
	switch($tipob){
		case "exacta": 
			$consulta=buscar_exacta($tabla,$des,"descripcion");
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,"descripcion");
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,"descripcion");
			break;
	}

}else{

$consulta="select * from ".$tabla;

}

$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);
include ("../header.php");
?>
<html class="fondo">
<HEAD>

<link href="../estilos.css" rel="stylesheet" type="text/css">
<TITLE></TITLE>
<SCRIPT language="JavaScript" type="text/javascript" src="transaccion.js">

</SCRIPT>
</HEAD>

<BODY>
<FORM name="<?echo $url?>" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
	if($_SESSION[CONSULTA]==1){
		titulo($modulo,"","../menu_int.php?cod=5","55");
	}ELSE{

		titulo($modulo,"bancos_agregar.php","../paginas/menu_int.php?cod=282","55");
	}
?>
<table class="tb-head" width="100%">
  <tr>
	<td><input type="text" name="buscar" size="20"></td>
	<td><? btn('search',$url,1); ?></td>
	<td><? btn('show_all',$url.".php?pagina=".$pagina); ?></td>
	<td width="120"><input onclick="javascript:actualizar(this);" checked="true" type="radio" name="palabra" value="exacta">Palabra exacta</td>
	<td width="140"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="todas">Todas las palabras</td>
	<td width="150"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="cualquiera">Cualquier palabra</td>
	<td colspan="3" width="386"></td>
  </tr>
</table>
<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
  <tbody>
    <tr class="tb-head" >
<? 
foreach($titulos as $nombre){
	echo "<td><STRONG>$nombre</STRONG></td>";
}
?>
      <td></td>
      <td></td>
      <td></td>
      <td></td>

    </tr>
<? 
	
	if($num_paginas!=0){
	$i=0; 
	while($fila=mysqli_fetch_array($resultado)){
   	$i++;
	if($i%2==0){
?>
    		<tr class="tb-fila">
<?
	}else{
		echo"<tr>";
	}
	foreach($indices as $campo){
		$nom_tabla=mysqli_fetch_field_direct($resultado, $campo)->name;
		
		$var=$fila[$nom_tabla];
		echo"<td>$var</td>";
	}
	$codigo=$fila["cod_ban"];
	//$descripcion=$fila["descripcion"];
	//$cuenta=$fila["cuenta"];
	//$tipo=$fila["tipo"];
	//$cuenta_contable=$fila["cuenta_contable"];
	if($_SESSION[CONSULTA]==1){
				   echo "<td></td><td></td>";
	}ELSE{

		icono("bancos_modificar.php?codigo=".$codigo, "Editar", "edit.gif");
		icono("bancos_eliminar.php?codigo=".$codigo, "Eliminar", "delete.gif");
	}
	//icono("tsaldos.php?codigo=".$codigo, "Saldos Mensuales", "generar_cheques.png");
	icono("chequeras.php?codigo=".$codigo, "Chequeras", "chequera.png");
	
    echo"</tr>";
	}
}else{
	echo"<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
cerrar_conexion($conexion);

?>
<script language="JavaScript" type="text/javascript">
function mostrar_paginas(num_paginas){

}
</script>
  </tbody>
</table>
<?
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des,$num_paginas);
?>
</FORM>
</BODY>
</html>



