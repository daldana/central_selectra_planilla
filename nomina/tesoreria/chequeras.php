<?php
session_start();
ob_start();

$url="chequeras";
$modulo="chequeras";
$tabla="nomchequera";

require_once '../lib/config.php';
require_once '../lib/common.php';
include('../header.php');

$conexion=conexion();

$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$codigo_banco=@$_GET['codigo'];
$pagina=@$_GET['pagina'];
$banco=$_GET['banco'];
$chequera=$_GET['chequera'];
$accion = $_GET['accion'];

if(isset($_POST['buscar']) || $tipob!=NULL){
	if(!$tipob){
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
		$codigo_banco=$_POST['banco'];
	}
	
	switch($tipob){
		case "exacta": 
			$consulta=buscar_exacta($tabla,$des,"chequera_id");
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,"chequera_id");
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,"chequera_id");
			break;
	}
	$consulta=$consulta." AND banco='".$codigo_banco."'";

}else{

	if($accion=='borrar_chequera')
	{
		$var_sql="delete from nomchequera WHERE banco=".$banco." and numero=".$chequera;
		$rs = query($var_sql,$conexion);
	}
	
	$consulta="select * from ".$tabla." where banco='".$codigo_banco."'";
}

$consulta_banco="select * from nombancos where cod_ban='".$codigo_banco."'";
$resultado_banco=query($consulta_banco, $conexion);
$banco_fila=fetch_array($resultado_banco);
$cuenta= $banco_fila['cuentacob'];
$descripcion=$banco_fila['des_ban'];

$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);

?>
<html class="fondo">
<HEAD>

<link href="../estilos.css" rel="stylesheet" type="text/css">
<TITLE></TITLE>
<SCRIPT language="JavaScript" type="text/javascript" src="transaccion.js">

</SCRIPT>
</HEAD>

<BODY>
<FORM name="<?echo $url?>" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
	if($_SESSION[CONSULTA]==1){
		titulo($descripcion.", Nro. Cuenta: ".$cuenta.". Relaci&oacute;n de Chequeras" ,"" , "bancos.php?pagina=1","cheque");
	}ELSE{

		titulo($descripcion.", Nro. Cuenta: ".$cuenta.". Relaci&oacute;n de Chequeras" ,$modulo."_agregar.php?banco=".$codigo_banco , "bancos.php?pagina=1","cheque");
	}
?>
<table class="tb-head" width="100%">
  <tr>
	<td><input type="text" name="buscar" size="20"></td>
	<td><? btn('search',$url,1); ?></td>
	<td><? btn('show_all',$url.".php?pagina=".$pagina."&codigo=".$codigo_banco); ?></td>
	<td width="120"><input onclick="javascript:actualizar(this);" checked="true" type="radio" name="palabra" value="exacta">Palabra exacta</td>
	<td width="140"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="todas">Todas las palabras</td>
	<td width="150"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="cualquiera">Cualquier palabra</td>
	<td colspan="3" width="386"></td>
	<INPUT type="hidden" name="banco" value="<?echo $codigo_banco?>">
  </tr>
</table>
<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
  <tbody>
    <tr class="tb-head" >
      <td><STRONG>N&uacute;mero</STRONG></td>
      <td><STRONG>Cantidad</STRONG></td>
      <td><STRONG>Situaci&oacute;n</STRONG></td>
      <td><STRONG>Inicio</STRONG></td>
      <td><STRONG>Fin</STRONG></td>
      <td></td>
<td></td><td></td><td></td><td></td><td></td><td></td>
    </tr>

<? 
	
	if($num_paginas!=0){
	$i=0; 
	while($fila=mysqli_fetch_array($resultado)){
   	$i++;
	if($i%2==0){
?>
    		<tr class="tb-fila">
<?
	}else{
		echo "<tr>";
	}
        



	$numero=$fila["chequera_id"];
	$cantidad=$fila["cantidad"];
	$inicio=$fila["inicio"];
	$situacion=$fila["situacion"];
	$final=($inicio+$cantidad)-1;

	echo "<td>$numero</td>";
	echo "<td>$cantidad</td>";
	switch($situacion){
		case 'D':
			echo "<td>Dep&oacute;sito</td>";
			break;
		case 'A':
			echo "<td>Activa</td>";
			break;
		case 'C':
			echo "<td>Consumida</td>";
			break;
		default:
			echo "<td>Ninguna</td>";
			break;
	}
	echo "<td>$inicio</td>";
	echo "<td>$final</td>";
	$editar = $modulo."_modificar";
	$eliminar = $modulo."_eliminar";
	
	$status = $fila['situacion'];

	$conCheques = "SELECT * FROM nomcheques WHERE  chequera='".$numero."'";
	//echo $conCheques."<br>";
	$resCheques = query($conCheques, $conexion);
	$registros = num_rows($resCheques);
	//echo $registros;
	if($_SESSION[CONSULTA]==1){
				   echo "<td></td><td></td>";
	}ELSE{

		if ($registros==0){
			icono("chequeras_modificar.php?codigo=".$numero."&banco=".$codigo_banco, "Editar", "edit.gif");
		}else{
			?>
			<td><img width="16" height="16" align="left" border="0" title="No puede Editar" src="../imagenes/ico_est6.gif"/></td>
			<?
		}
		if ($registros==0){
			icono("opcion_chequera.php?opcion=0&inicio=".$inicio."&fin=".$final."&status=".$situacion."& banco=".$codigo_banco."&chequera=".$numero, "Generar Cheques", "generar_cheques.png");
		}else{
			?>
			<td><img width="16" height="16" align="left" border="0" title="No puede Generar" src="../imagenes/ico_est6.gif"/></td>
			<?
		}
	}
	icono("cheques.php?pagina=1&banco=".$codigo_banco."&chequera=".$numero."&status=".$situacion, "Ver Cheques", "view.gif");

	if($_SESSION[CONSULTA]==1){
				   echo "<td></td><td></td><td></td>";
	}ELSE{

		if (($registros>0)&&($situacion=="D")){
			icono("opcion_chequera.php?opcion=1&banco=".$codigo_banco."&chequera=".$numero."&valor=A", "Activar", "activar.png");
		}else{
			?>
			<td><img width="16" height="16" align="left" border="0" title="No puede Activar" src="../imagenes/ico_est6.gif"/></td>
			<?
		}
		if (($registros==0)||($situacion=="A")){
			icono("opcion_chequera.php?opcion=1&banco=".$codigo_banco."&chequera=".$numero."&valor=C", "Consumir", "consumir.png");
		}else{
			?>
			<td><img width="16" height="16" align="left" border="0" title="No puede Consumir" src="../imagenes/ico_est6.gif"/></td>
			<?
		}
		if (($registros==0)||($situacion=="A")){
			icono("opcion_chequera.php?opcion=1&banco=".$codigo_banco."&chequera=".$numero."&valor=D", "Depósito", "deposito.png");
		}else{
			?>
			<td><img width="16" height="16" align="left" border="0" title="No puede Depositar" src="../imagenes/ico_est6.gif"/></td>
			<?
		}
		if ($registros==0){
			icono("chequeras.php?accion=borrar_chequera&banco=".$codigo_banco."&chequera=".$numero, "Borrar Chequera", "delete.gif");
		}else{
			?>
			<td><img width="16" height="16" align="left" border="0" title="No puede Borrar" src="../imagenes/ico_est6.gif"/></td>
			<?
		}
	}
   echo "</tr>";
	}
}else{
	echo "<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
cerrar_conexion($conexion);

?>
<script language="JavaScript" type="text/javascript">
function mostrar_paginas(num_paginas){

}
</script>
  </tbody>
</table>
<?
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des."&codigo=".$codigo_banco,$num_paginas);
?>
</FORM>
</BODY>
</html>