<?php
session_start();
ob_start();

//Si realizas otra modificacion colocarla arriba para hacer un historico de modificaciones
//Fecha Ultima Modificacion: 19-02-2008 Hora: 12:09 pm
//Realizada por Topo en el aeropuerto
//Modificacion: Estados de los Cheques

$url="cheques";
$modulo="cheques";
$tabla="nomcheques";

require_once '../lib/config.php';
require_once '../lib/common.php';
include('../header.php');

$conexion=conexion();

$tipob=@$_GET['tipo'];
$des=@$_GET['des'];
$pagina=@$_GET['pagina'];
$codigo_banco=@$_GET['banco'];
$chequera=@$_GET['chequera'];
$situacion=@$_GET['status'];
$rsac=@$_GET['rsac'];
$opcion=@$_GET['opcion'];
$cheque = @$_GET['cheque'];
$odp = @$_GET['odp'];
$monto = @$_GET['monto'];

function fecha($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY
  if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4);
  }
if(isset($_POST['buscar']) || $tipob!=NULL){
	if(!$tipob){
		$tipob=$_POST['palabra'];
		$des=$_POST['buscar'];
		$codigo_banco=$_POST['banco'];
		$chequera=$_POST['chequera'];
	}
	
	switch($tipob){
		case "exacta": 
			$consulta=buscar_exacta($tabla,$des,"cheque");
			break;
		case "todas":
			$consulta=buscar_todas($tabla,$des,"cheque");
			break;
		case "cualquiera":
			$consulta=buscar_cualquiera($tabla,$des,"cheque");
			break;
	}
	$consulta=$consulta." AND banco='".$codigo_banco."' AND chequera='".$chequera."'";
}else{
	if ($rsac == '1') 
	{
		?>
		<script type="text/javascript">	
			window.open('observaciones.php?opcion=<?php echo $opcion;?>&banco=<?php echo $codigo_banco;?>&chequera=<?php echo $chequera;?>&cheque=<?php echo $cheque;?>&monto=<?php echo $monto;?>&valor=D&status=<?php echo $status;?>&odp=<?php echo $odp;?>','Enviada a Administración/Regresar','width=420, height=120')
			</script>
		<?
	}
	$consulta="select * from ".$tabla." where chequera='".$chequera."' ORDER BY cheque";
}

$consulta_banco="select * from nombancos where cod_ban='".$codigo_banco."'";
$resultado_banco=mysqli_query($conexion,$consulta_banco) or die("No se puede obtener el banco");
$banco_fila=mysqli_fetch_array($resultado_banco);
$cuenta= $banco_fila['cuentacob'];
$descripcion=$banco_fila['des_ban'];

$consultaCheque = "select * from nomcheques where  chequera='".$chequera."' AND status='A' ORDER BY cheque";
$resultadoCheque = query($consultaCheque, $conexion);

$num_paginas=obtener_num_paginas($consulta);
$pagina=obtener_pagina_actual($pagina, $num_paginas);
$resultado=paginacion($pagina, $consulta);

?>

<FORM name="tcheques" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_self">
<?
	switch($situacion){
		case 'D':
			$sit="Deposito";
			break;
		case 'A':
			$sit="Activa";
			break;
		case 'C':
			$sit="Consumida";
			break;
		case 'D';
	}

	titulo($descripcion.", Nro. Cuenta: ".$cuenta.", Chequera Nro.: ".$chequera.", Situacion: (".$sit."). Relaci&oacute;n de Cheques" ,"", "chequeras.php?pagina=1&codigo=".$codigo_banco,"cheque");
?>
<table class="tb-head" width="100%">
  <tr>
	<td><input type="text" name="buscar" size="20"></td>
	<td><? btn('search',$url,1); ?></td>
	<td><? btn('show_all',$url.".php?pagina=".$pagina."&banco=".$codigo_banco."&chequera=".$chequera); ?></td>
	<td width="120"><input onclick="javascript:actualizar(this);" checked="true" type="radio" name="palabra" value="exacta">Palabra exacta</td>
	<td width="140"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="todas">Todas las palabras</td>
	<td width="150"><input onclick="javascript:actualizar(this);" type="radio" name="palabra" value="cualquiera">Cualquier palabra</td>
	<td colspan="3" width="386"></td>
	<INPUT type="hidden" name="banco" value="<?echo $codigo_banco?>">
<INPUT type="hidden" name="chequera" value="<?echo $chequera?>">	
  </tr>
</table>
<BR>
<table width="100%" cellspacing="0" border="0" cellpadding="1" align="center">
  <tbody>
    <tr class="tb-head" >
      <td width="5%"><STRONG>Situaci&oacute;n</STRONG></td>
      <td width="10%"><STRONG>N&uacute;mero</STRONG></td>
      <td width="30%"><STRONG>Beneficiario</STRONG></td>
      <td width="15%" align="right"><STRONG>Monto</STRONG></td>
		<td width="10%" align="center	"><STRONG>Fecha Cheque</STRONG></td>
		<td width="10%" align="center"><STRONG>Fecha Anulaci&oacute;n</STRONG></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
    </tr>
<?
	if($num_paginas!=0){
	$w=0; 
	while($fila=mysqli_fetch_array($resultado)){
   	
	if($w%2==0){
?>
    		<tr class="tb-fila">
<?
	}else{
		echo "<tr>";
	}
	$w++;
	$status=$fila['status'];
	$numero=$fila['cheque'];
	$chequex=$fila['cheque'];
	$beneficiario=$fila['beneficiario'];
	$monto=$fila['monto'];
	$fecha_cheque=$fila['fecha'];
	$fecha_anulacion=$fila['anulacion'];
	switch($status){
		case 'D':
			echo"<td>D</td>";
			break;
		case 'A':
			echo"<td>A</td>";
			break;
		case 'Ac':
			echo"<td>Ac</td>";
			break;
		case 'Im':
			echo"<td>Im</td>";
			break;
		case 'En':
			echo"<td>En</td>";
			break;
		case 'Da':
			echo"<td>Da</td>";
			break;
		case 'An':
			echo"<td>An</td>";
			break;
		default:
			echo"<td>Ninguna</td>";
			break;
	}
	$k=strlen($numero);
	if($k<8)
	{
		for($i=$k;$i<=8;$i++)
		{
			$numero="0".$numero;
		}
	}
	echo "<td>$numero</td>";
	
	echo "<td>$beneficiario</td>";
	echo "<td align=right>".number_format($monto, 2, ',', '.')."</td>";
	echo "<td align=center>".fecha($fecha_cheque)."</td>";
	echo "<td align=center>".fecha($fecha_anulacion)."</td>";
	$filaCq = num_rows($resultadoCheque);
	
	if($_SESSION[CONSULTA]==1){
				   echo "<td></td><td></td><td></td><td></td><td></td>";
	}ELSE{

		if ($filaCq == 0){
		echo "<td><a href=\"cheques_editar.php?banco=$codigo_banco&cheque=$chequex&chequera=$chequera&inicio=$inicio&fin=$final&valor=A\"><IMG title=\"Editar\" src=\"../imagenes/edit.gif\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
		}else{
		echo "<td></td>";
		}
		
		
		
		if ($status<>'Da' && $status<>'An')
		{
			echo "<td><a href=\"opcion_chequera.php?opcion=5&banco=$codigo_banco&chequera=$chequera&valor=A&cheque=$chequex&status=$status\"><IMG title=\"Activar\" src=\"../imagenes/activar.png\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
		
			echo "<td><a href=\"opcion_chequera.php?opcion=2&banco=$codigo_banco&chequera=$chequera&valor=D&cheque=$chequex&status=$status\"><IMG title=\"Depósito\" src=\"../imagenes/deposito.png\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
		
			echo "<td><a href=\"cheques.php?opcion=3&banco=$codigo_banco&chequera=$chequera&cheque=$chequex&monto=$monto&valor=D&status=$status&rsac=1&odp=$odp\"><IMG title=\"Dañado\" src=\"../imagenes/cheque_danado.png\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
			echo "<td><a href=\"cheques.php?opcion=4&banco=$codigo_banco&chequera=$chequera&cheque=$chequex&monto=$monto&valor=A&rsac=1&status=$status\"><IMG title=\"Anulado\" src=\"../imagenes/cheque_anular.png\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
		}else{
			echo "<td><a href=\"opcion_chequera.php?opcion=7&banco=$codigo_banco&chequera=$chequera&valor=A&cheque=$chequex&status=$status\"><IMG title=\"Activar\" src=\"../imagenes/activar.png\" width=\"16\" height=\"16\" align=\"left\" border=\"0\"></a></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
			echo "<td></td>";
		}
	}
   	 echo "</tr>";
	}
}else{
	echo"<tr><td>No existen registro con la busqueda especificada</td></tr>";
}
cerrar_conexion($conexion);

?>
<script language="JavaScript" type="text/javascript">
function mostrar_paginas(num_paginas){

}
</script>
  </tbody>
</table>
<?
pie_pagina($url,$pagina,"&tipo=".$tipob."&des=".$des."&banco=".$codigo_banco."&chequera=".$chequera,$num_paginas);
?>
</FORM>
</BODY>
</html>
