-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-06-2016 a las 17:30:39
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `central_planillaexpress`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caa_procesar_asistencia`
--

CREATE TABLE IF NOT EXISTS `caa_procesar_asistencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_encabezado` int(11) NOT NULL,
  `personal_id` int(11) DEFAULT NULL,
  `ficha` int(6) NOT NULL,
  `fecha` date NOT NULL,
  `referencia` decimal(11,2) DEFAULT NULL,
  `concepto` int(11) NOT NULL,
  `id_caa_encabezado` int(11) NOT NULL COMMENT 'id de la tabla caa_procesar_asistencia',
  PRIMARY KEY (`id`),
  KEY `Completo` (`ficha`,`fecha`,`referencia`,`concepto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
