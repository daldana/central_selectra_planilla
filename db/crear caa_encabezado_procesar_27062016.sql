-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2016 a las 15:13:08
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `central_planillaexpress`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caa_encabezado_procesar`
--

CREATE TABLE IF NOT EXISTS `caa_encabezado_procesar` (
  `id_caa_encabezado` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_reg` date NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `tipo_importacion` int(1) DEFAULT NULL COMMENT '0=Tabla, 1=Archivo',
  PRIMARY KEY (`id_caa_encabezado`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=1 ;

-- Modificacion a caa_procesar_asistencia

ALTER TABLE `caa_procesar_asistencia` ADD `id_caa_encabezado` INT(11) NOT NULL COMMENT 'id de la tabla caa_procesar_asistencia' ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
